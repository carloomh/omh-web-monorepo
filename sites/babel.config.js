module.exports = function (api) {
  api.cache(true);
  const presets = [
    ['next/babel'],
    '@zeit/next-typescript/babel',
    '@babel/preset-typescript'
  ]
  const plugins = [
    ["babel-plugin-root-import"]
  ]
  return {
    presets,
    plugins
  };
}
