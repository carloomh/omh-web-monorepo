const path = require("path");
const withPlugins = require('next-compose-plugins');
const withCustomBabelConfig = require("next-plugin-custom-babel-config");
const withTM = require("next-transpile-modules");
const withOffline = require('next-offline')
const withCSS = require('@zeit/next-css')
// env
const nextEnv = require('next-env')
const dotenvLoad = require('dotenv-load')
dotenvLoad()

const nextConfig = {
  resolve: {
    extensions: ['', '.js', '.jsx'],
    root: [
      path.resolve('./')
    ]
  },
  target: 'serverless',
  workboxOpts: {
    cacheId: 'omh-web',
    swDest: path.resolve('./', '.next', "service-worker.js"),
    runtimeCaching: [
      {
        urlPattern: /^https?.*/,
        handler: 'CacheFirst',
        options: {
          cacheName: 'https-calls',
        }
      },
      {
        urlPattern: /\.(svg|png|jpg|jpeg|gif)$/,
        handler: 'CacheFirst',
        options: {
          cacheName: 'cache-jpg'
        }
      },
      {
        urlPattern: new RegExp('^https://s3-ap-southeast-1.amazonaws.com/*'),
        handler: 'StaleWhileRevalidate',
        options: {
          cacheName: 'cache-avatars'
        }
      },
      {
        urlPattern: new RegExp('^https://misdirection.ohmyhome.com/image/*'),
        handler: 'StaleWhileRevalidate',
        options: {
          cacheName: 'cache-listing-image'
        }
      },
    ],
  },
  env: {
    OMH_API_KEY: 'oAsCpEwynd8Me6kjYnEPd8XoYKvVVTN81dKmkA7j',
    OMH_APP_FACEBOOK: '566264081176-ktt7qns23umod9tff7680pp54tib6f2b.apps.googleusercontent.com',
    OMH_APP_GOOGLE: '1847491042157912',
    OMH_MEDIA_PATH: 'https://misdirection.ohmyhome.com/image/',
    OMH_INTERCOM: 'd4wzoeye'

  }
}

module.exports = withPlugins([
  [withCustomBabelConfig, { babelConfigFile: path.resolve("../babel.config.js") }],
  [withTM, { transpileModules: ["omhwebui"] }],
  [withOffline, {
    generateInDevMode: false,
  }],
  [withCSS, {}],
], nextConfig)