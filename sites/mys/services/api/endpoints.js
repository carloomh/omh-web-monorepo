const HOST = 'https://staging.ohmyhome.com/ms'

const API = {
  BASE_URL: 'https://staging.ohmyhome.com/ms',
  // ONBOARDING
  SIGNUP: '/onboarding/signup/mobile',
  VERIFY_MOBILE: '/onboarding/signup/mobile/verify',
  VERIFY_CONFIRM: '/onboarding/signup/mobile/verify/confirm',
  ADD_USER_ROLE: '/user/update/info',
  FACEBOOK: '/onboarding/signup/facebook',
  GOOGLE: '/onboarding/signup/google',
  // Login
  LOGIN_MOBILE: '/onboarding/login/mobile',
  LOGIN_FACEBOOK: '/onboarding/login/facebook',
  LOGIN_GOOGLE: '/onboarding/login/google',
  LOGIN_FORGOT_PASSWORD: '/onboarding/forget/password',
  // UPDATE USER PROFILE
  UPDATE_MOBILE: '/user/update/mobile',
  UPDATE_VALIDATE_MOBILE: '/user/update/mobile/validate',

  // PROJECT DETAILS
  GET_PROJECT_DETAILS: '/project/info',
  GET_INFRASTRUCTURES: '/infrastructures/nearby',
  GET_SIMILAR_LISTINGS: '/projects/similar',
  SUBMIT_FREE_RIDE: '/form/general/submit',
  SUBMIT_UPTODATE: '/marketing/subscribe',
}

export default API