
import request from './request'
import API from './endpoints'

function getProjectDetails(values) {
  return request({
    url: API.GET_PROJECT_DETAILS,
    method: "GET",
    headers: {
      Authorization: 'public'
    },
    params: {
      ...values
    }
  })
}

function getInfrastructures(values) {
  return request({
    url: API.GET_INFRASTRUCTURES,
    method: "GET",
    headers: {
      Authorization: 'Bearer',
      "Accept-Country": "MYS"
    },
    params: {
      ...values
    }
  })
}

function getSimilarProjects(values) {
  return request({
    url: API.GET_SIMILAR_LISTINGS,
    method: "GET",
    headers: {
      Authorization: 'Bearer',
    },
    params: {
      ...values
    }
  })
}
function submitFreeRide(data) {
  return request({
    url: API.SUBMIT_FREE_RIDE,
    method: "POST",
    headers: {
      Authorization: 'Bearer',
    },
    data: data
  })
}

function submitUptodate(data) {
  return request({
    url: API.SUBMIT_UPTODATE,
    method: "POST",
    headers: {
      Authorization: 'Bearer',
    },
    data: data
  })
}

const NewLaunchesService = {
  getProjectDetails,
  getInfrastructures,
  getSimilarProjects,
  submitFreeRide,
  submitUptodate
}

export default NewLaunchesService