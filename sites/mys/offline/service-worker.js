const CACHE_NAME = "simple-cache-v1";
const urlsToCache = ["/profile"];

self.addEventListener("install", event => {
    console.log('Installed sw.js', event);
    const preLoaded = caches.open(CACHE_NAME)
        .then(cache => cache.addAll(urlsToCache))
    event.waitUntil(preLoaded);
});

self.addEventListener('activate', function(event) {
    console.log('Activated sw.js', event);
});

self.addEventListener("fetch", event => {
    console.log('Fetch sw.js', event);

    const response = caches.match(event.request)
        .then(match => match || fetch(event.request));
    event.respondWith(response);
});