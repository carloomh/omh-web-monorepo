const next = require('next')
const http2 = require('http2')
const { join }, path = require('path')
const fs = require('fs')

const port = parseInt(process.env.PORT, 10) || 5000
const dev = process.env.NODE_ENV !== 'production'

// Init the Next app:
const app = next({ dev })

// Create the secure HTTPS server:
// Don't forget to create the keys for your development
const server = http2.createSecureServer({
  key: fs.readFileSync('localhost-privkey.pem'),
  cert: fs.readFileSync('localhost-cert.pem')
})

app.prepare().then(() => {
  server.on('error', err => console.error(err))

  // Process the various routes based on `req`
  // `/`      -> Render index.js
  // `/about` -> Render about.js
  server.on('request', (req, res) => {
    switch(req.url) {
      case '/new-property-launch/:address/:id':
        return app.render(req, res, '/new-property-launch/[address]/[id]', { slug: req.params.slug, query: req.query, params: req.params })
      case '/profile':
        return app.render(req, res, '/profile', req)
      case '/about':
        return app.render(req, res, '/about', req.query)
      default:
        return app.render(req, res, '/', req.query)
    }
  })
  
  server.listen(port)

  console.log(`Listening on HTTPS port ${port}`)
})
