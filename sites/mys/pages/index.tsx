import * as React from "react"
import axios from 'axios'
import fetch from 'isomorphic-unfetch'
import Link from 'next/link'
// components
import Box from "@material-ui/core/Box"
import BtnDefault from "omhwebui/components/Button/default"
import { TextInput } from "omhwebui"

import { AppContext } from './_app'
import OnboardService from '../services/api/onboard'
import Layout from '../components/Layout'

const Index = () => {
  const value = React.useContext(AppContext)
  let drawerToggle = value['state']['setDrawerStatus']
  const userData = value['state']['userData']
  const bearerToken = value['state']['bearer']
  const pageCountry = value['state']['pageCountry']

  return (
    <Layout
      title={'Home'}
    >
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignContent: "center",
          height: "100%",
          width: "300px",
          paddingTop: "40px"
        }}
      >
        {userData && (
          <div>
            <h1>{userData.firstName || 'Not yet logged in'}</h1>
            <h4>{bearerToken}</h4>
          </div>
        )}

        <Box
          width={200}
        >
          <BtnDefault
            className={null}
            action={() => drawerToggle(true)}
            variant="contained"
            color="primary"
            text="Open login"
          />
        </Box>
        <Link href='/new-property-launch/[address]/[id]' as='/new-property-launch/m-vertica-kl-city/3'>
          <a className={'projectUrl'}>M Vertica Kuala Lumpur City</a>
        </Link>
      </div>
      <style jsx>
        {`
          a.projectUrl {
            margin: 40px 10px;
          }
        `}
      </style>
    </Layout>
  )
}

export default Index;