import * as React from 'react';
import App from 'next/app'
import { ThemeProvider } from '@material-ui/styles'
import CssBaseline from '@material-ui/core/CssBaseline'

// component
import Login from '../components/Onboard'
// context
export const AppContext = React.createContext({})
// theme
import theme from '../src/theme'

function OMHWeb(props) {
  const [drawerStatus, setDrawerStatus] = React.useState(false)
  const [bearer, setBearer] = React.useState()
  const [userData, setUserData] = React.useState({})
  const [pageCountry, setPageCountry] = React.useState("SGP")
  const [scrollPos, setScrollPos] = React.useState(0)

  const { Component, pageProps } = props

  const hooks = {
    drawerStatus,
    setDrawerStatus,
    bearer,
    setBearer,
    userData,
    setUserData,
    pageCountry,
    setPageCountry,
    setScrollPos,
    scrollPos
  }

  const handleScroll = event => {
    setScrollPos(+window.scrollY)
  }


  React.useEffect(() => {
    const jssStyles = document.querySelector('#jss-server-side')
    if(jssStyles) {
      jssStyles.parentNode.removeChild(jssStyles)
    }

    window.addEventListener('scroll', handleScroll);
    return () => window.removeEventListener('scroll', handleScroll);
  }, [])


  return (
    <ThemeProvider theme={theme}>
      <AppContext.Provider
        value={{
          state: hooks
        }}
      >
        <CssBaseline />
        <Component {...pageProps} />
        <Login />
      </AppContext.Provider>
    </ThemeProvider>
  )
}

export default OMHWeb
