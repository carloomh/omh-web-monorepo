import * as React from 'react'
import styled from 'styled-components'
import Intercom from 'react-intercom'
import { useRouter } from 'next/router'
import Responsive from 'react-responsive'
import { AppContext } from '../../../_app'
import Layout from '../../../../components/Layout'
import NewLaunchesService from '../../../../services/api/newlaunches'

import * as Module from '../../../../components/ProjectDetails'
import { device } from '../../../../src/media-queries'

const Desktop = props => <Responsive {...props} minWidth={1224} />
const Mobile = props => <Responsive {...props} maxWidth={1123} />

const Container = styled.div`
  position: relative;
  padding-top: 40px;
`

const Content = styled.div`
  width: 100%;
  margin: 0px auto;
  padding-top: 70px;
  padding-bottom: 0;

  @media (min-width : 1128px) {
    width: 972px;
    padding-top: 100px;
  }
`

const MainRow = styled.div`
  display: flex;
  position: relative;
`

const MainColumn = styled.div`
  width: 100%;

  @media (min-width : 1128px) {
    width: 60%;
    margin-right: 40px;
    min-width: 568px;
  }
`
const SideColumn = styled.div`
  width: 40%;
  min-width: 370px;
`

const ToggleHeader = styled.div`
  position: fixed;
  width: 100%;
  z-index: 999;
  transition: transform 200ms ease-in-out;
`

const media_path = 'https://misdirection.ohmyhome.com/cms/media'

const ProjectDetails = () => {
  const router = useRouter()
  const { id } = router.query
  const value = React.useContext(AppContext)
  const val = value['state']
  const pageCountry = val['pageCountry']
  const scrollPos = val['scrollPos']
  const [ filters, setFilter ] = React.useState([])
  const [ infrastructures, setInfrastructures ] = React.useState([])
  const [ similar_projects, setSimilarProjects ] = React.useState([])
  const [ activeNav, setNavLink ] = React.useState('photo_gallery')
  const [ project_details, setProjectDetails ] = React.useState({
    _id: null,
    galleries: [],
    name: '',
    highlights: [],
    floorPlans: [],
    vicinities: [],
    facilities: [],
    address: {},
    agent: {}
  })

  React.useEffect(() => {
    async function fetchDetails() {
      const params = { _uid: id }
      NewLaunchesService
      .getProjectDetails(params)
      .then(response => {
        const { data } = response
        const { _data } = data
        _data && _data.project && setProjectDetails(_data.project)
        const { address } = _data && _data.project
        address && fetchInfrastructures( address.longitude, address.latitude )
      })
    }

    fetchDetails()

    async function fetchInfrastructures(long, lat) {
      Array.from({length: 6}, (x, index) => {
        const params = {
          latitude: lat,
          longitude: long,
          radius: 3000,
          type: index+1
        }
        NewLaunchesService
        .getInfrastructures(params)
        .then(response => {
          const { data } = response
          const { _data } = data
          if(_data && _data.infrastructures){
            setInfrastructures(infrastructures => infrastructures.concat(_data.infrastructures))
            setFilter(filters => filters.concat(index+1) )
          }
        })
      })
    }

    async function fetchSimilarProjects() {
      const params = { _uid: id }
      NewLaunchesService
      .getSimilarProjects(params)
      .then(response => {
        const { data } = response
        const { _data } = data
        _data && _data.list && setSimilarProjects(_data.list)
      })
    }
    fetchSimilarProjects()
  }, [])

  const {
    name,
    highlights,
    galleries,
    floorPlans,
    vicinities,
    facilities,
    address,
    agent
  } = project_details

  const NavLinks = [
    'photo_gallery',
    'floor_plan',
    'location',
    'site_map',
    'similar_projects',
  ]

  NavLinks.filter( link => {
    if(link === 'floor_plan' && !floorPlans.length || link === 'site_map' && !vicinities.length || link === 'similar_projects' && !similar_projects.length)
      return false
    return link
  })

  const user = {
    name: 'Username',
    custom_launcher_selector: '#custom_launcher_selector'
  }

  return (
    <Layout
      title={'New Property Launches'}
    >
      <Intercom appID={process.env.OMH_INTERCOM} { ...user } hide_default_launcher={true} />
      <Container>
        <ToggleHeader
          style={{
            transform:  scrollPos > 0 ? 'translateY(-40px)' : 'translateY(0)'
          }}
        >
          <Desktop>
            <Module.HeaderDetails
              show={scrollPos > 0}
              details = {project_details}
              telephone = { '60 16-299 1366' }
            />
          </Desktop>
          <Module.Navigation
            links={NavLinks}
            active={ activeNav }
            scrollTo={setNavLink}
          />
        </ToggleHeader>
        <Content>
          <MainRow>
            <MainColumn>
              <Module.Gallery
                galleries={galleries}
              />
              <Module.About
                details={ project_details }
                pageCountry={ pageCountry }
              />
              <Module.FloorPlan
                floorPlans={floorPlans}
                media_path={media_path}
              />
              <Module.Infrastructures
                infrastructures={infrastructures}
                filters={filters}
                address={address}
                media_path={media_path}
              />
              <Module.Vicinities
                facilities={ facilities }
                vicinities={ vicinities }
                media_path={media_path}
              />
              <Module.FreeRide
                sidebar={false}
                project_details={project_details}
              />
            </MainColumn>
            <Desktop>
              <SideColumn>
                <Module.ProjectInfo
                  details = { project_details }
                  highlights= { highlights }
                  media_path={media_path}
                />
                <Module.FreeRide
                  sidebar={true}
                  project_details={project_details}
                />
                <Module.AgentTestimonial
                  agent = { agent }
                  media_path={media_path}
                />
              </SideColumn>
            </Desktop>
          </MainRow>
          <Module.SimilarProjects
            media_path={media_path}
            projects={similar_projects}
          />
        </Content>
      </Container>
    </Layout>
  )
}

ProjectDetails.getInitialProps = async ctx => {
  const initialProps = await ProjectDetails.getInitialProps(ctx);
  return {
    ...initialProps,
  }
}

export default ProjectDetails;