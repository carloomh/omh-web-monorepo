import * as React from 'react'
import { NextPageContext, NextPage } from 'next'
import { AppContext } from './_app'
import Router, { useRouter } from 'next/router'
import Layout from '../components/Layout'
import { withAuthSync, logout, auth } from '../services/api/auth'

interface ProfileProps {
  userAgent?: string,
}

const Profile: NextPage<ProfileProps> = (props) => {
  console.log(props)
  const value = React.useContext(AppContext)
  const val = value['state']
  const setUserData = val['setUserData']
  const userData = value['state']['userData']

  const router = useRouter()

  return(
    <Layout
      title={'New Property Launches'}
    >
      Token
      <button onClick={() => {
        setUserData({})
        logout()
      }}>Logout</button>
    </Layout>
  )
}

Profile.getInitialProps = async ({ req }) => {
  // const initialProps = Profile.getInitialProps(ctx)
  const userAgent = req ? req.headers['user-agent'] : navigator.userAgent
  // const token = auth(ctx)
  return {
    // ...initialProps,
    userAgent: userAgent
  }
}

export default Profile