import SGP from './SGP'
import MYS from './MYS'

const LANG = {
  "SGP": SGP,
  "MYS": MYS
}

export default LANG