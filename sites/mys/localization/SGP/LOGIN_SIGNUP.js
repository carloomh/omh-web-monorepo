export const LANG = {
  LOGIN: {
    OPTIONS: {
      TITLE: "Welcome Back",
      SUBTITLE: "We're happy to see you, log in below.",
      NO_ACCOUNT: "No account?",
      CREATE: "Create one"
    },
    PROPER: {
      TITLE: "Log in to your account",
      SUBTITLE: "Please enter your log in details.",
      FORGOT: "Forgot password?",
      ERROR: {
        LOGIN: 'You have entered an incorrect number, password or both. Please try again.'
      }
    },
    FORGOT: {
      TITLE: "Forgot password?",
      SUBTITLE: "Forgot password?",
    },
    PASSWORD_SUCCESS: {
      TITLE: "Password reset email sent",
      SUBTITLE: "An email has been sent to your inbox. Follow the instructions to reset your password.",
    },
    ERROR: {
      REQUIRED: 'This is required.',
      LOGIN: 'You have entered an incorrect number, password or both.Please try again.',
      INVALID: {
        MOBILE: 'Invalid mobile number',
        EMAIL: 'Invalid email address'
      }
    }
  },
  // signup
  SIGNUP: {
    OPTIONS: {
      TITLE: "Join Ohmyhome",
      SUBTITLE: "We're happy to see you, log in below",
      HAS_ACCOUNT: "Already have an account?",
      BY_SIGNING: "By signing up you agree to our",
      TERMS: "Terms and Conditions & Privacy Policy",
    },
    PROPER: {
      TITLE: "Let's set you up",
      SUBTITLE: "Don't worry - this won't take long.",
      HELPER: {
        EMAIL: "We'll use your email to recover your account securely.",
        MOBILE: "This is to verify your phone number & secure your account.",
      }
    },
    OTP: {
      TITLE: "OTP sent",
      SUBTITLE: "The OTP has been sent to your mobile number",
      RESEND: "Resend OTP"
    },
    PROCEED: {
      TITLE: "Are you sure you want to proceed?",
      SUBTITLE: "You are about to create a new account.",
    },
    EXISTING: {
      TITLE: "You seem familiar",
      SUBTITLE: "Looks like your mobile number has already been linked with your Google account.",
    },
    SOCIAL_VERIFY: {
      TITLE: "Let's set you up",
      SUBTITLE: "Don't worry - this won't take long.",
      HELPER: {
        MOBILE: "This is to verify your phone number & secure your account.",
      }
    },
    SELECTROLE: {
      TITLE: "Almost there",
      SUBTITLE: "Let us know your interest",
    },
    SUCCESS: {
      TITLE: "Forgot password?",
      SUBTITLE: "Forgot password?",
    },
    SHOUTOUT: {
      TITLE: "Finding a room",
      SUBTITLE: "Let leasers know you’re looking for one",
    }
  },
  // common
  COMMON: {
    BACK: "Back",
    LOG_IN: "Log in",
    FORGOT_PASSWORD: "Forgot password?"
  },
}

