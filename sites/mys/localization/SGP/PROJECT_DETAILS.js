export const PD_LANG = {
  ABOUT: {
    HEADER: 'About the Property',
    DEVELOPER: 'Property Developer',
    TENURE: 'Tenure',
    COMPLETION: 'Completion Year',
    UPTODATE: 'Get up-to-date on new launches'
  }
}