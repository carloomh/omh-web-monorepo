import { LANG } from './LOGIN_SIGNUP'
import { PD_LANG } from './PROJECT_DETAILS'

const MYS = {
  'LANG': LANG,
  'PD_LANG': PD_LANG
}

export default MYS