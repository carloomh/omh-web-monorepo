import { createMuiTheme } from '@material-ui/core/styles'

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#EE620F'
    },
  },
  overrides: {
    MuiFormHelperText: {
      root: {
        lineHeight: '1.7'
      }
    },
    MuiButton: {
      root: {
        boxShadow: 'none'
      },
      label: {
        textTransform: 'initial'
      },
      contained: {
        boxShadow: 'none'
      },
      outlined: {
        boxSizing: 'border-box'
      }
    },
    MuiButtonBase: {
      root: {
        height: '45px',
        boxShadow: 'none'
      }
    },
    MuiInputBase: {
      root: {
        height: '48px'
      },
    },
    MuiOutlinedInput: {
      root: {
        "&$focused > fieldset": {
          borderColor: "#72C7DB !important"
        }
      }
    },
    MuiInput: {
      underline: {
        "&::after": {
          borderBottom: "2px solid #EE620F"
        }
      }
    },
    MuiInputLabel: {
      root: {
        color: "#9B9B9B"
      },
      outlined: {
        transform: 'translate(14px, 17px) scale(1)'
      }
    },
    MuiFormControl: {
      root: {
        width: '100%'
      }
    }
  }
});

export default theme