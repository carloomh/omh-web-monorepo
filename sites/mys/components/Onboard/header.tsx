import * as React from "react"
import styled from 'styled-components'
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"

import { device } from '../../src/media-queries'

const Container = styled(Box)`
  text-align: center;
  margin: 22px 0;

  @media ${device.mobileL} {
    margin: 30px 0;
  }
`
const ContainerMobile = styled(Box)`
  text-align: left;
  margin: 22px 0;

  @media ${device.mobileL} {
    text-align: center;
    margin: 30px 0;
  }
`
const Title = styled(Typography)`
  font-size: 24px !important;
  font-weight: 600 !important;
  line-height: 1.5 !important;
  color: #212121;

  @media ${device.mobileL} {
    font-size: 28px !important;
  }
`
const SubTitle = styled(Typography)`
  font-size: 16px !important;
  color: #424242 !important;
  
  @media ${device.mobileL} {}
`

interface Props {
  title: string,
  subtitle: string,
}

function renderDefault(title, subtitle) {
  return (
    <Container>
      <Title
        component="div"
        variant="h3"
      >
        {title}
      </Title>
      <SubTitle
        component="div"
        variant="subtitle1"
      >
        {subtitle}
      </SubTitle>
    </Container>
  )
}

function renderMobileAlign(title: string, subtitle: string) {
  return (
    <ContainerMobile>
      <Title
        component="div"
        variant="h3"
      >
        {title}
      </Title>
      <SubTitle
        component="div"
        variant="subtitle1"
      >
        {subtitle}
      </SubTitle>
    </ContainerMobile>
  )
}

function Header({
  title,
  subtitle,
  hasAlign = false
}) {
  return (
    <React.Fragment>
      {!hasAlign
        ? renderDefault(title, subtitle)
        : renderMobileAlign(title, subtitle)}
    </React.Fragment>
  )
}

export default Header