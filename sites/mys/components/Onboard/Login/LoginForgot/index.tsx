import * as React from "react"
import { makeStyles } from "@material-ui/core/styles"
import { Formik } from 'formik'
import * as Yup from 'yup'
import styled from 'styled-components'
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import BtnDefault from 'omhwebui/components/Button/default'
import { MobileInput } from "omhwebui"
import { TextInput } from "omhwebui"
import Header from "../../header"
// config
import OnboardService from '../../../../services/api/onboard'
import { device } from '../../../../src/media-queries'
import { LoginForgotValidation } from './validation'
// localization
import LANG from '../../../../localization'

const Container = styled(Grid)`
  padding-top: 0px;

  @media ${device.mobileL} {
    padding-top: 180px;
  }
`

function LoginForgot({ hooks }) {
  const {
    country,
    setCountry,
    countryList,
    setCountryList,
    formData,
    setFormData,
    pageCountry,
    screen,
    setView,
    resetFormData,
  } = hooks

  const {
    TITLE,
    SUBTITLE,
  } = LANG[pageCountry].LANG.LOGIN.FORGOT

  const {
    FORGOT_PASSWORD,
    BACK,
  } = LANG[pageCountry].LANG.COMMON

  const useStyles = makeStyles(theme => ({}))
  const classes = useStyles({})
  const [values, setValues] = React.useState({
    amount: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  })

  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
  }

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  }

  const saveMobile = (value) => {
    setFormData({ ...formData, mobile: value })
  }

  const saveValue = (key, value) => {
    setFormData({ ...formData, [key]: value })
  }

  function renderView() {
    if(screen.current !== 'login_forgot') {
      return null
    }

    return (
      <Container
        container
        direction="column"
      >
        <Header
          title={TITLE}
          subtitle={SUBTITLE}
          hasAlign
        />
        <Box width={"100%"} mb={3}>
          <Grid
            container
            direction="column"
          >
            <Formik
              initialValues={{
                emailAddress: formData.emailAddress,
                mobileNumber: formData.mobileNumber,
              }}
              onSubmit={(values, { setSubmitting }) => {
                const params = {
                  "countryCode": countryList[country].code,
                  "mobile": `${countryList[country].phone}${values.mobileNumber}`,
                  "email": values.emailAddress
                }
                OnboardService
                  .forgotPassword(params)
                  .then(response => {
                    setView("login_password_success", screen.current)
                    resetFormData()
                  })
              }}
              validationSchema={LoginForgotValidation(
                countryList[country].code === undefined ||
                  countryList[country].code !== "SGP" ? "DEFAULT" : "SGP")}
            >
              {props => {
                const {
                  values,
                  touched,
                  errors,
                  dirty,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  handleReset,
                } = props
                return (
                  <form onSubmit={handleSubmit}>
                    <Box mb={3}>
                      <MobileInput
                        name="mobileNumber"
                        label="Mobile Number"
                        type="tel"
                        country={country}
                        setCountry={setCountry}
                        countryList={countryList}
                        saveValue={saveValue}
                        value={formData.mobileNumber}
                      />
                    </Box>
                    <Box mb={3}>
                      <TextInput
                        name="emailAddress"
                        label="Email"
                        type="email"
                        saveValue={saveValue}
                        value={formData.emailAddress}
                      />
                    </Box>
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Link
                        onClick={() =>
                          setView(screen.previous, screen.current)}
                      >
                        {BACK}
                      </Link>
                      <Box
                        width={94}
                        height={44}
                      >
                        <BtnDefault
                          type="submit"
                          variant="contained"
                          text="Reset"
                          color="primary"
                          isSubmitting={isSubmitting}
                          action={() => handleSubmit()}
                        />
                      </Box>
                    </Box>
                  </form>
                )
              }}
            </Formik>
          </Grid>
        </Box>
      </Container>
    )
  }

  return (
    renderView()
  )
}

export default LoginForgot