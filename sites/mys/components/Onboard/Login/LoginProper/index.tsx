import * as React from "react"
import { makeStyles } from "@material-ui/core/styles"
import styled from 'styled-components'
import { Formik } from 'formik'
import * as Yup from 'yup'
import axios from 'axios'
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import { TextInput } from "omhwebui"
import { MobileInput } from "omhwebui"
import { InputPassword } from "omhwebui"
import BtnDefault from 'omhwebui/components/Button/default'
import Header from "../../header"
// localization
import LANG from '../../../../localization'
// config
import OnboardService from '../../../../services/api/onboard'
import { setToken } from '../../../../services/api/auth'
import { device } from '../../../../src/media-queries'
import { LoginValidation } from './validation'

function LoginProper({ hooks }) {
  const {
    country,
    setCountry,
    countryList,
    setCountryList,
    setBearer,
    setUserData,
    drawerToggle,
    formData,
    setFormData,
    pageCountry,
    screen,
    setView,
    resetFormData,
  } = hooks

  const {
    TITLE,
    SUBTITLE,
    ERROR,
  } = LANG[pageCountry].LANG.LOGIN.PROPER

  const {
    FORGOT_PASSWORD,
    BACK,
  } = LANG[pageCountry].LANG.COMMON

  const [errorMessage, setErrorMessage] = React.useState(false)
  const [passwordValues, setPasswordValues] = React.useState({
    password: "",
    showPassword: false,
  })

  const handleClickShowPassword = () => {
    setPasswordValues({
      ...passwordValues,
      showPassword: !passwordValues.showPassword
    })
  }

  const saveValue = (key, value) => {
    setFormData({ ...formData, [key]: value })
  }

  function errorView() {
    if(!errorMessage) {
      return null
    }
    return (
      <div>
        <div
          className="error-message"
        >
          {ERROR.LOGIN}
        </div>
        <style jsx>{`
        .error-message {
          padding-bottom: 20px;
          color: red;
        }
      `}</style>
      </div>
    )
  }

  function renderView() {
    if(screen.current !== 'login_proper') {
      return null
    }

    return (
      <div
        className="container"
      >
        <Header
          title={TITLE}
          subtitle={SUBTITLE}
          hasAlign
        />
        <div
          className="error-container"
        >
          {errorView()}
        </div>

        <Box width={"100%"} mb={3}>
          <Grid
            container
            direction="column"
          >
            <Formik
              initialValues={{
                mobileNumber: formData.mobileNumber,
                password: '',
                passwordValues,
              }}
              onSubmit={(values, { setSubmitting }) => {
                setErrorMessage(false)
                const params = {
                  "countryCode": countryList[country].code,
                  "mobile": `${countryList[country].phone}${values.mobileNumber}`,
                  "password": values.password
                }
                OnboardService
                  .loginMobile(params)
                  .then(response => {
                    if(response.data._message === "ErrorOnboardingLoginMobileFailed") {
                      setSubmitting(false)
                      setErrorMessage(true)
                    } else {
                      setBearer(response.headers["x-amzn-remapped-authorization"])
                      setToken({ token: response.headers["x-amzn-remapped-authorization"]})
                      setUserData(response.data._data.user)
                      resetFormData()
                      drawerToggle(false)
                    }
                  })
              }}
              validationSchema={LoginValidation(
                countryList[country].code === undefined ||
                  countryList[country].code !== "SGP" ? "DEFAULT" : "SGP")}
            >
              {(props) => {
                const {
                  values,
                  touched,
                  errors,
                  dirty,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  handleReset,
                } = props

                return (
                  <form onSubmit={handleSubmit}>
                    <Box mb={3}>
                      <MobileInput
                        name="mobileNumber"
                        label="Mobile Number"
                        type="tel"
                        country={country}
                        setCountry={setCountry}
                        countryList={countryList}
                        saveValue={saveValue}
                        value={formData.mobileNumber}
                      />
                    </Box>
                    <Box mb={3}>
                      <InputPassword
                        name="password"
                        type='text'
                        label="Password"
                        value={passwordValues.password}
                        showPassword={passwordValues.showPassword}
                        togglePassword={handleClickShowPassword}
                      />
                    </Box>
                    <Box mb={3}>
                      <Typography
                        component="div"
                        variant="subtitle1"
                        align="right"
                        style={{
                          fontSize: "15px",
                          color: "#000000"
                        }}
                      >
                        <Link
                          onClick={() => setView("login_forgot", screen.current)}
                        >
                          {FORGOT_PASSWORD}
                        </Link>
                      </Typography>
                    </Box>
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Link
                        onClick={() =>
                          setView("login_home", screen.current)}
                      >
                        {BACK}
                      </Link>
                      <Box
                        width={94}
                        height={44}
                      >
                        <BtnDefault
                          type="submit"
                          variant="contained"
                          text="Log in"
                          color="primary"
                          isSubmitting={isSubmitting}
                          action={() => handleSubmit()}
                        />
                      </Box>
                    </Box>
                  </form>
                )
              }}
            </Formik>
          </Grid>
        </Box>
        <style jsx>{`
          .container {
            width: 100%;
            display: flex;
            flex-direction: column;
          }

          @media ${device.mobileL} {
            .container {
              padding-top: 180px;
            }
          }
        `}</style>
      </div>
    )
  }

  return (
    renderView()
  )
}

export default LoginProper