import * as React from "react"
import styled from 'styled-components'
import CircularProgress from '@material-ui/core/CircularProgress'
// components
import BtnWide from "omhwebui/components/Button/wide"
import Header from "../../header"
import GoogleLogin from '../../components/google'
import FacebookLogin from '../../components/facebook'
// config
import OnboardService from '../../../../services/api/onboard'
import { device } from '../../../../src/media-queries'
import { colors } from '../../../../src/vars'
// localization
import LANG from '../../../../localization'
//assets
import Artwork from "../../assets/artwork"

function LoginOptions({ hooks }) {
  const {
    step,
    drawerToggle,
    setBearer,
    setUserData,
    setSocial,
    setTokensetSocial,
    setToken,
    pageCountry,
    screen,
    setView,
  } = hooks

  const {
    LOGIN,
    TITLE,
    SUBTITLE,
    CREATE,
    NO_ACCOUNT,
  } = LANG[pageCountry].LANG.LOGIN.OPTIONS

  const [isFacebookActive, setIsFacebookActive] = React.useState(false)
  const [isGoogleActive, setIsGoogleActive] = React.useState(false)
  const [loading, setLoading] = React.useState(false)

  function loginFacebook(response) {
    setLoading(true)
    setUserData({
      email: response.email,
      firstName: response.first_name,
      lastName: response.last_name,
    })
    setIsFacebookActive(false)
    try {
      OnboardService
        .loginFacebook({ "token": response.accessToken })
        .then(response => {
          setSocial('facebook')
          setLoading(false)

          if(response && response.data._data.token !== undefined) {
            setToken(response.data._data.token)
            setView("signup_socialverify", screen.current)
          }

          if(response.data._data.user.altMobile !== undefined) {
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("verification_option", "")
          } else {
            setUserData(response.data._data.user)
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("signup_success", "")
            drawerToggle(false)
          }
        })
    } catch {
      console.error('error')
    }
  }

  function loginGoogle(response) {
    setUserData(response.profileObj)
    setLoading(true)
    try {
      OnboardService
        .loginGoogle({ "token": response.tokenId })
        .then(response => {
          setSocial('google')
          setLoading(false)

          if(response.data._data && response.data._data.token !== undefined) {
            setToken(response.data._data.token)
            setView("signup_socialverify", screen.current)
          }

          if(response.data._data.user.altMobile !== undefined) {
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("verification_option", "")
          } else {
            setUserData(response.data._data.user)
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("signup_success", "")
            drawerToggle(false)
          }
        })
    } catch {
      console.error('error')
    }
  }

  function loadingView() {
    return (
      <h2>sample, as in just a sample loading.....</h2>
    )
  }

  function renderView() {
    if(screen.current !== 'login_home') {
      return null
    }

    return (
      <div
        className="container"
      >
        <div
          className="art-content"
        >
          <Artwork />
        </div>

        <div
          className="heading"
        >
          <Header
            title={TITLE}
            subtitle={SUBTITLE}
          />
        </div>

        <div
          className="content"
        >
          <div
            className="btn-groups"
          >
            <FacebookLogin
              isFacebookActive={isFacebookActive}
              loginFacebook={loginFacebook}
              setIsFacebookActive={setIsFacebookActive}
              label={"Log in with Facebook"}
            />
            <GoogleLogin
              loginGoogle={loginGoogle}
              label={"Log in with Google"}
            />
            <BtnWide
              action={() =>
                setView("login_proper", screen.current)}
              className={null}
              variant="outlined"
              text="Log in with Mobile"
            />
          </div>
        </div>

        <footer
          className="footer-links"
        >
          <div
            className="footer-link"
          >
            {NO_ACCOUNT} <div className="footer-link-item" onClick={() => setView("signup_home", screen.current)}>{CREATE}</div>
          </div>
        </footer>
        <style jsx>{`
          .container {
            display: flex;
            flex-direction: column;
            justify-content: center;
            align-items: center;
            height: 100%;
            padding-top: 0;
          }

          .art-content {
            width: 197px;
            height: 150px;
          }

          .art-content svg {
            width: 100%;
            height: 100%;
          }

          .heading {
            text-align: center;
          }

          .content {
            width: 100%;
          }

          .btn-groups {
            display: flex;
            flex-direction: column;
            justify-content: center;
            alignItems: center;
          }

          .btn-container {
            width: 100% !important;
          }
          
          span {
            width: 100%;
          }

          button {
            width: 100%;
            margin: 0 0 20px 0;
            min-height: 45px;
            font-size: 14px;
            border-radius: 4px !important;
            font-family: inherit !important;
            font-weight: 500 !important;
            padding: 0 !important;
            box-sizing: content-box;
          }

          .footer-links {
            margin-top: 20px;
            color: #000000;
            font-size: 14px;
          }

          .footer-link {
            display: flex;
          }

          .footer-link-item {
            padding-left: 6px;
            color: ${colors.primary};
            cursor: pointer;
          }

          @media ${device.mobileL} {
            .art-content {
              width: 242px;
              height: 182px;
            }

            .footer-links {
              font-size: 15px;
            }
          }
        `}</style>
      </div>
    )
  }

  return (
    <React.Fragment>
      {loading
        ? loadingView()
        : renderView()}
    </React.Fragment>
  )
}

export default LoginOptions