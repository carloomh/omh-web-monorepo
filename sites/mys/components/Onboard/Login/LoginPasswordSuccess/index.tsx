import * as React from "react"
import { makeStyles } from "@material-ui/core/styles"
import styled from "styled-components"
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import BtnDefault from "omhwebui/components/Button/default"
import { TextInput } from "omhwebui"
import Header from "../../header"
// localization
import LANG from '../../../../localization'
// assets
import EmailSent from "../../assets/email-sent"
// config
import { device } from '../../../../src/media-queries'

const Container = styled(Grid)`
  padding-top: 0px;
  height: 100%;
`
const Heading = styled(Box)`
  & > div > .MuiTypography-root:nth-child(1) {
    padding: 0 80px 5px;
    line-height: 1.3 !important;

    @media ${device.mobileL} {
      padding: 0 50px 5px;
      line-height: 1.3 !important;
    }
  }
`
const ArtContent = styled(Box)`
  width: 197px;
  height: 150px;

  @media ${device.mobileL} {
      width: 242px;
      height: 182px;
  }

  svg {
    width: 100%;
    height: 100%;
  }
`

function LoginPasswordSuccess({ hooks }) {
  const {
    pageCountry,
    screen,
    setView
  } = hooks

  const {
    TITLE,
    SUBTITLE,
  } = LANG[pageCountry].LANG.LOGIN.PASSWORD_SUCCESS

  const useStyles = makeStyles(theme => ({}))
  const classes = useStyles({})
  const [values, setValues] = React.useState({
    amount: "",
    password: "",
    weight: "",
    weightRange: "",
    showPassword: false,
  })

  const handleChange = prop => event => {
    setValues({ ...values, [prop]: event.target.value });
  }

  const handleClickShowPassword = () => {
    setValues({ ...values, showPassword: !values.showPassword });
  }

  function renderView() {
    if(screen.current !== 'login_password_success') {
      return null
    }

    return (
      <Container
        container
        direction="column"
        justify="center"
        alignItems="center"
      >
        <ArtContent>
          <EmailSent />
        </ArtContent>
        <Heading>
          <Header
            title={TITLE}
            subtitle={SUBTITLE}
          />
        </Heading>
        <Box
          width={145}
        >
          <BtnDefault
            className={null}
            variant="outlined"
            text="Back to Log in"
            action={() =>
              setView("login_proper", screen.current)}
          />
        </Box>
      </Container>
    )
  }

  return (
    renderView()
  )
}

export default LoginPasswordSuccess