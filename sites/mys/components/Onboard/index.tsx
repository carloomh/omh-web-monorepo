import * as React from "react"
import styled from "styled-components"
import axios from 'axios'
// components
import Box from "@material-ui/core/Box"
import LoginOptions from "./Login/LoginOptions"
import LoginProper from "./Login/LoginProper"
import LoginForgot from "./Login/LoginForgot"
import LoginPasswordSuccess from './Login/LoginPasswordSuccess'
import SignupOptions from "./Signup/SignupOptions"
import SignupSocialVerify from './Signup/SignupSocialVerify'
import SignupProper from "./Signup/SignupProper"
import SignupExisting from "./Signup/SignupExisting"
import SignupProceed from "./Signup/SignupProceed"
import SignupSelectRole from "./Signup/SignupSelectRole"
import SignupShoutout from "./Signup/SignupShoutout"
import SignupOtp from "./Signup/SignupOtp"
import SignupSuccess from "./Signup/SignupSuccess"
import VerificationOption from "./Verification/VerificationOption"
import VerificationOTP from "./Verification/VerificationOtp"
import { Drawer } from "omhwebui"
// context
import { AppContext } from "../../pages/_app"
// config
import { device } from '../../src/media-queries'

interface Window {
  PasswordCredential: any,
  FederatedCredential: any
}
interface navigator {
  credentials: any,
}

declare var window: Window
declare var navigator: navigator

// styled
const Wrapper = styled(Box)`
  height: 100%;
  padding: 45px 23px;

  @media ${device.mobileL} {
    padding: 0 64px;
  }
`

function Login() {
  // data _app scope
  const value = React.useContext(AppContext)
  const val = value['state']
  const drawerStatus = val['drawerStatus']
  const drawerToggle = val['setDrawerStatus']
  const bearer = val['bearer']
  const setBearer = val['setBearer']
  const userData = val['userData']
  const setUserData = val['setUserData']
  const pageCountry = val['pageCountry']

  // local scope
  const [step, setStep] = React.useState("login_home")
  const [token, setToken] = React.useState("")
  const [social, setSocial] = React.useState("")
  const [country, setCountry] = React.useState("0")
  const [countryList, setCountryList] = React.useState([])
  const [loading, setLoading] = React.useState(false)
  const [savedParams, setSavedParams] = React.useState(false)
  const [timerCount, setTimerCount] = React.useState(10)
  const [formData, setFormData] = React.useState({
    firstName: '',
    lastName: '',
    emailAddress: '',
    mobileNumber: '',
  })

  /*
  ** steps:
    1. login_home
    2. login_proper
    3. login_forgot
    4. login_password_success
    5. signup_home
    6. signup_proper
    7. signup_otp
    8. signup_existing
    9. signup_proceed
    10. signup_selectrole
    11. signup_shoutout
    12. signup_socialverify
    13. verification_option
    14. verification_otp
  */
  const [screen, setScreen] = React.useState({
    current: 'login_home',
    previous: ''
  })
  const [otp, setOtp] = React.useState({
    first: '',
    second: '',
    third: '',
    fourth: '',
  })

  /*
  ** Control the view screen
  ** for navigating through views
  */
  function setView(current: string, previous: string) {
    setScreen({
      current: current,
      previous: previous === undefined ? "" : previous
    })
  }

  /*
  ** Reset the formData
  ** Function for reset once form submit success
  */
  function resetFormData() {
    setFormData({
      firstName: '',
      lastName: '',
      emailAddress: '',
      mobileNumber: '',
    })
  }

  /*
  ** Reset OTP
  */
  function resetOTP() {
    setOtp({
      first: '',
      second: '',
      third: '',
      fourth: '',
    })
  }


  React.useEffect(() => {
    async function fetchData() {
      const response = await axios('https://qa.ohmyhome.com.my/api/global/config')
      setCountryList(response.data.countries)
    }
    fetchData()
  }, [])

  const hooks = {
    step,
    setStep,
    country,
    setCountry,
    token,
    setToken,
    countryList,
    setCountryList,
    bearer,
    setBearer,
    userData,
    setUserData,
    drawerToggle,
    social,
    setSocial,
    loading,
    setLoading,
    savedParams,
    setSavedParams,
    timerCount,
    setTimerCount,
    pageCountry,
    // screen
    screen,
    setView,
    // persist form data
    formData,
    setFormData,
    // persist otp
    otp,
    setOtp,
    resetFormData,
    resetOTP,
  }

  const isToggleHidden = step === 'login_home' || step === 'signup_home'

  return (
    <Drawer
      anchor="right"
      status={drawerStatus}
      toggle={() => drawerToggle(false)}
      isToggleHidden={isToggleHidden}
    >
      <Wrapper>
        <LoginOptions hooks={hooks} />
        <LoginProper hooks={hooks} />
        <LoginForgot hooks={hooks} />
        <LoginPasswordSuccess hooks={hooks} />
        <SignupOptions hooks={hooks} />
        <SignupSocialVerify hooks={hooks} />
        <SignupProper hooks={hooks} />
        <SignupExisting hooks={hooks} />
        <SignupProceed hooks={hooks} />
        <SignupSelectRole hooks={hooks} />
        <SignupShoutout hooks={hooks} />
        <SignupOtp hooks={hooks} />
        <SignupSuccess hooks={hooks} />
        <VerificationOption hooks={hooks} />
        <VerificationOTP hooks={hooks} />
      </Wrapper>
    </Drawer>
  )
}

export default Login