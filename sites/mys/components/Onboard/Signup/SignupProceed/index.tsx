import * as React from "react"
import { makeStyles } from "@material-ui/core/styles"
import styled from "styled-components"
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import BtnDefault from "omhwebui/components/Button/default"
import Header from "../../header"
// localization
import LANG from '../../../../localization'
// config
import { device } from '../../../../src/media-queries'
//assets
import Artwork from "../../assets/artwork"

// styled
// styled
const Container = styled(Grid)`
  padding-top: 0px;
`
const Heading = styled(Box)`
  & > div > .MuiTypography-root:nth-child(1) {
    padding: 0 50px 5px;
    line-height: 1.3 !important;

    @media ${device.mobileL} {
      padding: 0 50px 5px;
    }
  }
`
const ArtContent = styled(Box)`
  width: 197px;
  height: 150px;

  @media ${device.mobileL} {
    width: 242px;
    height: 182px;
  }

  svg {
    width: 100%;
    height: 100%;
  }
`

function SignupProceed({ hooks }) {
  const {
    pageCountry,
    screen,
    setView,
  } = hooks

  const {
    TITLE,
    SUBTITLE,
  } = LANG[pageCountry].LANG.SIGNUP.PROCEED

  const useStyles = makeStyles(theme => ({
    link: {
      fontSize: "15px",
      lineHeight: "18px",
      textAlign: "center",
      color: "#9E9E9E",
      display: 'block'
    }
  }))
  const classes = useStyles({})

  function renderView() {
    if(screen.current !== "signup_proceed") {
      return null
    }
    return (
      <Container
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{
          justifyContent: "center",
          display: "flex",
          height: "100%"
        }}
      >
        <ArtContent>
          <Artwork />
        </ArtContent>
        <Heading>
          <Header
            title={TITLE}
            subtitle={SUBTITLE}
          />
        </Heading>
        <Box width={"100%"}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Box
              width={202}
              marginBottom={"20px"}
            >
              <BtnDefault
                className={''}
                variant="contained"
                text="Continue"
                color="primary"
                action={() => setView('signup_selectrole')}
              />
            </Box>
            <Box
              width={202}
            >
              <Link
                href="#"
                className={classes.link}
              >
                Contact support
            </Link>
            </Box>
          </Grid>
        </Box>
      </Container>
    )
  }

  return (
    renderView()
  )
}

export default SignupProceed