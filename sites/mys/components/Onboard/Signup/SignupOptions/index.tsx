import * as React from "react"
import styled from 'styled-components'
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import BtnWide from "omhwebui/components/Button/wide"
import GoogleLogin from '../../components/google'
import FacebookLogin from '../../components/facebook'
import Header from "../../header"
// localization
import LANG from '../../../../localization'
// config
import OnboardService from '../../../../services/api/onboard'
import { device } from '../../../../src/media-queries'
import { colors } from '../../../../src/vars'
//assets
import Artwork from "../../assets/artwork"

// styled components
const Container = styled(Grid)`
  padding-top: 0px;
`
const ArtContent = styled(Box)`
  width: 197px;
  height: 150px;

  @media ${device.mobileL} {
      width: 242px;
      height: 182px;
  }

  svg {
    width: 100%;
    height: 100%;
  }
`

function SignupOptions({ hooks }) {
  const {
    token,
    setToken,
    social,
    setSocial,
    setUserData,
    setBearer,
    drawerToggle,
    pageCountry,
    screen,
    setView
  } = hooks

  const {
    TITLE,
    SUBTITLE,
    HAS_ACCOUNT,
    BY_SIGNING,
    TERMS,
  } = LANG[pageCountry].LANG.SIGNUP.OPTIONS

  const {
    BACK,
    LOG_IN
  } = LANG[pageCountry].LANG.COMMON

  const [isFacebookActive, setIsFacebookActive] = React.useState(false)
  const [isGoogleActive, setIsGoogleActive] = React.useState(false)
  const [loading, setLoading] = React.useState(false)

  function loginFacebook(response) {
    setLoading(true)
    setUserData({
      email: response.email,
      firstName: response.first_name,
      lastName: response.last_name,
    })
    setIsFacebookActive(false)
    try {
      OnboardService
        .loginFacebook({ "token": response.accessToken })
        .then(response => {
          setSocial('facebook')
          setLoading(false)

          if(response && response.data._data.token !== undefined) {
            setToken(response.data._data.token)
            setView("signup_socialverify", screen.current)
          }

          if(response.data._data.user.altMobile !== undefined) {
            console.log('unverified')
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("verification_option", "")
          } else {
            setUserData(response.data._data.user)
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("signup_success", "")
            drawerToggle(false)
          }
        })
    } catch {
      console.error('error')
    }
  }

  function loginGoogle(response) {
    console.log(response)
    setLoading(true)
    try {
      OnboardService
        .loginGoogle({ "token": response.tokenId })
        .then(response => {
          setSocial('google')
          setLoading(false)

          if(response.data._data && response.data._data.token !== undefined) {
            setToken(response.data._data.token)
            setView("signup_socialverify", screen.current)
          }

          if(response.data._data.user.altMobile !== undefined) {
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("verification_option", "")
          } else {
            setUserData(response.data._data.user)
            setBearer(response.headers["x-amzn-remapped-authorization"])
            setView("signup_success", "")
            drawerToggle(false)
          }

        })
    } catch {
      console.error('error')
    }
  }

  function loadingView() {
    return (
      <h2>sample, as in just a sample loading.....</h2>
    )
  }

  function renderView() {
    if(screen.current !== 'signup_home') {
      return null
    }

    return (
      <Container
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{
          display: "flex",
          justifyContent: "center",
          height: "100%",
        }}
      >
        <ArtContent>
          <Artwork />
        </ArtContent>
        <Header
          title={TITLE}
          subtitle={SUBTITLE}
        />
        <Box width={"100%"}>
          <div
            className="btn-groups"
          >
            <FacebookLogin
              isFacebookActive={isFacebookActive}
              loginFacebook={loginFacebook}
              setIsFacebookActive={setIsFacebookActive}
              label="Sign up with Facebook"
            />
            <GoogleLogin
              loginGoogle={loginGoogle}
              label="Sign up with Google"
            />
            <BtnWide
              className={null}
              action={() =>
                setView("signup_proper", screen.current)}
              variant="outlined"
              text="Sign up with Mobile"
            />
          </div>
        </Box>

        <div>
          <footer
            className="footer-links"
          >
            <div
              className="link-have-account"
            >
              {HAS_ACCOUNT} <div className="footer-link-item" onClick={() => setView("login_home")}>{LOG_IN}</div>
            </div>
            <div
              className="link-signing-up"
            >
              {BY_SIGNING}
            </div>
            <div
              className="link-terms-privacy"
            >
              {TERMS}
            </div>
          </footer>
        </div>

        <style jsx>{`
          .btn-groups {
            display: flex;
            flex-direction: column;
            justify-content: center;
            alignItems: center;
          }

          .btn-container {
            width: 100% !important;
          }
          
          span {
            width: 100%;
          }

          button {
            width: 100%;
            margin: 0 0 20px 0;
            min-height: 45px;
            font-size: 14px;
            border-radius: 4px !important;
            font-family: inherit !important;
            font-weight: 500 !important;
            padding: 0 !important;
            box-sizing: content-box;
          }

          .footer-links {
            margin-top: 20px;
            color: #000000;
            font-size: 14px;
            display: flex;
            flex-direction: column;
            align-items: center;
          }

          .footer-link { display: flex; }

          .footer-link-item {
            padding-left: 6px;
            color: ${colors.primary};
            cursor: pointer;
          }

          .link-have-account { display: flex; }
          .link-signing-up {
            font-size: 12px;
            color: #757575;
            padding: 25px 0 5px;
          }

          .link-terms-privacy {
            font-size: 12px;
          }

          .link-terms-privacy span {
            font-size: 12px;
            color: #757575;
          }

          @media ${device.mobileL} {
            .footer-links {
              font-size: 15px;
            }
          }
        `}</style>
      </Container>
    )
  }

  return (
    <React.Fragment>
      {loading
        ? loadingView()
        : renderView()}
    </React.Fragment>
  )
}

export default SignupOptions