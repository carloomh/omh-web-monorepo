import * as React from "react"
import styled from 'styled-components'
// components
import Button from "@material-ui/core/Button"
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import Header from "../../header"
import BtnDefault from "omhwebui/components/Button/default"
// localization
import LANG from '../../../../localization'
// config
import OnboardService from '../../../../services/api/onboard'
// assets
import iconSeller from '../../assets/icon_seller'
import iconBuyer from '../../assets/icon_buyer'
import iconLandlord from '../../assets/icon_landlord'
import iconTenant from '../../assets/icon_tenant'
import { device } from '../../../../src/media-queries'
// styled
const Container = styled(Grid)`
  padding-top: 0px;
`
const ButtonRole = styled(Button)`
  width: 100% !important;
  height: 76px !important;
  border: 1px solid #DADCDF !important;
  margin-bottom: 20px !important;

  &.active {
    border: 2px solid #EE620F !important;
    background-color: transparent !important;
  }

  &:hover {
    border: 2px solid #EE620F !important;
    background-color: transparent !important;
  }

  .MuiButton-label {
    display: flex;
    flex-direction: row;
    justify-content: flex-start;
    padding-left: 20px;
  }
`
const Title = styled.div`
  font-size: 14px;
`
const SubTitle = styled.div`
  font-size: 12px;
  font-weight: 300;
  line-height: 17px;
`

const btnDetails = {
  'seller': {
    icon: iconSeller,
    title: 'Seller',
    subtitle: 'I would like to sell my property',
    value: 1
  },
  'buyer': {
    icon: iconBuyer,
    title: 'Buyer',
    subtitle: 'I am looking for a property',
    value: 2
  },
  'landlord': {
    icon: iconLandlord,
    title: 'Landlord',
    subtitle: 'I am looking to rent out my property',
    value: 3
  },
  'tenant': {
    icon: iconTenant,
    title: 'Tenant',
    subtitle: 'I would like to rent a room',
    value: 4
  }
}


function SignupSelectRole({ hooks }) {
  const {
    userData,
    bearer,
    pageCountry,
    screen,
    setView,
  } = hooks

  const {
    TITLE,
    SUBTITLE,
  } = LANG[pageCountry].LANG.SIGNUP.SELECTROLE

  const [userType, setUserType] = React.useState(1)

  function userSignup() {
    const params = {
      "ethnicities": [],
      "firstName": userData.firstName || userData.givenName,
      "genderType": userData.genderType || 0,
      "languages": [],
      "lastName": userData.lastName || userData.familyName,
      "maritalStatusType": userData.maritalStatusType || 0,
      "nationalities": [],
      "userType": userType
    }

    OnboardService
      .addUserRole(params, bearer)
      .then(response => {
        setView("signup_shoutout")
      })
  }

  function renderView() {
    if(screen.current !== "signup_selectrole") {
      return null
    }
    return (
      <Container
        container
        direction="column"
      >
        <Header
          title={TITLE}
          subtitle={SUBTITLE}
          hasAlign
        />
        <Box
          width={"100%"}
          marginBottom={"20px"}
        >
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            {Object.keys(btnDetails).map(index => (
              <ButtonRole
                onClick={() => setUserType(btnDetails[index].value)}
                className={userType === btnDetails[index].value ? 'active' : ''}
              >
                <Box
                  width={40}
                >
                  {btnDetails[index].icon}
                </Box>
                <Box
                  display="flex"
                  flexDirection="column"
                  alignItems="flex-start"
                  ml={2}
                >
                  <Title>{btnDetails[index].title}</Title>
                  <SubTitle>{btnDetails[index].subtitle}</SubTitle>
                </Box>
              </ButtonRole>
            ))}
          </Grid>
        </Box>
        <Box
          display="flex"
          justifyContent="flex-end"
        >
          <Box
            width={94}
            height={44}
          >
            <BtnDefault
              type="submit"
              variant="contained"
              text="Sign up"
              color="primary"
              action={() => userSignup()}
            />
          </Box>
        </Box>
      </Container>
    )
  }

  return (
    renderView()
  )
}

export default SignupSelectRole