import * as React from "react"
import { makeStyles } from "@material-ui/core/styles"
import styled from 'styled-components'
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import BtnDefault from "omhwebui/components/Button/default"
import Header from "../../header"
// localization
import LANG from '../../../../localization'
//assets
import Artwork from "../../assets/artwork"
import { device } from '../../../../src/media-queries'
// styled
const Container = styled(Grid)`
  padding-top: 0px;
`

function SignupShoutout({ hooks }) {
  const {
    pageCountry,
    drawerToggle,
    screen,
    setView,
  } = hooks

  const {
    TITLE,
    SUBTITLE,
  } = LANG[pageCountry].LANG.SIGNUP.SHOUTOUT

  const useStyles = makeStyles(theme => ({
    link: {
      fontSize: "15px",
      lineHeight: "18px",
      textAlign: "center",
      color: "#9E9E9E",
      display: 'block'
    }
  }))
  const classes = useStyles({})

  function renderView() {
    if(screen.current !== "signup_shoutout") {
      return null
    }
    return (
      <Container
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{
          justifyContent: "center",
          display: "flex",
          height: "100%"
        }}
      >
        <Box>
          {Artwork}
        </Box>
        <Header
          title={TITLE}
          subtitle={SUBTITLE}
        />
        <Box width={"100%"}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Box
              width={202}
              marginBottom={"20px"}
            >
              <BtnDefault
                className={''}
                variant="contained"
                text="Post shoutout"
                color="primary"
                action={() => console.log('post a shoutout')}
              />
            </Box>
            <Box
              width={202}
            >
              <Link
                href="#"
                onClick={() => drawerToggle(true)}
                className={classes.link}
              >
                Skip for now
            </Link>
            </Box>
          </Grid>
        </Box>
      </Container>
    )
  }

  return (
    renderView()
  )
}

export default SignupShoutout