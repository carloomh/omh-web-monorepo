import * as React from "react"
import { makeStyles } from "@material-ui/core/styles"
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import Header from "../../header"
//assets
import Artwork from "../../assets/artwork"

function SignupSuccess({ hooks }) {
  const {
    screen
  } = hooks

  function renderView() {
    if(screen.current !== "signup_success") {
      return null
    }

    return (
      <Grid
        container
        direction="column"
        justify="center"
        alignItems="center"
      >
        <Box>
          {Artwork}
        </Box>
        <Box width={"100%"}>
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Header
              title={"Test Success"}
              subtitle={"This is a test view."}
            />
          </Grid>
        </Box>
      </Grid>
    )
  }

  return (
    renderView()
  )
}

export default SignupSuccess