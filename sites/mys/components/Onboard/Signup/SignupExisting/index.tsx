import * as React from "react"
import { makeStyles } from "@material-ui/core/styles"
import styled from 'styled-components'
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import BtnDefault from "omhwebui/components/Button/default"
import Header from "../../header"
// localization
import LANG from '../../../../localization'
//assets
import Artwork from "../../assets/artwork"
// config
import OnboardService from '../../../../services/api/onboard'
import { device } from '../../../../src/media-queries'

// styled
const Container = styled(Grid)`
  padding-top: 0px;
`
const ArtContent = styled(Box)`
  width: 197px;
  height: 150px;

  @media ${device.mobileL} {
      width: 242px;
      height: 182px;
  }

  svg {
    width: 100%;
    height: 100%;
  }
`

function SignupExisting({ hooks }) {
  const {
    setToken,
    token,
    bearer,
    setBearer,
    setUserData,
    pageCountry,
    screen,
    setView,
  } = hooks

  const {
    TITLE,
    SUBTITLE,
  } = LANG[pageCountry].LANG.SIGNUP.EXISTING

  const {
    BACK,
  } = LANG[pageCountry].LANG.COMMON

  function proceedSignup() {
    OnboardService
      .verifyConfirm(token)
      .then(response => {
        console.log(response)
        if(response.data._data.token) {
          setToken(response.data._data.token)
        }
        setUserData(response.data._data.user)
        setBearer(response.headers["x-amzn-remapped-authorization"])
        setView("signup_proceed")
      })
  }

  function renderView() {
    if(screen.current !== "signup_existing") {
      return null
    }
    return (
      <Container
        container
        direction="column"
        justify="center"
        alignItems="center"
        style={{
          justifyContent: "center",
          display: "flex",
          height: "100%"
        }}
      >
        <ArtContent>
          <Artwork />
        </ArtContent>
        <Header
          title={TITLE}
          subtitle={SUBTITLE}
        />
        <Box
          width={"100%"}
        >
          <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
          >
            <Box
              width={202}
              marginBottom={"20px"}
            >
              <BtnDefault
                className={''}
                variant="contained"
                text="Go to Login"
                color="primary"
                action={() => setView("login_home")}
                fullWidth={true}

              />
            </Box>
            <Box
              width={202}
            >
              <BtnDefault
                className={null}
                variant="outlined"
                color="primary"
                text="Proceed with sign up"
                action={() => proceedSignup()}
                fullWidth={true}
              />
            </Box>
          </Grid>
        </Box>
      </Container>
    )
  }

  return (
    renderView()
  )
}

export default SignupExisting