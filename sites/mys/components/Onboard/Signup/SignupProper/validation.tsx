import * as Yup from 'yup'

const Mobile = {
  'SGP': {
    min: 8,
    max: 8
  },
  'DEFAULT': {
    min: 5,
    max: 15
  }
}

export function SignupValidation(value) {
  return (
    Yup.object().shape({
      mobileNumber: Yup.
        string()
        .min(Mobile[value].min, 'Invalid mobile number. Please try again.')
        .max(Mobile[value].max, 'Invalid mobile number. Please try again.')
        .required('Invalid mobile number'),
      emailAddress: Yup
        .string()
        .email()
        .max(100, 'Email Address too long.')
        .required('Invalid email address'),
      firstName: Yup
        .string()
        .max(50, 'First Name too long.')
        .required('Required'),
      lastName: Yup
        .string()
        .max(50, 'Last Name too long.')
        .required('Required'),
      password: Yup
        .string()
        .oneOf(
          [Yup.ref('passwordConfirm'), null],
          ''
        )
        .required('Password is required'),
      passwordConfirm: Yup
        .string()
        .oneOf(
          [Yup.ref('password'), null],
          'Your password and confirm password do not match. Please try again.'
        )
        .required('Required')
    })
  )
}
