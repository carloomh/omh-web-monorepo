import * as React from "react"
import styled from 'styled-components'
import { makeStyles } from "@material-ui/core/styles"
import { Formik } from 'formik'
import * as Yup from 'yup'
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import { MobileInput } from "omhwebui"
import { TextInput } from "omhwebui"
import { InputPassword } from "omhwebui"
import Header from "../../header"
import BtnDefault from "omhwebui/components/Button/default"
// localization
import LANG from '../../../../localization'
// config
import OnboardService from '../../../../services/api/onboard'
import { device } from '../../../../src/media-queries'
import { SignupValidation } from './validation'

// styled
const Container = styled(Grid)`
  padding-top: 0px;
`

const HelperText = styled.small`
  color: #9B9B9B;
  font-size: 12px;
  display: flex;
  padding: 5px 0 0 10px;
`

function SignupProper({ hooks }) {
  const {
    country,
    countryList,
    token,
    setToken,
    setCountry,
    setSavedParams,
    formData,
    setFormData,
    pageCountry,
    screen,
    setView,
    resetFormData,
  } = hooks

  const {
    TITLE,
    SUBTITLE,
    HELPER,
  } = LANG[pageCountry].LANG.SIGNUP.PROPER

  const {
    BACK,
  } = LANG[pageCountry].LANG.COMMON

  const useStyles = makeStyles(theme => ({}))
  const classes = useStyles({})

  const [passwordValues, setPasswordValues] = React.useState({
    password: "",
    showPassword: false,
  })
  const [confirmPasswordValues, setConfirmPasswordValues] = React.useState({
    confirmPassword: "",
    showConfirmPassword: false,
  })

  const toggleDefaultPassword = () => {
    setPasswordValues({
      ...passwordValues,
      showPassword: !passwordValues.showPassword
    })
  }

  const toggleConfirmPassword = () => {
    setConfirmPasswordValues({
      ...confirmPasswordValues,
      showConfirmPassword: !confirmPasswordValues.showConfirmPassword
    })
  }

  const saveValue = (key, value) => {
    setFormData({ ...formData, [key]: value })
  }

  function renderView() {
    if(screen.current !== 'signup_proper') {
      return null
    }
    return (
      <Container
        container
        direction="column"
      >
        <Header
          title={TITLE}
          subtitle={SUBTITLE}
          hasAlign
        />
        <Box width={"100%"} mb={3}>
          <Grid
            container
            direction="column"
          >
            <Formik
              initialValues={{
                passwordValues,
                confirmPasswordValues,
                countryCode: "",
                mobileNumber: formData.mobileNumber || "",
                password: "",
                firstName: formData.firstName || "",
                lastName: formData.lastName || "",
                emailAddress: formData.emailAddress || "",
              }}
              onSubmit={(values, { setSubmitting, resetForm }) => {
                const params = {
                  "countryCode": `${countryList[country].code}`,
                  "mobile": `+${countryList[country].phone}${values.mobileNumber}`,
                  "password": values.password,
                  "firstName": values.firstName,
                  "lastName": values.lastName,
                  "email": values.emailAddress,
                }
                setSavedParams(params)
                OnboardService
                  .signup(params)
                  .then((response) => {
                    setToken(response.data._data.token)
                    setView('signup_otp', screen.current)
                    resetFormData()
                    resetForm()
                  });
              }}
              validationSchema={SignupValidation(
                countryList[country].code === undefined ||
                  countryList[country].code !== "SGP" ? "DEFAULT" : "SGP")}
            >
              {props => {
                const {
                  values,
                  touched,
                  errors,
                  dirty,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  handleReset,
                } = props
                return (
                  <form onSubmit={handleSubmit}>
                    <Box>
                      <Box mb={3}>
                        <TextInput
                          name="firstName"
                          label="First name"
                          type="text"
                          value={formData.firstName}
                          saveValue={saveValue}
                        />
                      </Box>
                      <Box mb={3}>
                        <TextInput
                          name="lastName"
                          label="Last name"
                          type="text"
                          value={formData.lastName}
                          saveValue={saveValue}
                        />
                      </Box>
                    </Box>
                    <Box mb={3}>
                      <TextInput
                        name="emailAddress"
                        label="Email"
                        type="email"
                        value={formData.emailAddress}
                        saveValue={saveValue}
                      />
                      {!errors.emailAddress && (<HelperText>{HELPER.EMAIL}</HelperText>)}
                    </Box>
                    <Box mb={3}>
                      <MobileInput
                        name="mobileNumber"
                        label="Mobile Number"
                        type="tel"
                        country={country}
                        setCountry={setCountry}
                        countryList={countryList}
                        value={formData.mobileNumber}
                        saveValue={saveValue}
                      />
                      {!errors.mobileNumber && (<HelperText>{HELPER.MOBILE}</HelperText>)}
                    </Box>
                    <Box mb={3}>
                      <InputPassword
                        name="password"
                        type="text"
                        label="Password"
                        value={passwordValues.password}
                        showPassword={passwordValues.showPassword}
                        togglePassword={toggleDefaultPassword}
                      />
                    </Box>
                    <Box mb={3}>
                      <InputPassword
                        name="passwordConfirm"
                        type="text"
                        label="Confirm password"
                        value={confirmPasswordValues.confirmPassword}
                        showPassword={confirmPasswordValues.showConfirmPassword}
                        togglePassword={toggleConfirmPassword}
                      />
                    </Box>
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Link onClick={() => setView(screen.previous, screen.current)}>{BACK}</Link>
                      <Box>
                        <BtnDefault
                          isSubmitting={isSubmitting}
                          variant="contained"
                          text="Continue"
                          color="primary"
                          action={() => handleSubmit()}
                        />
                      </Box>
                    </Box>
                  </form>
                )
              }}
            </Formik>
          </Grid>
        </Box>
      </Container>
    )
  }

  return (
    renderView()
  )
}

export default SignupProper