import React from 'react'
import FacebookLogin from 'react-facebook-login/dist/facebook-login-render-props'
import BtnSocial from "omhwebui/components/Button/social"

const fbAppId = process.env.OMH_APP_FACEBOOK

type IProps = {
  isFacebookActive: boolean;
  loginFacebook: Function;
  setIsFacebookActive: Function;
  label: string;
}

const FacebookButton: React.FC<IProps> = ({
  isFacebookActive,
  loginFacebook,
  setIsFacebookActive,
  label
}) => {
  return (
    <React.Fragment>
      {
        isFacebookActive
          ?
          <FacebookLogin
            appId={"1847491042157912"}
            fields="name,email,picture,first_name,last_name"
            onClick={(e: Object) => loginFacebook(e)}
            callback={(e: Object) => loginFacebook(e)}
            autoLoad
            responseType="code"
            scope="public_profile,email"
            render={
              renderProps => (
                <BtnSocial
                  icon="facebook"
                  variant="contained"
                  text={label}
                />
              )
            }
          />
          :
          <BtnSocial
            icon="facebook"
            variant="contained"
            text={label}
            action={() => setIsFacebookActive(true)}
          />
      }
    </React.Fragment>
  )
}

export default FacebookButton