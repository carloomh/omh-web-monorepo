import React from 'react'
import GoogleLogin from 'react-google-login'
import BtnSocial from "omhwebui/components/Button/social"

const googleAppId = process.env.OMH_APP_GOOGLE

function GoogleButton({
  loginGoogle,
  label
}) {
  return (
    <GoogleLogin
      clientId={"566264081176-ktt7qns23umod9tff7680pp54tib6f2b.apps.googleusercontent.com"}
      buttonText="Login"
      onSuccess={(e) => loginGoogle(e)}
      onFailure={(e) => console.error(e)}
      cookiePolicy={'single_host_origin'}
      responseType="id_token"
      render={
        renderProps => (
          <BtnSocial
            icon="google"
            variant="contained"
            text={label}
            action={renderProps.onClick}
          />
        )
      }
    />
  )
}

export default GoogleButton