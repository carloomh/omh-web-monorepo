import * as React from "react"
import styled from 'styled-components'
import { makeStyles } from "@material-ui/core/styles"
import { Formik } from 'formik'
import * as Yup from 'yup'
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import Link from "@material-ui/core/Link"
import { MobileInput } from "omhwebui"
import { TextInput } from "omhwebui"
import { InputPassword } from "omhwebui"

import Header from "../../header"
import BtnDefault from "omhwebui/components/Button/default"
// localization
import LANG from '../../../../localization'
// config
import OnboardService from '../../../../services/api/onboard'
import { device } from '../../../../src/media-queries'
import { VerifyValidation } from './validation'
// styled
const Container = styled(Grid)`
  padding-top: 0px;

  @media ${device.mobileL} {
    padding-top: 180px;
  }
`

const HelperText = styled.small`
  color: #9B9B9B;
  font-size: 12px;
  line-height: 32px;
  display: flex;
  justify-content: center;
`

function VerificationOption({ hooks }) {
  const {
    bearer,
    country,
    setCountry,
    token,
    setToken,
    social,
    countryList,
    userData,
    formData,
    setFormData,
    pageCountry,
    screen,
    setView,
  } = hooks

  const {
    TITLE,
    SUBTITLE,
    HELPER,
  } = LANG[pageCountry].LANG.SIGNUP.SOCIAL_VERIFY

  const {
    BACK,
  } = LANG[pageCountry].LANG.COMMON

  const saveValue = (value: string) => {
    setFormData({ ...formData, mobile: value })
  }

  function handleSocial(type: string, params: Object) {
    if(type === 'facebook') {
      OnboardService
        .updateMobile(params, bearer)
        .then((response) => {
          setView('verification_otp', screen.current)
          setToken(response.data._data.token)
        })
    } else {
      OnboardService
        .updateMobile(params, bearer)
        .then((response) => {
          setView('verification_otp', screen.current)
          setToken(response.data._data.token)
        })
    }
  }

  function renderView() {
    if(screen.current !== "verification_option") {
      return null
    }
    return (
      <Container
        container
        direction="column"
      >
        <Header
          title={TITLE}
          subtitle={SUBTITLE}
          hasAlign
        />
        <Box width={"100%"} mb={3}>
          <Grid
            container
            direction="column"
          >
            <Formik
              initialValues={{
                countryCode: "",
                mobileNumber: "",
              }}
              onSubmit={(values, { setSubmitting }) => {
                const params = {
                  "countryCode": `${countryList[country].code}`,
                  "mobile": `${countryList[country].phone}${values.mobileNumber}`,
                }
                handleSocial(social, params)
              }}
              validationSchema={VerifyValidation(
                countryList[country] === undefined ||
                  countryList[country].code !== "SGP" ? "DEFAULT" : "SGP")}
            >
              {props => {
                const {
                  values,
                  touched,
                  errors,
                  dirty,
                  isSubmitting,
                  handleChange,
                  handleBlur,
                  handleSubmit,
                  handleReset,
                } = props
                return (
                  <form onSubmit={handleSubmit}>
                    <Box mb={3}>
                      <MobileInput
                        name="mobileNumber"
                        label="Mobile Number"
                        type="tel"
                        country={country}
                        setCountry={setCountry}
                        countryList={countryList}
                        value={formData.mobileNumber}
                        saveValue={saveValue}
                      />
                      {!errors.mobileNumber && (<HelperText>{HELPER.MOBILE}</HelperText>)}
                    </Box>
                    <Box
                      display="flex"
                      justifyContent="space-between"
                      alignItems="center"
                    >
                      <Link
                        onClick={() => setView(screen.previous)}
                      >
                        {BACK}
                      </Link>
                      <Box
                        width={94}
                        height={44}
                      >
                        <BtnDefault
                          isSubmitting={isSubmitting}
                          variant="contained"
                          text="Continue"
                          color="primary"
                          action={() => handleSubmit()}
                        />
                      </Box>
                    </Box>
                  </form>
                )
              }}
            </Formik>
          </Grid>
        </Box>
      </Container>
    )
  }

  return (
    renderView()
  )
}

export default VerificationOption