import * as React from "react"
import styled from 'styled-components'
// components
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import FormHelperText from '@material-ui/core/FormHelperText'
import Link from "@material-ui/core/Link"
import TextField from '@material-ui/core/TextField'
import Header from "../../header"
import BtnDefault from 'omhwebui/components/Button/default'
import { Countdown } from "omhwebui"
// localization
import LANG from '../../../../localization'
// config
import OnboardService from '../../../../services/api/onboard'
import { device } from '../../../../src/media-queries'
// styled
const Container = styled(Grid)`
  padding-top: 0px;

  @media ${device.mobileL} {
    padding-top: 180px;
  }
`
const HelperText = styled.small`
  color: #9B9B9B;
  font-size: 12px;
  line-height: 32px;
  display: flex;
  justify-content: center;
`
const Digit = styled(TextField)`
  width: 52px !important;

  input {
    text-align: center;
    font-size: 30px;
    font-weight: 500;
  }
`
const Heading = styled(Box)`
  & > div > .MuiTypography-root:nth-child(2) {
    padding: 0 50px 0 0;

    @media ${device.mobileL} {
      padding: 0 50px;
    }
  }
`

const useForceUpdate = () => React.useState()[1]


function VerificationOTP({ hooks }) {
  const {
    bearer,
    country,
    countryList,
    token,
    savedParams,
    setToken,
    setBearer,
    setUserData,
    pageCountry,
    screen,
    setView,
    resetOTP,
  } = hooks

  const {
    TITLE,
    SUBTITLE,
    RESEND
  } = LANG[pageCountry].LANG.SIGNUP.OTP

  const {
    BACK,
  } = LANG[pageCountry].LANG.COMMON

  const [pin, setPin] = React.useState({
    firstDigit: '',
    secondDigit: '',
    thirdDigit: '',
    fourthDigit: ''
  })
  const [restart, setRestart] = React.useState(false)
  const [isSubmitting, setIsSubmitting] = React.useState(false)
  const [timerCount, setTimerCount] = React.useState(300)
  const [invalidOTP, setInvalidOTP] = React.useState(false)

  const addDigit = (digit: string, value: string, next: string) => {
    setPin({
      ...pin,
      [digit]: value,
    })
    setInvalidOTP(false)
    if(value !== "" && next !== null) {
      document.getElementById(next).focus()
    }
  }

  function resendOTP() {
    return (
      OnboardService
        .signup(savedParams)
        .then((response) => {
          setToken(response.data._data.token)
        })
    )
  }

  function submitOTP() {
    const otp = `${pin.firstDigit}${pin.secondDigit}${pin.thirdDigit}${pin.fourthDigit}`
    setIsSubmitting(true)
    OnboardService
      .updateValidateMobile(otp, bearer)
      .then(response => {
        const { _data } = response.data

        if(_data && _data.user !== undefined) {
          setUserData(response.data._data.user)
          setBearer(response.headers["x-amzn-remapped-authorization"])
          resetOTP()
          setView("signup_success")
        }

        setIsSubmitting(false)
      })
  }

  function renderView() {
    if(screen.current !== 'verification_otp') {
      return null
    }

    const {
      mobile
    } = savedParams

    return (
      <Container
        container
        direction="column"
      >
        <Heading>
          <Header
            title={TITLE}
            subtitle={`${SUBTITLE} ${mobile}`}
            hasAlign
          />
        </Heading>
        <Box width={"100%"} mb={4}>
          <Grid
            container
            direction="row"
            style={{
              flexWrap: "nowrap",
              justifyContent: "space-around"
            }}
          >
            <Digit
              id="firstDigit"
              value={pin.firstDigit}
              onChange={(e) => addDigit("firstDigit", e.target.value, "secondDigit")}
              margin="normal"
              inputProps={{ maxLength: 1 }}
              error={invalidOTP}
              type={'tel'}
            />
            <Digit
              id="secondDigit"
              value={pin.secondDigit}
              onChange={(e) => addDigit("secondDigit", e.target.value, "thirdDigit")}
              margin="normal"
              inputProps={{ maxLength: 1 }}
              error={invalidOTP}
              type={'tel'}
            />
            <Digit
              id="thirdDigit"
              value={pin.thirdDigit}
              onChange={(e) => addDigit("thirdDigit", e.target.value, "fourthDigit")}
              margin="normal"
              inputProps={{ maxLength: 1 }}
              error={invalidOTP}
              type={'tel'}
            />
            <Digit
              id="fourthDigit"
              value={pin.fourthDigit}
              onChange={(e) => addDigit("fourthDigit", e.target.value, null)}
              margin="normal"
              inputProps={{ maxLength: 1 }}
              error={invalidOTP}
              type={'tel'}
            />
          </Grid>
          {invalidOTP && <FormHelperText style={{ textAlign: 'center' }} error={invalidOTP}>{'Invalid OTP. Please try again.'}</FormHelperText>}
        </Box>
        <Box
          display="flex"
          flexDirection="column"
          justifyContent="space-between"
          alignItems="center"
        >
          <Box
            display="flex"
            mb={"60px"}
          >
            <Countdown
              count={timerCount}
              setCount={setTimerCount}
            />
            <span>&nbsp;-&nbsp;</span>
            <div
              onClick={() => resendOTP()}
            >
              {RESEND}
            </div>
          </Box>

          <Box
            display="flex"
            justifyContent="space-between"
            alignItems="center"
            width={"100%"}
          >
            <Box>
              <Link onClick={() =>
                setView(screen.previous)}
              >
                Back
              </Link>
            </Box>
            <Box
              width={94}
              height={44}
            >
              <BtnDefault
                isSubmitting={isSubmitting}
                variant="contained"
                text="Continue"
                color="primary"
                action={() =>
                  isSubmitting
                    ? null
                    : submitOTP()}
              />
            </Box>
          </Box>
        </Box>
      </Container >
    )
  }

  return (
    renderView()
  )
}

export default VerificationOTP