import * as React from "react"

function iconBuyer() {
  return (
    <div>
      <svg width="25" height="22" viewBox="0 0 25 22" fill="none" xmlns="http://www.w3.org/2000/svg"><path fillRule="evenodd" clipRule="evenodd" d="M12.5 0L0 11.2566H3.19448V22H11.0686V14.8385H13.9302V22H21.8043V11.2566H25L12.5 0ZM5.18298 9.1595L12.5 2.56977L19.8946 9.33735V20.0879H15.8412V12.9276H9.15879V20.0879H5.10426L5.18298 9.1595Z" fill="#EE620F" /></svg>
    </div>
  )
}

export default iconBuyer