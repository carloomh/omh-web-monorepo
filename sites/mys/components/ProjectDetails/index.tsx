import HeaderDetails from './components/header-details'
import Navigation from './components/navigation'
import Gallery from './components/gallery'
import About from './components/about'
import FloorPlan from './components/floor-plan'
import Infrastructures from './components/infrastructures'
import Vicinities from './components/vicinities'
import FreeRide from './components/free-ride'
import SimilarProjects from './components/similar-projects'
// import ContactAgent from './contact-agent'
import ProjectInfo from './components/project-info'
import AgentTestimonial from './components/agent-testimonial'
// import UpToDate from './uptodate'
export {
  HeaderDetails,
  Navigation,
  Gallery,
  About,
  FloorPlan,
  Infrastructures,
  Vicinities,
  FreeRide,
  SimilarProjects,
  // ContactAgent,
  ProjectInfo,
  AgentTestimonial,
  // UpToDate
}