import * as React from 'react'
import styled from 'styled-components'
import { SlickSlider, ImageLightbox } from "omhwebui"

const Container = styled.section`
  margin: 0px;

  @media (min-width : 1128px) {
    margin-bottom: 40px;
  }
`
const GalleryContainer = styled.div`
  display: flex;
  justify-content: flex-start;
  width: 100%;
  overflow: auto;
  padding: 10px;
  overflow-x: scroll;
  overflow-y: hidden;
  box-sizing: border-box;

  @media (min-width : 1128px) {
    padding: 10px 0 0;
    overflow: hidden;
  }
`

const Album = styled.section`
  width: 85px;
  height: 85px;
  margin-right: 15px;
  box-sizing: border-box;
  cursor: pointer;
  position: relative;
  border-radius: 7px;
  overflow: hidden;


  &:before{
    content: '';
    background: rgba(39, 39, 39, 0.4);
    height: 100%;
    width: 100%;
    position: absolute;
    z-index: 2;
    top: 0;
    opacity: 1;
    left: 0;
    transition: opacity 200ms ease-in-out;
  }

  &:last-child{
    margin-right: 0;
  }

  @media (min-width : 1128px) {
    min-width: 134px;
    height: 100%;
    border: 0;

    &:before{
      display: none;
    }
  }
`
const AlbumCover = styled.div`
  width: 100%;
  height: 100%;
  border-radius: 5px;
  margin-bottom: 10px;

  @media (min-width : 1128px) {
    height: 125px;
  }
`
const AlbumName = styled.p`
  margin: 0;
  text-align: center;
  font-family: Roboto, sans-serif;
  font-size: 14px;
  position: absolute;
  transform: translate(-50%, -50%);
  top: 50%;
  left: 50%;
  color: #FFFFFF;
  position: absolute;
  z-index: 99;

  @media (min-width : 1128px) {
    font-size: 16px;
    position: relative;
    transform: none;    
    z-index: 1;
    top: auto;
    left: auto;
  }
`

const Counter = styled.div`
  position: absolute;
  top: 15px;
  right: 15px;
  color: #FFFFFF;
  border-radius: 18px;
  background-color: rgba(255, 255, 255, 0.28);
  padding: 8px 13px;
  font-size: 18px;
  line-height: 18px;
  font-weight: 500;
`

interface AlbumsProps {
  galleries: any,
  setAlbum?: Function | null
  current: any,
}

const Albums = ({
  galleries,
  setAlbum,
  current
} : AlbumsProps) => {
  return(
    <GalleryContainer>
      {
        galleries && galleries.map((album, key) => {
          const { title, images } = album
          const display = images[0].imageUrl
          return(
            <Album
              key={key}
              onClick={() => setAlbum(key)}
            >
              <AlbumCover
                style={{
                  background: `center / cover url(${display}?h=320&w=320&c=limit) no-repeat`,
                }}
              />
              <AlbumName>{title}</AlbumName>
            </Album>
          )
        })
      }
    </GalleryContainer>
  )
}

const Gallery = ({
  galleries
}) => {
  const [album, setAlbum] = React.useState(0)
  const [currentImage, setClickImage] = React.useState(null)
  const [isOpen, setLightbox] = React.useState(false)
  const selectedAlbum = galleries && galleries[album]
  const selectedImages = selectedAlbum && selectedAlbum.images


  const handleClickImage = (key) => {
    setClickImage(key)
    setLightbox(!isOpen)
  }

  const handleSlide = value => {
    setClickImage(value)
  }

  const mainSrc = selectedImages != null && selectedImages[currentImage] && selectedImages[currentImage].imageUrl
  const nextSrc = selectedImages != null && selectedImages[currentImage+1] && selectedImages[currentImage+1].imageUrl
  const prevSrc = selectedImages != null && selectedImages[currentImage-1] && selectedImages[currentImage-1].imageUrl
  const albumSize = selectedImages && selectedImages.length || 0

  return (
    <React.Fragment>
      <Container>
        <div
          style={{
            width: '100%',
            position: 'relative'
          }}
        >
          <SlickSlider
            slidesToShow={1}
            slidesToScroll={1}
            afterChange={handleSlide}
          >
            {
              selectedImages && selectedImages.map( (image, key) => {
                return(
                  <div key={key}>
                    <div
                      onClick={() => handleClickImage(key)}
                      style={{
                        background: `center / cover url(${image.imageUrl}) no-repeat`,
                        height: '300px'
                      }}
                    >
                    </div>
                  </div>
                )
              })
            }
          </SlickSlider>
          <Counter>
            {currentImage + 1} / {albumSize}
          </Counter>
        </div>
        <Albums
          galleries={galleries}
          setAlbum={setAlbum}
          current={album}
        />
      </Container>
      {
        mainSrc && isOpen && currentImage != null ?
          <ImageLightbox
            mainSrc={mainSrc}
            nextSrc={selectedImages.length > 1 && nextSrc ? nextSrc : null}
            prevSrc={selectedImages.length > 1 ? prevSrc : null}
            onCloseRequest={() => handleClickImage(null)}
            imagePadding={60}
            reactModalStyle={{
              content: {
                width:  '100%'
              }
            }}
            onMovePrevRequest={() => setClickImage(currentImage - 1) }
            onMoveNextRequest={() => setClickImage(currentImage + 1) }
          />
          : ''
      }
    </React.Fragment>
  )
}

export default Gallery 