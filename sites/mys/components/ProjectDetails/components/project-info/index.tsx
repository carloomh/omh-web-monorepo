import * as React from 'react'
import styled from 'styled-components'
import numeral from 'numeral'
import IconPin from '../../assets/icon_pin'

const ProjectInfoWrapper = styled.div`
  position: relative;
  padding: 20px;

  @media (min-width : 1128px) {
    padding: 0;
    margin-bottom: 20px;
  }
`
const ProjectInfoTitle = styled.h1`
  font-family: Roboto, sans-serif;
  font-weight: bold;
  font-size: 28px;
  line-height: 32px;
  color: #000;
  margin: 0 0 6px;
  display: block;

  @media (min-width : 1128px) {
    font-size: 32px;
    line-height: 34px;
  }
`

const ProjectInfoAddress = styled.span`
  font-family: Roboto,sans-serif;
  font-size: 16px;
  line-height: 19px;
  color: #4A4A4A;
  margin: 0 0 17px;
  display: block;
  opacity: 0.7;

  & svg{
    height: 15px;
    margin-bottom: -2px; 
    width: 15px;
  }
`

const ProjectInfoPrice = styled.h2`
  font-family: Roboto, sans-serif;
  font-size: 24px;
  line-height: 28px;
  font-weight: bold;
  color: #000;
  margin: 0 0 5px;
  display: block;
`

const ProjectInfoPsf = styled.span`
  font-family: Roboto, sans-serif;
  font-size: 16px;
  line-height: 20px;
  opacity: 0.7;
  color: #4A4A4A;
  display: block;
  margin-bottom: 20px;
`

const ProjectInfoHighlightsWrapper = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin-top: 20px;
`

const Item = styled.div`
  display: flex;
  border: 1px solid #D9DDE7;
  border-radius: 3px;	
  box-shadow: 0 2px 4px 0 rgba(0,0,0,0);
  padding: 2px 0;
  margin: 0 10px 10px 0;
`

const ItemIcon = styled.div`
  height: 30px;
  width: 40px;
  padding: 5px 10px;
  border-right: 1px solid #D9DDE7;
`

const ItemName = styled.span`
  font-size: 12px;
  line-height: 18px;
  padding: 0 10px;
  display: flex;
  align-items: center;

  @media (min-width : 1128px) {
    font-size: 15px;
  }
`

const Pin = styled(IconPin)`
  height: 15px;
  margin-bottom: -2px; 
  margin-right: 5px;
`

const ProjectInfo = ({
  details,
  highlights,
  media_path
}) => {

  const {
    address
  } = details

  return (
    <ProjectInfoWrapper>
      <ProjectInfoTitle>{details && details.name}</ProjectInfoTitle>
      { 
        details && address && 
          <ProjectInfoAddress><Pin />{address.addressStreetName}, {address.townName}, {address.stateName}</ProjectInfoAddress>
      }
      {
        details.minAmount &&
        details.maxAmount &&
        <ProjectInfoPrice>RM {numeral(details.minAmount).format('0,0')} - RM {numeral(details.maxAmount).format('RM0,0') }</ProjectInfoPrice>
      }
      <ProjectInfoPsf>RM {numeral(details.minAmount).format('0,0')} psft</ProjectInfoPsf>
      {
        highlights &&
        <ProjectInfoHighlightsWrapper>
          {
            highlights.map((item, index) => {
              return(
                <Item key={index}>
                  <ItemIcon 
                    style={{
                      background: `center / 70% url(${item.iconUrl}) no-repeat`
                    }}
                  />
                  <ItemName>{ item.title}</ItemName>
                </Item>
              )
            })
          }
        </ProjectInfoHighlightsWrapper>
      }
    </ProjectInfoWrapper>

  )
}

export default ProjectInfo 