import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import { SlickSlider, ImageLightbox } from "omhwebui"
import Facilities from '../facilities'
import './style.scss'

const Container = styled.section`
  margin-bottom: 30px;

  &:before{
    content: '';
    margin-top: -120px;
    height: 120px;
    visibility: hidden; 
    pointer-events: none;
    display: block; 
  }
`

const Header = styled.header`
  font-size: 22px;
  font-weight: 500;
  margin: 0 0 22px;
  padding: 0 20px;
  
  @media (min-width : 1128px) {
    margin: 0 0 22px;
    padding: 0;
  }
`

const Content = styled.div`
  border: 1px solid #D9DDE7;

  @media (min-width : 1128px) {
    border-radius: 3px;
  }
`

const SiteNav = styled.div`
  display: flex;
  background: #F9F9FA;
  border-bottom: 1px #D9DDE7 solid;
  overflow: scroll;
  width: 100vw;

  & a {
    padding: 12px 20px;
    color: #9B9B9B;
    font-size: 16px;
    cursor: pointer;
    font-weight: 500;
    white-space: nowrap;

    &.active{
      color: #E55710;
      border-bottom: 2px #E55710 solid;
    }
  }

  @media (min-width : 1128px) {
    width: auto;
    overflow: hidden;
  }
`

const Map = styled.div`
  padding: 22px;
`
const SiteMapItem = styled.div`
  min-height: 300px;
  max-height: 500px;
  cursor: pointer;
  outline: none;
`
const MapItem = styled.div`
  height: 100%;
  min-height: 300px;
  max-height: 500px;
  width: auto;
  margin-bottom: 20px;

  &:last-child{
    margin: 0;
  }
`

interface VicinitiesProps {
  vicinities: any,
  media_path:any,
  facilities:any,
}

const Vicinities = ({
  vicinities,
  media_path,
  facilities
} : VicinitiesProps) => {

  const [active, setActive] = React.useState(0)
  const [isOpen, setOpen] = React.useState(false)
  const [activeImage, handleClickImage] = React.useState(null)

  const handleActiveImage = (url) => {
    handleClickImage(url)
    setOpen(!isOpen)
  }

  const activeGroup = vicinities && vicinities[active] && vicinities[active].images

  return(
    <Fragment>
        <Container id={'site_map'}>
          <Header>Site Map</Header>
          <Content>
            { activeGroup ? 
              <Fragment>
                <SiteNav>
                  {
                    vicinities && vicinities.length && vicinities.map((item, index) => {
                      const { images } = item
                      const hasImage = images && images.length
                      return(
                        hasImage ?
                          <a
                            key={index}
                            onClick={() => setActive(index)}
                            className={ index === active ? 'active' : '' }
                          >
                            { item.title }
                          </a> : ''
                      )
                    })
                  }
                </SiteNav>
                <Map>
                  <SlickSlider
                    slidesToScroll={1}
                    slidesToShow={1}
                  >
                    {
                      vicinities && activeGroup && activeGroup.length && activeGroup.map((image, index) => {
                        return(
                          <SiteMapItem
                            key={index}
                            onClick={() => handleActiveImage(image.imageUrl)}
                          >
                            <MapItem
                              style={{
                                background: `center / contain url(${encodeURI(image.imageUrl)}?h=1024&w=1024&c=limit) no-repeat`,
                              }}
                            />
                          </SiteMapItem>
                        )
                      })
                    }
                  </SlickSlider>
                </Map>
              </Fragment>
              : ''
            }
            {  
              facilities && facilities.length ?
                <Facilities
                  facilities={facilities}
                  media_path={media_path}
                />
                : ''
            }
          </Content>
        </Container>
        { isOpen &&
          <ImageLightbox
            mainSrc={activeImage}
            onCloseRequest={() => handleActiveImage(activeImage)}
            reactModalStyle={{
              overlay: {
                backgroundColor: '#000000'
              },
              content: {
                width:  '100vw',
              }
            }}
          />
        }
      </Fragment>
  )

}
export default Vicinities