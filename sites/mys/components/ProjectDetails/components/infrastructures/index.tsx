import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import { LocationMap } from "omhwebui"

const Container = styled.section`
  margin-bottom: 40px;
  
  &:before{
    content: '';
    margin-top: -200px;
    height: 200px;
    visibility: hidden; 
    pointer-events: none;
    display: block; 
  }
`

const Header = styled.header`
  font-size: 22px;
  font-weight: 500;
  margin: 0 0 22px;
  padding: 0 20px;

  @media (min-width : 1128px) {
    padding: 0;
  }
`

const Content = styled.div`
  border: 1px solid #D9DDE7;
  
  @media (min-width : 1128px) {
    border-radius: 3px;
  }
`

interface InfrastructuresProps {
  infrastructures: any,
  media_path: any,
  address: any,
  filters: any
}

const Infrastructures = ({
  infrastructures,
  address,
  filters
} : InfrastructuresProps) => {
  
  const {
    latitude,
    longitude
  } = address

  return (
    <Container id={'location'}>
      <Header>Location</Header>
      <Content>
        {
          infrastructures && infrastructures.length ?
          <LocationMap
            markers={infrastructures}
            showList={true}
            filters={filters}
            centerLat={parseFloat(latitude)}
            centerLong={parseFloat(longitude)}
            zoom={4}
          /> : ''
        }
      </Content>
    </Container>
  )
}

export default Infrastructures