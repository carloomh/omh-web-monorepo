
import React from 'react'

const IconPin = ( { fill }) => {
  return(
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="800 9894 82 82">
      <g id="ic_directions_pin_Lgreen" transform="translate(-42 9192)">
        <path fill={fill} id="ic_directions_pin" className="pin-icon" d="M1375.557,562.566a9.029,9.029,0,1,1,9.029-9.029A9.029,9.029,0,0,1,1375.557,562.566Zm0-27.086A17.991,17.991,0,0,0,1357.5,553.4a17.777,17.777,0,0,0,2.007,8.186c3.8,9.617,12.908,20.679,14.981,31.072l1.07,2.818,1.177-2.818c2.073-10.393,11.089-21.4,14.887-31.072a17.72,17.72,0,0,0,1.993-8.186A17.99,17.99,0,0,0,1375.557,535.48Z" transform="translate(-492.499 177.52)"/>
        <rect id="rectangle" fill = 'none' className="cls-2" width="82" height="82" transform="translate(842 702)" />
      </g>
    </svg>
  )
}

export default IconPin