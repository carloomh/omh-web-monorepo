import React, { useState } from 'react'
import styled from 'styled-components'
// import PropTypes from 'prop-types'

const Container = styled.div`
  padding: 22px;
  border-top: 1px #EBEEF4 solid;
`

const Header = styled.header`
  font-size: 18px;
  font-weight: 500;
  line-height: 22px;
  margin: 0 0 16px;
`

const Content = styled.div`
  display: flex;
  flex-wrap: wrap;
`

const Facility = styled.div`
  display: flex;
  border: 1px solid #D9DDE7;
  border-radius: 3px;	
  box-shadow: 0 2px 4px 0 rgba(0,0,0,0);
  padding: 2px 0;
  margin: 0 10px 10px 0;
`

const FacilityIcon = styled.div`
  height: 30px;
  width: 40px;
  padding: 5px 10px;
  border-right: 1px solid #D9DDE7;
`

const ViewIcon = styled.div`
  height: 30px;
  width: 30px;
  transform: rotate(90deg);
  border-right: none;
  border-top: 1px solid #D9DDE7;
  padding: 5px;
  cursor: pointer;
`

const FacilityName = styled.span`
  font-size: 15px;
  line-height: 18px;
  padding: 0 10px;
  display: flex;
  align-items: center;
`

const Facilities = ({ facilities, media_path }) => {
  const [ more, setViewMore ] = useState(6)

  const facilitiesList = facilities && facilities.slice()
  const limitFacilities = facilitiesList && facilitiesList.splice(0, more)

  return (
    <Container>
      <Header>Facilities</Header>
      <Content>
        {
          limitFacilities && limitFacilities.map(( item, index) => {
            const { iconUrl, title } = item
            return (
              title ?
                <Facility key={index}>
                  <FacilityIcon 
                    style={{
                      background: `center / 70% url(${iconUrl ? `${iconUrl}` : '/themes/omhmy/assets/images/icon_facilities.svg'}) no-repeat`
                    }}
                  />
                  <FacilityName>{title}</FacilityName>
                </Facility>
                : ''
            )
          })
        }
        {
          facilities && facilities.length > 6 ?
            <Facility onClick={() => setViewMore(more === facilities.length ? 6: facilities.length)}>
              <ViewIcon
                style={{
                  background: 'center / 70% url(https://misdirection.ohmyhome.com/cms/media/malaysia/icons8-menu-vertical-filled.svg) no-repeat'
                  }}
                />
              <FacilityName>View {more === facilities.length ? 'Less': 'More'}</FacilityName>
            </Facility>
            : ''
        }
      </Content>
    </Container>
  )
}

// Facilities.propTypes = {
//   facilities: PropTypes.array,
// }

export default Facilities