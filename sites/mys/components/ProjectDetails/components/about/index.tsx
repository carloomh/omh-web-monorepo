import React, { useState } from 'react'
import styled from 'styled-components'
import Uptodate from '../uptodate'
import PD_LANG from '../../../../localization'

const Container = styled.section`
  padding: 20px;
  position: relative;

  @media (min-width : 1128px) {
    margin-bottom: 40px;
    padding: 0px;
  }
`

const Header = styled.header`
  font-size: 22px;
  font-weight: 500;
  margin: 0 0 18px;

  @media (min-width : 1128px) {
    margin: 0 0 22px;
  }
`

const Slogan = styled.h5`
  font-size: 16px;
  font-weight: 400;
  line-height: 18px;
  margin: 0 0 10px;

  @media (min-width : 1128px) {
    font-size: 20px;
    line-height: 22px;
  }
`
const Description = styled.p`
  font-size: 14px;
  line-height: 22px;
  margin: 0 0 18px;
  white-space: pre-line;

  @media (min-width : 1128px) {
    font-size: 16px;
    line-height: 24px;
  }
`

const Row = styled.div`
  display: grid;
  grid-template-columns: 100%;

  @media (min-width : 1128px) {
    grid-template-columns: 60% 40%;
  }
`

const Column = styled.div`
  margin-bottom: 10px;
  font-size: 14px;
    display: grid;
    grid-template-columns: auto auto;
    justify-content: flex-start;

  @media (min-width : 1128px) {
    font-size: 16px;
    line-height: 20px;
  }

  & span{
    color: #4A4A4A;
    opacity: 0.7;
    margin-right: 5px;
  }
`

const UpdateLink = styled.a`
  color: #EE620F;
  text-decoration: underline !important;
  cursor: pointer;
`

const View = styled.span`
  color: #e55710;
  cursor: pointer;
`

const ShareIcon = styled.span`
  position: absolute;
  width: 30px;
  height: 30px;
  cursor: pointer;
  top: 20px;
  right: 20px;

  @media (min-width : 1128px) {
    top: 0;
    right: 0;
  }
`

const About = ({
  details,
  pageCountry
}) => {
  const [ view, setView ] = useState(false)
  const [modal, handleUpdates] = React.useState(false)

  // const [ isShareModal, setShareModal ] = useState(false)

  const {
    slogan,
    description,
    developer,
    tenure_type,
    launchDateAt,
    completionDateAt
  } = details

  const {
    ABOUT,
  } = PD_LANG[pageCountry].PD_LANG

  return (
    <Container>
      <Header>{ABOUT.HEADER || 'About the Property'}</Header>
      {/* <ShareIcon onClick={() => setShareModal(true)}><Share /></ShareIcon> */}
      { slogan && <Slogan>{slogan}</Slogan> }
      <Description>
        {
          description && description.length > 400 ? `${description.slice(0, view ? description.length : 400 )}` : description }
        { description && description.length > 400 && <View onClick={ () => setView(!view)}> {view ? 'View Less' : 'View More'}</View>
        }
      </Description>
      <Row>
        <Column>
          <span>{ABOUT.DEVELOPER || 'Property Developer:'}</span> <div>{ developer && developer.name }</div>
        </Column>
        <Column>
          <span>{ABOUT.TENURE || 'Tenure:'}</span> <div>{ tenure_type }</div>
        </Column>
      </Row>
      <Row>
        <Column>
          <span>{ABOUT.TOP || 'TOP:'}</span>
          <div>{ launchDateAt }</div>
          {/* <div>{ moment(launched_date).format('YYYY') }</div> */}
        </Column>
        <Column>
          <span>{ABOUT.COMPLETION || 'Completion Year:'}</span>
          <div>{ completionDateAt }</div>
          {/* <div>{ moment(completion_date).format('YYYY')}</div> */}
        </Column>
        <UpdateLink onClick={ () => handleUpdates(true)}>{ABOUT.UPTODATE || ''}</UpdateLink>
      </Row>
      {/* <ShareModal
        isShareModal={isShareModal}
        handleShare={setShareModal}
        project_details={project_details}
      /> */}
      <Uptodate
        show={modal}
        handleShow={handleUpdates}
      />
    </Container>
  )
}

export default About