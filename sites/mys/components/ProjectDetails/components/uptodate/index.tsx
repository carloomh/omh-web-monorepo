import React from 'react'
import styled from 'styled-components'
import { Formik } from 'formik'
import * as Yup from 'yup'
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import Dialog from '@material-ui/core/Dialog'
import FormHelperText from '@material-ui/core/FormHelperText'
import { TextInput } from "omhwebui"
import CheckSuccess from './check'

import BtnDefault from 'omhwebui/components/Button/default'
import NewLaunchesService from '../../../../services/api/newlaunches'
import Close from './close'

const ModalContainer = styled.div`
  border-radius: 10px;
  background: #FFFFFF;
  padding: 20px;
  width: 425px;
  margin: 25px;
`

const Title = styled.h1`
  font-size: 26px;
  margin: 0 0 10px;
  font-weight: 500;
`

const SubTitle = styled.p`
  font-size: 18px;
  line-height: 28px;
  color: #616161;
  margin: 0;
  letter-spacing: 0.2px;
`

const Form = styled.div`
  margin-top: 20px;
`
const CloseIcon = styled.div`
  position: absolute;
  right: 15px;
  top: 15px;
  cursor: pointer;
`

const Helpertext = styled.span`
  letter-spacing: .1px;
  color: #f44336;
  font-size: 14px;
`

const SuccessContainer = styled.div`
  border-radius: 10px;
  background: #FFFFFF;
  padding: 20px;
  width: 320px;
  margin: 25px;
  text-align: center;
`

const SuccessContent = styled.div`
  text-align: center;
  color: #000000;
  font-family: Roboto;
  margin-top: 20px;

  & h2{
    font-size: 26px;
    font-weight: 500;
    line-height: 36px;
    margin: 0 0 20px;
  }

  & p{
    color: #616161;
    font-family: Roboto;
    font-size: 17px;
    line-height: 26px;
    margin: 0 0 80px;
  }
`

const SuccessButton = styled.button`
  border-radius: 3px;
  background-color: #E55710;
  text-align: center;
  color: #FFFFFF;
  font-size: 18px;
  line-height: 18px;
  width: 100%;
  cursor: pointer;
  border: 1px solid #E55710;
  transition: all 0.2s ease-out 0.1s;
  height: 45px;
  font-weight: 500;

  &:hover {
    background-color: #FFFFFF;
    color: #E55710;
  }
`

interface HeaderProps {
  show: any,
  handleShow: any
}

const Uptodate = ({ show, handleShow }: HeaderProps) => {
  const [formData, setFormData] = React.useState({ email: '' })
  const resetFormData = () => {
    handleShow(false)
    setFormData({ email: '' })
  }
  const [errorMessage, setErrorMessage] = React.useState(false)
  const [success, setSuccess] = React.useState(false)

  const saveValue = (key, value) => {
    setFormData({ ...formData, [key]: value })
  }
  const closeModal = () => {
    setSuccess(false)
    resetFormData()
    handleShow(false)
  }

  return (
    <Dialog 
      onClose={() => closeModal()} 
      open={show}
    >
      {
        success ?
          <SuccessContainer>
            <CheckSuccess />
            <SuccessContent>
              <h2>Request received!</h2>
              <p>Our team will reach out to you shortly! If you would like to talk to someone, call us at +60 16-299 1366.</p>
              <SuccessButton onClick={() => closeModal()}>Continue</SuccessButton>
            </SuccessContent>
          </SuccessContainer>
          :
          <ModalContainer>
            <CloseIcon onClick={() => closeModal()} ><Close/></CloseIcon>
            <Title>Get-up-to-date on new launches</Title>
            <SubTitle>Be sure to leave your email behind so that we can notify you about the latest information.</SubTitle>
            <Formik
                initialValues={{
                  email: '',
                }}
                onSubmit={(values, { setSubmitting }) => {
                  setErrorMessage(false)
                  const params = {
                    "email": formData.email
                  }
                  NewLaunchesService
                    .submitUptodate(params)
                    .then(response => {
                      if(response.data._statusCode === 1){
                        setSubmitting(false)
                        setErrorMessage(false)
                        setSuccess(true)
                      }
                      if(response.data._statusCode === 3){
                        setSubmitting(false)
                        setErrorMessage(true)
                        resetFormData()
                      }
                    })
                }}
                validationSchema={
                  Yup.object().shape({
                    email: Yup
                      .string()
                      .required('This is required.'),
                  })
                }
              >
              {(props) => {
                  const {
                    isSubmitting,
                    handleSubmit,
                  } = props
                return (
                  <Form>
                    <form onSubmit={handleSubmit}>
                      <Grid
                        container
                        direction="row"
                        justify="space-between"
                      >
                        <Box width={'65%'}>
                          <TextInput
                            name="email"
                            label={"Enter your email"}
                            type="email"
                            saveValue={saveValue}
                            value={formData.email}
                          />
                        </Box>
                        <Box width={'30%'}>
                          <BtnDefault
                            type="submit"
                            variant="contained"
                            text="Notify Me"
                            color="primary"
                            isSubmitting={isSubmitting}
                            action={() => {} }
                          />
                        </Box>
                      </Grid>
                    </form>
                    {
                      errorMessage ?
                        <FormHelperText>
                          <Helpertext>This email has an existing subscription</Helpertext>
                        </FormHelperText>
                        : ''
                    }
                  </Form>
                )
              }}
            </Formik>
          </ModalContainer>
      }

    </Dialog>
  )
}

export default Uptodate