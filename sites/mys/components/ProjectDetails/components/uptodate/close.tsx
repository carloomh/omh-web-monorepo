import React from 'react'
const Close = () => (
  <svg version="1.1" id="Two-Tone" x="0px" y="0px" viewBox="0 0 24 24" xmlSpace="preserve" width="24px" height="24px">
    <line style={{
      fill: 'none',
      stroke: "#000000",
      strokeWidth: 2,
      strokeMiterlimit: 10
    }} x1="4" y1="4" x2="20" y2="20"/>
    <line style={{
        fill: 'none',
        stroke: "#000000",
        strokeWidth: 2,
        strokeMiterlimit: 10
      }} x1="20" y1="4" x2="4" y2="20"/>
  </svg>
)
export default Close