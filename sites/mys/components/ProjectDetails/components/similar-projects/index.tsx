import React from 'react'
import styled from 'styled-components'
import { SlickSlider } from "omhwebui"
import Responsive from 'react-responsive'
import ProjectItem from './project-item'


const Mobile = props => <Responsive {...props} maxWidth={1127} />
const Desktop = props => <Responsive {...props} minWidth={1128} />

const Container = styled.div`
  margin: 20px 0 60px;
  
  & .slick-slider{
    & .slick-track{
      margin: 0;
    }
    & .slick-next{
      right: -45px;
    }
    & .slick-prev{
      left: -40px;
    }
  }
  &:before{
    content: '';
    margin-top: -210px;
    height: 210px;
    visibility: hidden; 
    pointer-events: none;
    display: block; 
  }
`

const Header = styled.header`
  font-size: 22px;
  font-weight: 500;
  margin: 0 0 15px;
  padding: 0 20px;
  
  @media (min-width : 1128px) {
    margin: 0 0 20px;
    padding: 0;
  }
`

const MobileScroll = styled.div`
  display: flex;
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: normal;
  -webkit-overflow-scrolling: touch;
`

interface SimilarProps {
  projects: any,
  media_path: any,
}

const SimilarProjects = ({ projects, media_path }: SimilarProps) => {
  return (
    <Container id={'similar_projects'}>
      <Header>Similar Projects</Header>
      <Desktop>
        <SlickSlider
          slidesToScroll={1}
          slidesToShow={3}
        >
          {
            projects && projects.map( (item, index) => {
              const { obj } = item
              return (
                obj &&
                <ProjectItem 
                  key={index} 
                  project={obj}
                  media_path={media_path}
                  label={'Price from'}
                />
              )
            })
          }
        </SlickSlider>
      </Desktop>
      <Mobile>
        <MobileScroll>
          {
            projects && projects.map( (item, index) => {
              const { obj } = item
              return (
                obj &&
                <ProjectItem 
                  key={index} 
                  project={obj}
                  media_path={media_path}
                  label={'Price from'}
                />
              )
            })
          }
        </MobileScroll>
      </Mobile>
    </Container>
  )
}

export default SimilarProjects