import React from 'react'
import styled from 'styled-components'
// import { Link } from 'react-router-dom'
import numeral from 'numeral'

const LinkItem = styled.a`
  color: #000000;
  text-decoration:none;
  
  &:last-child{
    & .project-item{
      padding-right: 20px;
    }
  }
`

const Item = styled.div`
  margin-left: 15px;
  padding: 10px 0;
  height: 270px;  
  display: flex;
  align-items: center;
  width: 258px;
  box-sizing: content-box;

  @media (min-width: 481px) and (max-width: 767px) {
    margin: 0 10px 0 20px;
    height: 290px;
  }

  @media (min-width: 767px) and (max-width: 1223px) {
    margin: 0 10px;
    height: 320px;
    width: 305px;
  }

  @media (min-width : 1224px) {
    margin: 0 10px;
    height: 320px;
    width: 305px;
  }
`
const Container = styled.div`
  border: 1px solid rgba(0,0,0,0.17);
  border-radius: 10px;
  overflow: hidden;
  position: relative;
  display: block;
  width: 100%;
  height: 100%;
  transition: transform 300ms ease-in-out, box-shadow 300ms ease-in-out;
  transform: scale(1);
  cursor: pointer;

  &:hover {
    box-shadow: none;
    transform: scale(1);
  }

  @media (min-width: 481px) and (max-width: 767px) {
    &:hover {
      box-shadow: 0px 0px 12px 2px rgba(0,0,0,0.15);
      transform: scale(1.02);
    }
  }

  @media (min-width: 767px) and (max-width: 1223px) {
    &:hover {
      box-shadow: 0px 0px 12px 2px rgba(0,0,0,0.15);
      transform: scale(1.02);
    }
  }

  @media (min-width : 1224px) {
    &:hover {
      box-shadow: 0px 0px 12px 2px rgba(0,0,0,0.15);
      transform: scale(1.02);
    }
  }
`

const Header = styled.div`
  position: relative;
  height: 152px;

  @media (min-width: 481px) and (max-width: 767px) {
    height: 152px;
  }

  @media (min-width: 767px) and (max-width: 1223px) {
    height: 178px;
  }

  @media (min-width : 1224px) {
    height: 178px;
  }
`

const ImageContainer = styled.div`
  height: 100%;
  position: relative;
  overflow: hidden;
  border-radius: 10px 10px 0 0;
  background-size: cover;

  &:before{
    content: '';
    background: #54545426;
    height: 100%;
    width: 100%;
    position: absolute;
    z-index: 2;
    top: 0;
    opacity: 0;
    left: 0;
    transition: opacity 200ms ease-in-out;

    ${Container}:hover & {
      opacity: 1;
    }
  }
`

const Body = styled.div`
  padding: 10px 15px;
  color: #000000;	
  font-family: Roboto;	

  @media (min-width : 1224px) {
    padding: 15px 20px;
  }
`

const Name = styled.h5`
  font-weight: bold;
  font-size: 16px;
  line-height: 26px;
  margin: 0;
  white-space: nowrap;
  width: 100%;
  overflow: hidden;
  text-overflow: ellipsis;

  @media (min-width : 1224px) {
    font-size: 22px;	
    line-height: 34px;
  }
`

const Address = styled.p`
  font-size: 14px;
  line-height: 19px;
  margin: 0px 0 12px;
  white-space: nowrap;
  text-overflow: ellipsis;
  overflow: hidden;

  @media (min-width : 1224px) {
    font-size: 16px;
    line-height: 18px;
  }
`
const Label = styled.p`
  font-size: 12px;
  line-height: 14px;
  font-weight: 300;
  margin: 0 0 5px;

  @media (min-width : 1224px) {
    font-size: 13px;
    line-height: 15px;
  }
`
const Price = styled.h5`
  font-size: 16px;
  font-weight: 500;
  margin: 0;

  @media (min-width : 1224px) {
    font-size: 22px;
  }
`
const defaultImage = '/themes/omhmy/assets/images/no_image_listing_placeholder.png'

const ProjectItem = ({ project, label, media_path }) => {
  const {
    name,
    address,
    minAmount,
    featuredImageUrl,
    webUrl
  } = project

  const {
    addressStreetName = '',
    townName = '',
    stateName = ''
  } = address || {}

  return (
    <LinkItem href={webUrl}>
      <Item className={'project-item'}>
        <Container>
          <Header>
            {
              featuredImageUrl ?
                <ImageContainer
                  style={{
                    background: `center / cover url(${featuredImageUrl}?h=720&w=720&c=limit) no-repeat`,
                  }}
                />
                : 
                <ImageContainer
                  style={{
                    background: `center / cover url(${defaultImage}) no-repeat`,
                  }}
                />
            }
          </Header>
          <Body>
            <Name title={name}>{name}</Name>
            <Address> {addressStreetName} {townName} {stateName} </Address>
            <Label>{label || 'Price from'}</Label>
            <Price>RM { numeral(minAmount).format('0,0') }</Price>
          </Body>
        </Container>
      </Item>
    </LinkItem>
  )
}

export default ProjectItem
