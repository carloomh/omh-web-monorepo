import React from 'react'
import styled from 'styled-components'
import axios from 'axios'
import Box from "@material-ui/core/Box"
import Grid from "@material-ui/core/Grid"
import { Formik } from 'formik'
import { TextInput } from "omhwebui"
import { MobileInput } from "omhwebui"
import Dialog from '@material-ui/core/Dialog'
import BtnDefault from 'omhwebui/components/Button/default'
import RideImage from './ride_image'
import { RideValidation } from './validation'
import NewLaunchesService from '../../../../services/api/newlaunches'
import CheckSuccess from './check'

const Container = styled.section`
  position: relative;
  border: 1px solid #D9DDE7;
  border-radius: 3px;
  margin: 0 0 25px;
  display: flex;
  align-content: center;
  flex-direction: column;
  position: relative;

  @media (min-width : 700px) {
    flex-direction: row;
  }
`
const RideImg = styled.div`
  width: 43%;
  background-position: center right !important;
  overflow: hidden;

  @media (max-width : 1223px) and (min-width : 1024px){
    height: auto;
    background-size: cover !important;
    background-position-x: -189px !important;
    background-position-y: -40px !important;
  }

  @media (max-width : 1127px) and (min-width : 837px) {
    width: 50%;
    min-height: 240px;
    background-position-x: -40px !important;
  }
  @media (max-width : 1023px) and (min-width : 700px) {
    background-position-x: -189px !important;
    width: 40%;
  }
  @media (max-width : 699px)  {
    width: 100%;
    background-position-x: 0 !important;
    min-height: 240px;
  }
`
const Content = styled.div`
  padding: 25px 25px;
  width: 50%;
  box-sizing: border-box;

  @media (min-width : 1128px) {
    width: 50%;
    height: 100%;
    padding: 25px 25px;
  }
  @media (max-width : 1127px) and (min-width : 700px) {
    padding: 45px 35px;
    max-width: 420px;
    span {
      line-height: 22px;
    }
  }
  @media (max-width : 699px) {
    width: 100% !important;
  }
`

const FreeRideSidebarTitle = styled.h2`
  font-family: Roboto, sans-serif;
  font-size: 20px;
  line-height: 20px;
  font-weight: bold;
  color: #000;
  margin: 0 0 10px;
  display: flex;
  align-items: center;
  white-space: nowrap;

  & svg{
    height: 30px;
    margin-right: 5px;
    width: 35px;
  }
`

const FreeRideSidebarAddress = styled.span`
  font-family: Roboto, sans-serif;
  font-size: 14px;
  line-height: 18px;
  font-weight: 400;
  color: #000;
  margin: 0 0 18px;
  display: block;
`

const ModalContainer = styled.div`
  border-radius: 10px;
  background: #FFFFFF;
  padding: 20px;
  width: 320px;
  margin: 25px;
  text-align: center;
`

const SuccessContent = styled.div`
  text-align: center;
  color: #000000;
  font-family: Roboto;
  margin-top: 20px;

  & h2{
    font-size: 26px;
    font-weight: 500;
    line-height: 36px;
    margin: 0 0 20px;
  }

  & p{
    color: #616161;
    font-family: Roboto;
    font-size: 17px;
    line-height: 26px;
    margin: 0 0 80px;
  }
`

const SuccessButton = styled.button`
  border-radius: 3px;
  background-color: #E55710;
  text-align: center;
  color: #FFFFFF;
  font-size: 18px;
  line-height: 18px;
  width: 100%;
  cursor: pointer;
  border: 1px solid #E55710;
  transition: all 0.2s ease-out 0.1s;
  height: 45px;
  font-weight: 500;

  &:hover {
    background-color: #FFFFFF;
    color: #E55710;
  }
`

const CarIcon = () => (
  <svg width="70px" height="70px" viewBox="0 0 70 70" version="1.1">
      <g id="icon_malaysia_free_car_ride" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <g>
              <rect id="Rectangle" x="0" y="0" width="70" height="70"></rect>
              <g id="icon" transform="translate(8.000000, 14.000000)" fill="#E55710">
                  <path d="M11.5534119,26.4166509 C10.0312348,26.4166509 8.79309504,25.164209 8.79309504,23.6244487 C8.79309504,22.0857978 10.0312348,20.8322465 11.5534119,20.8322465 C13.075589,20.8322465 14.3148253,22.0857978 14.3148253,23.6244487 C14.3148253,25.164209 13.075589,26.4166509 11.5534119,26.4166509 L11.5534119,26.4166509 Z M42.4465881,26.4166509 C40.924411,26.4166509 39.6851747,25.164209 39.6851747,23.6244487 C39.6851747,22.0857978 40.924411,20.8322465 42.4465881,20.8322465 C43.9687652,20.8322465 45.206905,22.0857978 45.206905,23.6244487 C45.206905,25.164209 43.9687652,26.4166509 42.4465881,26.4166509 L42.4465881,26.4166509 Z M33.5865963,32.6666576 L32.6862307,30.5833221 L21.3137693,30.5833221 L20.4134037,32.6666576 L16.6814379,32.6666576 L18.9811535,27.0822533 L35.0177498,27.0822533 L37.3185621,32.6666576 L33.5865963,32.6666576 Z M13.6129569,3.49995941 C12.5535743,3.49995941 11.6038587,4.21659136 11.3022746,5.24383777 L8.53757108,14.5833491 L45.4624289,14.5833491 L42.6977254,5.24272843 C42.3961413,4.21659136 41.4464257,3.49995941 40.3870431,3.49995941 L13.6129569,3.49995941 Z M6.09090171,22.878974 L6.076645,35.4144863 L47.923355,35.4144863 L47.923355,22.9732677 L46.4867181,18.0833085 L7.51218522,18.0833085 L6.09090171,22.878974 Z M49.1932981,41 L48.4859464,38.9166644 L6.61072299,38.9166644 L5.90337124,41 L4.34610073,41 C3.39199838,41 2.61555646,40.2145891 2.61555646,39.2483563 L2.61555646,22.5827809 L2.71316003,21.9171785 L4.04232331,17.3555832 L0,15.9922076 L1.078026,12.718553 L5.07867587,14.089694 L8.00787977,4.18885792 C8.74922827,1.68397413 10.9842405,0 13.5701868,0 L40.3870431,0 C42.9982128,0 45.2551584,1.70061419 46.0052803,4.23101274 L48.9213241,14.089694 L52.921974,12.718553 L54,15.9922076 L49.9576767,17.3555832 L51.3339968,22.0913445 L51.3877335,22.4951433 L51.3844435,37.1650207 C51.3921202,37.2815011 51.3943136,37.3746855 51.3855402,37.4601044 L51.3844435,39.2483563 C51.3844435,40.2145891 50.6080016,41 49.6538993,41 L49.1932981,41 Z" id="Fill-1"></path>
              </g>
          </g>
      </g>
  </svg>
)

interface FreeRideProps {
  sidebar: Boolean,
  project_details: any
}

const FreeRide = ({
  sidebar,
  project_details
}: FreeRideProps) => {
  const {
    name
  } = project_details

  const [formData, setFormData] = React.useState({
    name: '',
    mobile: '',
  })
  const [countryList, setCountryList] = React.useState([])
  const [country, setCountry] = React.useState("0")
  const [errorMessage, setErrorMessage] = React.useState(false)
  const [showSuccess, setSuccess] = React.useState(false)

  const saveValue = (key, value) => {
    setFormData({ ...formData, [key]: value })
  }

  const resetFormData = () => {
    setSuccess(false)
    setFormData({
      name: '',
      mobile: '',
    })
  }

  React.useEffect(() => {
    async function fetchData() {
      const response = await axios('https://qa.ohmyhome.com.my/api/global/config')
      setCountryList(response.data.countries)
    }
    fetchData()
  }, [])

  return(
    <React.Fragment>
      <Container>
        { !sidebar && 
          <RideImg><RideImage /></RideImg>
        }
        <Content
          style={{
            width: sidebar ? '100%' : '58%'
          }}
        >
          <FreeRideSidebarTitle >
            { sidebar && <CarIcon />} {'Free Grab Ride to Showroom'}
          </FreeRideSidebarTitle>
          <FreeRideSidebarAddress>
              {sidebar ?
                'Interested in viewing a launch project? Leave it to us to arrange a free Grab car ride to the showroom.'
                : 'Let us take you to any showroom in Kuala Lumpur with a free Grab car ride at your convenience.'
              }
          </FreeRideSidebarAddress>
          <Box width={"100%"} mb={1}>
            <Grid
              container
              direction="column"
            >
            <Formik
                initialValues={{
                  mobile: formData.mobile,
                  name: '',
                }}
                onSubmit={(values, { setSubmitting }) => {
                  setErrorMessage(false)
                  const params = {
                    "countryCode": countryList[country].code,
                    "mobile": `${countryList[country].phone}${values.mobile}`,
                    "name": values.name
                  }

                  NewLaunchesService
                    .submitFreeRide(params)
                    .then(response => {
                      if(response.data._statusCode === 1){
                        setSubmitting(false)
                        resetFormData()
                        setErrorMessage(false)
                        setSuccess(true)
                      }
                    })
                }}
                validationSchema={RideValidation(
                  countryList[country] && countryList[country].code === undefined ||
                  countryList[country] && countryList[country].code !== "SGP" ? "DEFAULT" : "SGP")}
              >
              {(props) => {
                const {
                  handleSubmit,
                  isSubmitting
                } = props

                return (
                  <form onSubmit={handleSubmit}>
                    <Box mb={2}>
                      <TextInput
                        name="name"
                        label="Enter your name"
                        type="text"
                        saveValue={saveValue}
                        value={formData.name}
                      />
                    </Box>
                    <Box mb={2}>
                      <MobileInput
                        name="mobile"
                        label="Enter your number"
                        type="tel"
                        country={country}
                        setCountry={setCountry}
                        countryList={countryList}
                        saveValue={saveValue}
                        value={formData.mobile}
                      />
                    </Box>
                    <Box>
                      <BtnDefault
                        type="submit"
                        variant="contained"
                        text="Book a Ride"
                        color="primary"
                        isSubmitting={isSubmitting}
                        action={() => {} }
                      />
                    </Box>
                  </form>
                )
              }}
            </Formik>
          </Grid>
          </Box>
        </Content>
      </Container>

      <Dialog
        onClose={() => setSuccess(false)} 
        open={showSuccess}
      >
        <ModalContainer>
          <CheckSuccess />
          <SuccessContent>
            <h2>Request received!</h2>
            <p>Our team will reach out to you shortly! If you would like to talk to someone, call us at +60 16-299 1366.</p>
            <SuccessButton onClick={resetFormData}>Continue</SuccessButton>
          </SuccessContent>
        </ModalContainer>
      </Dialog>
    </React.Fragment>
  )
}

export default FreeRide