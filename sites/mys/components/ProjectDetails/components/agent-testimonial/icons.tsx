import * as React from 'react'

const Ratings = () => {
  return(
    <div style={{ position: 'relative'}}>
        <svg width="140px" height="20px" viewBox="0 0 140 20" version="1.1">
            <title>STAR RATING</title>
            <desc>Created with Sketch.</desc>
            <defs>
                <polygon id="path-rating" points="0 0 150 0 150 19.1304348 0 19.1304348"></polygon>
            </defs>
            <g id="project_details(desktop)" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Project_Details_1b" transform="translate(-987.000000, -847.000000)">
                    <g id="Group-54" transform="translate(178.000000, 177.000000)">
                        <g id="Group-28" transform="translate(61.000000, 0.000000)">
                            <g id="Group-48" transform="translate(611.000000, 0.000000)">
                                <g id="Message-Agent-Module" transform="translate(1.000000, 602.000000)">
                                    <g id="USER-DETAILS" transform="translate(20.000000, 21.000000)">
                                        <g id="Group-24" transform="translate(116.000000, 0.000000)">
                                            <g id="STAR-RATING" transform="translate(0.000000, 47.000000)">
                                                <mask id="mask-rating" fill="white">
                                                    <use xlinkHref="#path-rating"></use>
                                                </mask>
                                                <g id="Duplicate-control"></g>
                                                <g id="shape-star" mask="url(#mask-rating)" fill="#EE620F" fillRule="nonzero">
                                                    <path d="M19.6272822,6.96592079 L13.2445807,6.03429816 L10.3912553,0.223995364 C10.3100271,0.0852166859 10.1617464,0 10.0014936,0 C9.84124084,0 9.69296008,0.0852166859 9.61173187,0.223995364 L6.7571022,6.03298786 L0.372661647,6.96592079 C0.208889953,6.98966902 0.0727353985,7.10475384 0.0214348011,7.26279576 C-0.0298657964,7.42083768 0.0125825944,7.59443374 0.130935467,7.71060742 L4.75068796,12.2337598 L3.66031159,18.6201574 C3.63248537,18.7838849 3.69950742,18.9492982 3.83324257,19.0469586 C3.96697772,19.144619 4.14427724,19.157622 4.2907126,19.0805091 L9.99997194,16.065511 L15.7092313,19.0791988 C15.8556666,19.1563117 16.0329662,19.1433087 16.1667013,19.0456483 C16.3004365,18.9479879 16.3674585,18.7825746 16.3396323,18.6188471 L15.2492559,12.2337598 L19.8690084,7.71060742 C19.9873429,7.59449368 20.0298368,7.42097773 19.9786366,7.26296088 C19.9274364,7.10494402 19.7914136,6.98980588 19.627717,6.96592079 L19.6272822,6.96592079 Z" id="Shape"></path>
                                                </g>
                                                <g id="shape-star" mask="url(#mask-rating)" fill="#EE620F" fillRule="nonzero">
                                                    <g transform="translate(30.000000, 0.000000)" id="Shape">
                                                        <path d="M19.6272822,6.96592079 L13.2445807,6.03429816 L10.3912553,0.223995364 C10.3100271,0.0852166859 10.1617464,0 10.0014936,0 C9.84124084,0 9.69296008,0.0852166859 9.61173187,0.223995364 L6.7571022,6.03298786 L0.372661647,6.96592079 C0.208889953,6.98966902 0.0727353985,7.10475384 0.0214348011,7.26279576 C-0.0298657964,7.42083768 0.0125825944,7.59443374 0.130935467,7.71060742 L4.75068796,12.2337598 L3.66031159,18.6201574 C3.63248537,18.7838849 3.69950742,18.9492982 3.83324257,19.0469586 C3.96697772,19.144619 4.14427724,19.157622 4.2907126,19.0805091 L9.99997194,16.065511 L15.7092313,19.0791988 C15.8556666,19.1563117 16.0329662,19.1433087 16.1667013,19.0456483 C16.3004365,18.9479879 16.3674585,18.7825746 16.3396323,18.6188471 L15.2492559,12.2337598 L19.8690084,7.71060742 C19.9873429,7.59449368 20.0298368,7.42097773 19.9786366,7.26296088 C19.9274364,7.10494402 19.7914136,6.98980588 19.627717,6.96592079 L19.6272822,6.96592079 Z"></path>
                                                    </g>
                                                </g>
                                                <g id="shape-star" mask="url(#mask-rating)" fill="#EE620F" fillRule="nonzero">
                                                    <g transform="translate(60.000000, 0.000000)" id="Shape">
                                                        <path d="M19.6272822,6.96592079 L13.2445807,6.03429816 L10.3912553,0.223995364 C10.3100271,0.0852166859 10.1617464,0 10.0014936,0 C9.84124084,0 9.69296008,0.0852166859 9.61173187,0.223995364 L6.7571022,6.03298786 L0.372661647,6.96592079 C0.208889953,6.98966902 0.0727353985,7.10475384 0.0214348011,7.26279576 C-0.0298657964,7.42083768 0.0125825944,7.59443374 0.130935467,7.71060742 L4.75068796,12.2337598 L3.66031159,18.6201574 C3.63248537,18.7838849 3.69950742,18.9492982 3.83324257,19.0469586 C3.96697772,19.144619 4.14427724,19.157622 4.2907126,19.0805091 L9.99997194,16.065511 L15.7092313,19.0791988 C15.8556666,19.1563117 16.0329662,19.1433087 16.1667013,19.0456483 C16.3004365,18.9479879 16.3674585,18.7825746 16.3396323,18.6188471 L15.2492559,12.2337598 L19.8690084,7.71060742 C19.9873429,7.59449368 20.0298368,7.42097773 19.9786366,7.26296088 C19.9274364,7.10494402 19.7914136,6.98980588 19.627717,6.96592079 L19.6272822,6.96592079 Z"></path>
                                                    </g>
                                                </g>
                                                <g id="shape-star" mask="url(#mask-rating)" fill="#EE620F" fillRule="nonzero">
                                                    <g transform="translate(90.000000, 0.000000)" id="Shape">
                                                        <path d="M19.6272822,6.96592079 L13.2445807,6.03429816 L10.3912553,0.223995364 C10.3100271,0.0852166859 10.1617464,0 10.0014936,0 C9.84124084,0 9.69296008,0.0852166859 9.61173187,0.223995364 L6.7571022,6.03298786 L0.372661647,6.96592079 C0.208889953,6.98966902 0.0727353985,7.10475384 0.0214348011,7.26279576 C-0.0298657964,7.42083768 0.0125825944,7.59443374 0.130935467,7.71060742 L4.75068796,12.2337598 L3.66031159,18.6201574 C3.63248537,18.7838849 3.69950742,18.9492982 3.83324257,19.0469586 C3.96697772,19.144619 4.14427724,19.157622 4.2907126,19.0805091 L9.99997194,16.065511 L15.7092313,19.0791988 C15.8556666,19.1563117 16.0329662,19.1433087 16.1667013,19.0456483 C16.3004365,18.9479879 16.3674585,18.7825746 16.3396323,18.6188471 L15.2492559,12.2337598 L19.8690084,7.71060742 C19.9873429,7.59449368 20.0298368,7.42097773 19.9786366,7.26296088 C19.9274364,7.10494402 19.7914136,6.98980588 19.627717,6.96592079 L19.6272822,6.96592079 Z"></path>
                                                    </g>
                                                </g>
                                                <g id="shape-star" mask="url(#mask-rating)" fill="#EE620F" fillRule="nonzero">
                                                    <g transform="translate(120.000000, 0.000000)" id="Shape">
                                                        <path d="M19.6272822,6.96592079 L13.2445807,6.03429816 L10.3912553,0.223995364 C10.3100271,0.0852166859 10.1617464,0 10.0014936,0 C9.84124084,0 9.69296008,0.0852166859 9.61173187,0.223995364 L6.7571022,6.03298786 L0.372661647,6.96592079 C0.208889953,6.98966902 0.0727353985,7.10475384 0.0214348011,7.26279576 C-0.0298657964,7.42083768 0.0125825944,7.59443374 0.130935467,7.71060742 L4.75068796,12.2337598 L3.66031159,18.6201574 C3.63248537,18.7838849 3.69950742,18.9492982 3.83324257,19.0469586 C3.96697772,19.144619 4.14427724,19.157622 4.2907126,19.0805091 L9.99997194,16.065511 L15.7092313,19.0791988 C15.8556666,19.1563117 16.0329662,19.1433087 16.1667013,19.0456483 C16.3004365,18.9479879 16.3674585,18.7825746 16.3396323,18.6188471 L15.2492559,12.2337598 L19.8690084,7.71060742 C19.9873429,7.59449368 20.0298368,7.42097773 19.9786366,7.26296088 C19.9274364,7.10494402 19.7914136,6.98980588 19.627717,6.96592079 L19.6272822,6.96592079 Z"></path>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </svg>
    </div>
  )
}

const Messenger = () => {
  return(
    <svg width="85px" height="84px" viewBox="0 0 85 84" version="1.1">
        <title>Input</title>
        <desc>Created with Sketch.</desc>
        <g id="placement_final" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="malaysia_homepage_new_launch" transform="translate(-762.000000, -3853.000000)">
                <g id="Group-97" transform="translate(-3.000000, 2554.000000)">
                    <g id="Group-95">
                        <g id="cta" transform="translate(100.000000, 1070.000000)">
                            <g id="Group-87" transform="translate(132.000000, 0.000000)">
                                <g id="Group-85" transform="translate(505.000000, 15.000000)">
                                    <g id="Group-80-Copy-2" transform="translate(0.000000, 79.800000)">
                                        <g id="Group-78">
                                            <g id="Group-86" transform="translate(0.000000, 133.800000)">
                                                <g id="Group-59" transform="translate(0.000000, 0.546172)">
                                                    <g id="Input" transform="translate(28.053814, 0.000000)">
                                                        <path d="M41.472,0 L42.6894429,7.10542736e-15 C65.5937961,2.89796594e-15 84.1614429,18.5676469 84.1614429,41.472 L84.1614429,41.818792 C84.1614429,64.7231452 65.5937961,83.290792 42.6894429,83.290792 L41.472,83.290792 C18.5676469,83.290792 9.91040163e-15,64.7231452 7.10542736e-15,41.818792 L7.10542736e-15,41.472 C4.30045308e-15,18.5676469 18.5676469,1.13128888e-14 41.472,7.10542736e-15 Z" id="Rectangle-4-Copy" fill="#0083FF"></path>
                                                        <g id="Group-42" transform="translate(19.637670, 18.046338)" fill="#FFFFFF">
                                                            <g id="Group-56">
                                                                <g id="Group-62">
                                                                    <path d="M12.1582649,42.60048 L9.9475983e-13,48.5862954 L4.81678901,36.3905674 C1.87252597,32.5827282 0.125022447,27.8325924 0.125022447,22.6813815 C0.125022447,10.1546456 10.4590178,-2.84217094e-14 23.2070103,-2.84217094e-14 C35.9549006,-2.84217094e-14 46.2887936,10.1546456 46.2887936,22.6813815 C46.2887936,35.2080167 35.9549006,45.3628638 23.2070103,45.3628638 C19.2043992,45.3628638 15.4397678,44.3617658 12.1582649,42.60048 Z M7.01345358,31.9281369 L19.2004155,24.0943952 L27.121092,29.916118 L39.27534,13.8817987 L27.3886672,21.9814964 L18.9982682,16.0950101 L7.01345358,31.9281369 Z" id="Combined-Shape"></path>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  )
}

const Intercom = () => {
  return(
    <svg width="85px" height="84px" viewBox="0 0 85 84" version="1.1">
        <title>Screen Shot 2019-01-10 at 10.06.14 AM</title>
        <desc>Created with Sketch.</desc>
        <defs>
            <path d="M62.6780308,65.8099356 L62.6780308,25.5527195 C61.7981528,22.5086019 60.2292579,20.9865432 57.9713459,20.9865432 C55.7134339,20.9865432 45.456943,20.9865432 27.201873,20.9865432 C24.4083633,22.1680053 23.0116085,23.6900641 23.0116085,25.5527195 C23.0116085,27.4153749 23.0116085,37.9087456 23.0116085,57.0328318 C23.9179695,59.9316408 25.3147244,61.3810454 27.201873,61.3810454 C29.0890217,61.3810454 37.1275536,61.3810454 51.3174688,61.3810454 L62.6780308,65.8099356 Z" id="path-3"></path>
        </defs>
        <g id="placement_final" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="malaysia_homepage_new_launch" transform="translate(-1084.000000, -3852.000000)">
                <g id="Group-97" transform="translate(-3.000000, 2554.000000)">
                    <g id="Group-95">
                        <g id="cta" transform="translate(100.000000, 1070.000000)">
                            <g id="Group-87" transform="translate(132.000000, 0.000000)">
                                <g id="Group-85" transform="translate(505.000000, 15.000000)">
                                    <g id="Group-80-Copy-2" transform="translate(0.000000, 79.800000)">
                                        <g id="Group-78">
                                            <g id="Group-86" transform="translate(0.000000, 132.800000)">
                                                <g id="Group-81" transform="translate(322.099349, 0.000000)">
                                                    <g id="Group-82">
                                                        <g id="Group-58" transform="translate(27.094175, 0.434518)">
                                                            <g id="Screen-Shot-2019-01-10-at-10.06.14-AM">
                                                                <path d="M42.4316393,0.565481859 L43.6490822,0.565481859 C66.5534353,0.565481859 85.1210822,19.1331287 85.1210822,42.0374819 L85.1210822,42.3842739 C85.1210822,65.288627 66.5534353,83.8562739 43.6490822,83.8562739 L42.4316393,83.8562739 C19.5272861,83.8562739 0.959639267,65.288627 0.959639267,42.3842739 L0.959639267,42.0374819 C0.959639267,19.1331287 19.5272861,0.565481859 42.4316393,0.565481859 Z" id="Rectangle-4-Copy" fill="#E55710"></path>
                                                                <mask id="mask-intercom" fill="white">
                                                                    <use xlinkHref="#path-3"></use>
                                                                </mask>
                                                                <g id="Mask"></g>
                                                                <image mask="url(#mask-intercom)" x="-1.84574216" y="-9.15177721" width="92.5775872" height="101.33713" xlinkHref="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAIQAAACSCAYAAACEy2IhAAAMR2lDQ1BJQ0MgUHJvZmlsZQAASImVVwdYU8kWnltSSWiBCEgJvQlSpEsJoUUQkCrYCEkgocSQEETsLssquHYRAXVFV0VcdC2ArBV7WQR7fyiLysq6WLCh8iYF1nW/9973zvfNvX/OnPOfkrn3zgCgU8uTSvNQXQDyJYWyhMhQ1uS0dBapGyAABWTgB7x4fLmUHR8fA6AM3/8ub25AayhXXZRc/5z/r6InEMr5ACDxEGcK5Px8iA8AgJfypbJCAIi+UG89q1CqxFMhNpDBBCGWKnG2GpcqcaYaV6lskhI4EO8GgEzj8WTZAGi3QD2riJ8NebRvQewmEYglAOiQIQ7ii3gCiKMgHpOfP1OJoR1wyPyCJ/tvnJkjnDxe9ghW16IScphYLs3jzf4/2/G/JT9PMRzDDg6aSBaVoKwZ9u1W7sxoJaZB3CfJjI2DWB/id2KByh5ilCpSRCWr7VFTvpwDewaYELsJeGHREJtCHCHJi43R6DOzxBFciOEKQYvFhdwkje8SoTw8UcNZK5uZEDeMs2Qctsa3kSdTxVXan1LkJrM1/LdEQu4w/+sSUVKqOmeMWiROiYVYG2KmPDcxWm2D2ZSIOLHDNjJFgjJ/G4j9hZLIUDU/Nj1LFpGgsZfly4frxZaIxNxYDa4uFCVFaXh283mq/I0gbhFK2MnDPEL55JjhWgTCsHB17ViHUJKsqRfrkhaGJmh8X0rz4jX2OFWYF6nUW0FsKi9K1PjiQYVwQar58VhpYXySOk88M4c3IV6dD14MYgAHhAEWUMCRCWaCHCBu72vug7/UMxGAB2QgGwiBi0Yz7JGqmpHAayIoAX9AJATyEb9Q1awQFEH9pxGt+uoCslSzRSqPXPAY4nwQDfLgb4XKSzISLQX8BjXif0Tnw1zz4FDO/VPHhpoYjUYxzMvSGbYkhhPDiFHECKIjboIH4QF4DLyGwOGB++J+w9n+ZU94TOgkPCJcJ3QRbs8QL5Z9VQ8LTARdMEKEpubML2vG7SCrFx6KB0J+yI0zcRPggo+Dkdh4MIztBbUcTebK6r/m/lsNX3RdY0dxo6CUUZQQisPXntpO2l4jLMqeftkhda6ZI33ljMx8HZ/zRacF8B79tSW2BNuPncVOYOexw1gzYGHHsBbsEnZEiUdW0W+qVTQcLUGVTy7kEf8jHk8TU9lJuVuDW6/bR/VcobBY+X4EnJnS2TJxtqiQxYZvfiGLK+G7jmF5uLn7AaD8jqhfU6+Yqu8Dwrzwl67gOAB+5VCZ/ZeOZw3AoccAMN78pbN+CR+PlQAc6eArZEVqHa68EAAV6MAnyhiYA2vgAOvxAN4gAISAcDABxIEkkAamwy6L4HqWgVlgLlgEykAFWAnWgWqwGWwFO8FPYB9oBofBCXAGXAQd4Dq4C1dPD3gG+sEbMIggCAmhIwzEGLFAbBFnxAPxRYKQcCQGSUDSkAwkG5EgCmQu8g1SgaxGqpEtSD3yM3IIOYGcRzqR28hDpBd5iXxAMZSGGqBmqB06FvVF2Wg0moROQ7PRArQELUWXo1VoHbobbUJPoBfR62gX+gwdwACmhTExS8wF88U4WByWjmVhMmw+Vo5VYnVYI9YK/+erWBfWh73HiTgDZ+EucAVH4ck4Hy/A5+PL8Gp8J96En8Kv4g/xfvwzgU4wJTgT/AlcwmRCNmEWoYxQSdhOOEg4DZ+mHsIbIpHIJNoTfeDTmEbMIc4hLiNuJO4hHid2EruJAyQSyZjkTAokxZF4pEJSGWkDaTfpGOkKqYf0jqxFtiB7kCPI6WQJeTG5kryLfJR8hfyEPEjRpdhS/ClxFAFlNmUFZRullXKZ0kMZpOpR7amB1CRqDnURtYraSD1NvUd9paWlZaXlpzVJS6y1UKtKa6/WOa2HWu9p+jQnGoc2laagLaftoB2n3aa9otPpdvQQejq9kL6cXk8/SX9Af6fN0HbV5moLtBdo12g3aV/Rfq5D0bHVYetM1ynRqdTZr3NZp0+Xomuny9Hl6c7XrdE9pHtTd0CPoeeuF6eXr7dMb5feeb2n+iR9O/1wfYF+qf5W/ZP63QyMYc3gMPiMbxjbGKcZPQZEA3sDrkGOQYXBTwbtBv2G+objDFMMiw1rDI8YdjExph2Ty8xjrmDuY95gfhhlNoo9Sjhq6ajGUVdGvTUabRRiJDQqN9pjdN3ogzHLONw413iVcbPxfRPcxMlkksksk00mp036RhuMDhjNH10+et/oO6aoqZNpgukc062ml0wHzMzNIs2kZhvMTpr1mTPNQ8xzzNeaHzXvtWBYBFmILdZaHLP4nWXIYrPyWFWsU6x+S1PLKEuF5RbLdstBK3urZKvFVnus7ltTrX2ts6zXWrdZ99tY2Ey0mWvTYHPHlmLrayuyXW971vatnb1dqt13ds12T+2N7Ln2JfYN9vcc6A7BDgUOdQ7XHImOvo65jhsdO5xQJy8nkVON02Vn1NnbWey80blzDGGM3xjJmLoxN11oLmyXIpcGl4euTNcY18Wuza7Px9qMTR+7auzZsZ/dvNzy3La53XXXd5/gvti91f2lh5MH36PG45on3TPCc4Fni+eLcc7jhOM2jbvlxfCa6PWdV5vXJ28fb5l3o3evj41Phk+tz01fA99432W+5/wIfqF+C/wO+7339/Yv9N/n/2eAS0BuwK6Ap+PtxwvHbxvfHWgVyAvcEtgVxArKCPohqCvYMpgXXBf8KMQ6RBCyPeQJ25Gdw97Nfh7qFioLPRj6luPPmcc5HoaFRYaVh7WH64cnh1eHP4iwisiOaIjoj/SKnBN5PIoQFR21Kuom14zL59Zz+yf4TJg34VQ0LToxujr6UYxTjCymdSI6ccLENRPvxdrGSmKb40AcN25N3P14+/iC+F8mESfFT6qZ9DjBPWFuwtlERuKMxF2Jb5JCk1Yk3U12SFYkt6XopExNqU95mxqWujq1a/LYyfMmX0wzSROntaST0lPSt6cPTAmfsm5Kz1SvqWVTb0yzn1Y87fx0k+l504/M0JnBm7E/g5CRmrEr4yMvjlfHG8jkZtZm9vM5/PX8Z4IQwVpBrzBQuFr4JCswa3XW0+zA7DXZvaJgUaWoT8wRV4tf5ETlbM55mxuXuyN3KC81b08+OT8j/5BEX5IrOTXTfGbxzE6ps7RM2lXgX7CuoF8WLdsuR+TT5C2FBnDDfknhoPhW8bAoqKim6N2slFn7i/WKJcWXZjvNXjr7SUlEyY9z8Dn8OW1zLecumvtwHnvelvnI/Mz5bQusF5Qu6FkYuXDnIuqi3EW/LnZbvHrx629Sv2ktNStdWNr9beS3DWXaZbKym98FfLd5Cb5EvKR9qefSDUs/lwvKL1S4VVRWfFzGX3bhe/fvq74fWp61vH2F94pNK4krJStvrApetXO13uqS1d1rJq5pWstaW7729boZ685XjqvcvJ66XrG+qyqmqmWDzYaVGz5Wi6qv14TW7Kk1rV1a+3ajYOOVTSGbGjebba7Y/OEH8Q+3tkRuaaqzq6vcStxatPXxtpRtZ3/0/bF+u8n2iu2fdkh2dO1M2Hmq3qe+fpfprhUNaIOioXf31N0dP4X91NLo0rhlD3NPxV6wV7H3958zfr6xL3pf237f/Y0HbA/UHmQcLG9CmmY39TeLmrta0lo6D0041NYa0HrwF9dfdhy2PFxzxPDIiqPUo6VHh46VHBs4Lj3edyL7RHfbjLa7JyefvHZq0qn209Gnz52JOHPyLPvssXOB5w6f9z9/6ILvheaL3hebLnldOvir168H273bmy77XG7p8Oto7RzfefRK8JUTV8OunrnGvXbxeuz1zhvJN27dnHqz65bg1tPbebdf3Cm6M3h34T3CvfL7uvcrH5g+qPuX47/2dHl3HXkY9vDSo8RHd7v53c9+k//2saf0Mf1x5ROLJ/VPPZ4e7o3o7fh9yu89z6TPBvvK/tD7o/a5w/MDf4b8eal/cn/PC9mLoZfLXhm/2vF63Ou2gfiBB2/y3wy+LX9n/G7ne9/3Zz+kfngyOOsj6WPVJ8dPrZ+jP98byh8akvJkPNVWAIMDzcoC4OUOAOhpcO/QAQB1ivqcpxJEfTZVIfCfsPosqBJvAHaEAJC8EIAYuEfZBIctxDR4V27Vk0IA6uk5MjQiz/L0UHPR4ImH8G5o6JUZAKRWAD7JhoYGNw4NfdoGk70NwPEC9flSKUR4NvjBVYk6ep6Dr+XfF6J/h7uTxe8AABxXSURBVHgB7V0JlBzVdX3VM6ORBEjIQiAwiwCzGxBiEZstFmNjHDbbmJiEJI4TB5+D8QnxAR9wyIZt7CTEJ7FPsANewxacYIRJsNmxQWxiB7HY7AYJCdAuzdLd+fdP3+L1619Lz0z3zLTqndPz9v9/vfumqrqrujqqOhJFRlWebDFvbp64PDFcUTOxzBkNHkVR7mHyxOaJwYR540KLy8qNXDHjhlBiaKxEW968rLgsPxeQN47x7eJZxeY6suKy/HnHYZzlaePHDTGcIufNyYobqd9ucNZ4Nj6vnlbI0BhZ8SP1c86scRineVKOb4jhFDBPTlpMmg8Lz/LnjdFFaJWcVFw9X1ZMmj/NxznyxDCWPJQTVSqV+JDBwDQ+UqDS8tN8WFOWn+vOG8f4vDxUwFBuVlyaf7g+riMtnzGW65ymGiKr0Gn+VviwYWnj2g1vha6LGRo/zd8KH9aQNm5ojTonV0PkKXpSTLN2LC4pJ8uXx4+YkVBWsdP8w/El5STZ9bblibHxmQ2RBg4GS/K32p42t95Iyknrod/yZoqZFpvka7Wd25M0D/2WpzZEVhGT/CF7yIbFNGNPik0bx27waOlphQ75QjaspRl7M7F6O5PydAzlxIYYTvFDOSEbJg/Z89qS8rlRmofG1P4sOW8xk+JC9rw2rG2ksdy+0Dj0ad7QEFkFDPlDNkwSsuex5YnRG5E013BikJOneFkxIb+1WT1p7lBcs7GIByWNNeR1fv22MwQEA8FD/tG02bGsnrUWrjEtT4/RrIxiJhU0yY45rC9LD+WM1IZ8kp2fdvC4IdKKmOSzdqtjgjw2G2P1tHFCsYgnZfkZl8TTiocc+EMxeWw2xuoc367Nxlmd8Un2pHG9HXuItKKFfMO12Tyta5kbFLK59dJdx0OxIRuTrK/Z4iXFl0olThHzUKy2aRlJWXoophkbYkF2Hm8rl8uJn1TaoiEhj83GpOnWF5oj1AQ2jzo5xrGU5tOxoULRTx+5tVMHt81hcxCjbVq2vjx6KCbJBjuoYc6khggVz9qsjgm0TcvN+BibJz8UY20Yz5KNsYWx8dARY+OokzNO56flMS4rPxSXZNNjJcXQDq7jo1BD2GIhydqa0XWslkPj2r2BjoecpmM8UGXNO1Jd+qJUlr0s1bdel+rbS6WyarlUnV3Wr5bqxvUiA30usDyUUOoS6emVaPJUkanTJNpihpSmz5LoPbMlmrmdlLbZSaLZO0vJ2S1ZsEO6zsnaa9SBo+630HaM16weytHr4ngNDaELjgSrh2w6Rss2Vvu0jDjdCNoHOUuvvPC4lH/7qFReekoqLy+R6jtLMeSoUzRjtpR22ktKc/aRrl3nSmmX/eqAQVFZWEwe0rmotMawYzCHY1LXcdbXTIyOrWsIXXgEjUTXuVq24+ZpBOTrMfDfXn7i11J+epGUl9wv0uf+48eCeqdK117zpWvvw6Rr3yP9XoXL0M2gZfg1kLoxtN3GaZ+WbdxwdOSQ4obQBYdzJLrOTZPp05wy1lDXLG43P/jQL6T88G1SfsY1wTikrj1dc8w7VroP+sjQ4ae2Rgs6AdWcMlKalW3OcHTkgHxDaBBgbEbXsXllxmlOGfOzEWDD4WBw0UIp3/+/Ui0Pwj3uKerqlq75J0j3YSfVHVZa3Ri6kVCkZnWfMzg4WPe2UwODgDRd+/LIGmiMDaJNyxir/MjtMnD3dVJ5brGPm6h/SrsfKD0fPE26DjgmBkg3hpYJYMiG7ac/r2zjcum6ITSoSE7TtY8yuc2FnT7NKbMpoA886A4Lt18llVeWYJiOodKOe0nXMWdIz8HucFJ790DgodOmOWUUISSHbDa2WT1iQxAcDABK07WPMrnN1WDTx1jtG3QniIM3/0Aq7t1CJ1PJvTvpPv4z0u1ORAloWmPQh5owXsshm/ZDBum4NN03BAHyme5Pmk4fOXJCMmy0k+sGoK28/HfSf+O/S8WdMG5KVHInnpNO/Lx0zXqv32wARtDYBNS1jzYkheSQjXXVPptPvaEhCBQH0TplcsSEZNj44jhsBnL4B267SgZv+PaEOVnktowWx8ln98lnS8+xZ8TgshnIMReA5Is616BBpkxuY62u4+LxBgYG4pNKDS4CtE6ZXPutDTptugFoK7/+gvT/9FKpjNO3jyxOu3jJvV2d9MlzpWu7XfyUAIpgsTFoox2BlMmzbH5wlafjYx8bgmDRoXXK5IihTA6bBl/r2j54z89k4NpvbrJ7BdQlRNhb9Jx+nnQfcUoMNJuBnMBTxzi0kWfZOHco3ueGGkKDTJkcSZTJYdOga5128P6rL5HyPdfDXVBCBbqOOFUmffrL8ZVSgk9OIKljGNrIs2zaDxnE3AgNoYENyVk2gs446JCpD775mgz85O86/h3EUGlH/hfvRHrO/Bvp3np7PxjAwotNQPCoI4g28iyb9kMGITfq7+8PnkMQTHIkUNYcckinvfz8w9L3g6+IrFqBIQrKW4HpW0nvZy6Wrt3mDQFVawoPmpNBlNkEljNG8yw5bgiCioSQTJvmkKnrvQTtgw/fKv3fvxADYtiCmq2AA37Sn35Vuud9qAF87h2ymoJNgqnzyA33exFgDEDZcvpot80AfeDehdJ/xQVFM6BYwyX3j4Qaopb2MGxrzimIieXw08bYEPcNEQqkzXIMwsVRBkccXvD5dxJXXgxzQaNQgQFXS9SUdScmuikoYzr6LddLoU/HQy6FHLSRZw2EOLx8Myy6UQau/rpOKeRRqABqOuhqa5tCY6RlTkmb5fDTpuX4kEGn5ToYPr5op46Flh+5TdDNBbWmAqgtaqybQuNFLIgNV6FjrI8xtCceMhioB9MT6kX5ZnDvJvwJJBML3pIKoMZ452brj8mIj8YsaRFJMfEeggMmcU4Gv11MZcXr0v/Dv8aK4C6olRVwNUatUXOLA6YlThZwq+sl0gdbfA5BY4jDRrteBAfFh07F5wysRhu4+0zH17w2FfEBNiDqxCyL14bxrG4PoR1aThoQC8DH0Z1+D4OuxXiRUXNfe9UEWJvFKmu9Nr7uHMI6oWub1tEMeCtUXJvIKnnr/Kg9345iFuJjMaMvxGEjIS/1kMEJkEAZHM1QeeNFGfyvf+RYBR+jCgADYAFMNEZYTkinPcRhSz1kYEBQaDLczzBR7oL2G9Ghf4ABsADpBgBmtHkh55/EQwabgZzjQcedTtVnH6Cp4GNcAWDhMan9A3M5xA5cy/An6d10hAZhIgdE1+EeyPLC7zC8pbxry61l2mlflKlHnCRR75SWzjWag1fWr5GBl56W/heekLU3fV/KK98czeGDYwGT0n4LRNw9mrzwhUBc0AJ+mnMA2qn7+PXr1/vjAhsDXO9u7OGi74cXteWG2CmHniAzPvc1KU3ZXK93wslojrf++Szpe7r13zbDjbu9f/L3dVdG0Ry8yqll2ixvOGSwMVB5K+NW+XbcHT157gKZ+cV/nfDNgBqWpm4hW51/hUzadT+oLSVgA4wsbpwUdvosZ0zDSSUD9Z4BMl743kSrKZo0WWac9Y1WT9PW8bFNM7/0XQFvNfnvttTwYgMAOxCxDa2Bvrq3nTSS68Ty4luk+sJj2tQSebNjTpcud7dQp1HXlrME29ZqAkbAyhIxBdcy4qhDrttD0MEkcO4dKndcjfiW05RDP9ryOcZqgnZtG7AibhpLbDcxDtUAvvgcgoHkOrny6B1t+65lz3vfF1prR9h6ttu1LduB78UCM1AIT9hotzzeQ9DBQZjkzx1+9VM/eDv+lDab3o5pxmSO0ubt27ZBhxmwI44W36QCxOcQCGAyZXA8n6Hqrr+3jWp3FLdtvnZOFMX/fy2fFZgBOxCbQeOr7T6o9sevEJ1EYhI47OX7bqSr4BOsAsAuaS9hMcemAfO6loWhjvo2SOWB/6szFcrEqYDHzmGoqQFj7XRyfMhAoH2VF//SHTNqj+4ziYU6ASrgsAOGFlc2Be3YEtrihtCbx8CKe6xPQRO7AsCQeNotYRNouz9k6AQGVVevkEpxRVPXakLKwBBYgmJsa0cDbhDt0ONzCNsUlSfvYXzBJ3gFgCVB1zhjs2inHDcEt5kJeChoQZ1RAWBJXO0W6YaAz59D0EgOR/XZB8EK6oAKaCyJMThlbCLl+ByC2w2H/0BjrB4XzIUUfPQq4LD0HzA6bElsAOrk8SEDAXxV2nBVkwsoeHsqAEyJr20G/SFVXUNwaXiqfEGdVQGNqW0IvaUl3R1x4KvP6phC7oQK1DCNMXbbxI+1uXnwxXsIGv2Pj7To9yY4R8HbXwH8hgiwzaK6k0p0CH6JpqDOrACw5R6CnFvKI0XcEAyovvkKYwreYRUgtv4fX73j0JsZHzLihnC/UVVQZ1YAvz8GItbcSq3HDREHvrOMcQXvtAo4bDX4MeZqO+sawgfULoSomELskArwIpfeHNsgjQ2R40xUD1jIE6cC/mcqM5bb0BDRhjUZKYV7olYgC1vsLeoaAgb/I6cTdYuLdadWANjaQ4RO8A3B95+xA794W1BnVsBg24C92+p4DxF3TnEPZWc2A7aqhm2MdWBLfUOEOiUQW5g6qAJJmMd7iHhb8cPoBXVmBXJg29gQPb2dWYxiq0RSsOVhpKEhoslTi9J1aAXyYOsbgo+VQR2qU7bo0HIUm6Wx1ZjrysR7iDhg8y21v5A7qQI1bGOsA9tWss5oWuc9vSWw3ZukyWKrsacc7yFQIW+csc24Llb/84/IW986W9bd2b5nVqQWpFqRdXdc59eEtY1rctgS+NA64euGAwLPMqOZ28m7N2uH0sbOVh0ckBWXfFYq61fLhvtvlgH3HMhpp58rY/WQkYFXn5O3v/NXMvDyEl+UvicXybaX3SdRd8/YFSllZmBLSmqMuj0EgqOtd2TOuOOVlct9M3Bha2+5UpZ+YYGsueEyqZqPZRnTCl4d6Jc1N35P3rzwlLgZME9l3SqpjOOfo8yDbcM5hGwzpxU1HJUxu7baTiYfcHTdWJUNa2XVNf8kb5z9QVlz0xV1DVMXOAoKLg6t/cVPZOk5R8mqq9zPVbvG0DR53jHSNXNbbRpfssEWewm7p/CHDL3qyJ2JRjNmC+7SHY+01Xn/IWsWfldWX/ctwSGEVFn9lqz6z6/L6msv9Y9CnnrkydK793wcDxkyLI6G61/ygGx4+A5Z/+ufSdU8gAOD4hAx7RPnyBanfH5Yc7QjCZgC2yxqbAgUcIc9RMZpQ2CDtjjpL2TKIR+Rd753gfQ5sDTh0LHuzuv8qzRtpkw9/PekZ6c9pXv2HOnedufMZ2CWl78mfc+7Hyf5zWPS/5z7HbEXn4wvCul5KPfudYh/BDPGH9fkMLV7A7te+OOG4O4DJ5fRTntL9fG7bPy40gHArIuu8ieXK92eobzidw3rw15j7c0/qrPjvzmaOs2diA694MTzqKvrVvtzAL3XqUs0Sves7WX6GedLu549aaZvWgWmIOKcNEDjuwzsIebsmxQ/7uxT5h8vUw46TjY+dpes+9X1snHxbQ3Hdr1oAF51jYJmaZYidy1g8oHHymYfOEUm779ApGsCXQh0mOo9hJZRB+rxHoJG7CFKu+wnlV53TWOifAPcAYMTOrxw4rfx8btlw+LbZePDt0tl7cpmca+LL7nj7uR5R8sU1whogon0Mw3xhjgsgSmIwNNn9W4Y/GGixuPAPQ4e94cNrlVzXMCZcojba7iX2zAZXPayOw94Sgbwevlp93W2lVJx943iMFFZu0oi/HyAe2I9foYBvMudd/TsuKf0zNlbJu28jz/3cFXUU0w4OXJYamITWI6YhkMGE6M950/IhuD6PXdA+pNJnPAd9rE616akAEtLbAZrjz+YQgBf/oc29jncxrZFD72ta8vEHTxJ5LDkj6cQY7u5bJC4IRhABy6ERLvX72oY00o+6N72FTR6FQCGvKhFbO3o2u4/qaSBnAnR3KMpto3jXUJBo1cBiyExBueLs0GP9xA20O9iDviQuy+7vW+t1t7847pPILnYgg+jAg67yGGoDxcYhViHRowbIhSIM/boYHe23kbCL9itvML9sHxBI64AsLO3zdlmgK5tviG0UctYUXRI+8/Oca/DqisvGXFBNvUBNHYaVy3bGtVd7UQgiAnY1fgPNN53gM1rub7m55fLim/+uf/ty+IXhIdRbocZsEs7XBBvjE45/mCKU8LBm2VoKx3xcan8pv13A2185A7BK+rqlu7td/PXH7imseC9exwkm334DwU/qDbeCZhZIui0Q7e2+KNrOvipJXUkR/sfJdEOe0r11Wc4Vls59hC8K6mtE5vJ8GOsqxdeJlPnf1SmfuBUmbzvkePyegawAmYggk48qXun+QNffA5BHxM4AHc5suB0hmzavFyW9ff+XFZ847Py+ucOkncu/8q4aNY6UBxWHlx3yAdZTGnzzpqfct3nEKmB89xb0J1b/+u0XNhE4Lgesu62a2TZl0+UNy86zV+KH/PzHYdRCVjViP/YUK2sdcbHbzvZRUzkngH2WD7uj5hXcFMB3g3+xlmHysof/YPgBtyxoMhhVIeZwg/r0ThT17yuIeAAIYmk5RIuksw7jq6CByqAy+24KWfZeSfI8ovPlA0P3Zp6f0ZgiOGbHDYeo9oIGrsk2U7mTyoRzHcWWsaeAV8bhy2Wj/8zqT7qfnqpeI6ErWWD3vfUIsEr6pkkve8/QjY//o8bYkbNgE8lHTbECpwy54BOCsk+Z926da4Xhp6Ej2A0AJsDMnXYqFfuvFaqC7/NsQs+DioQnXS2lI463f/jxof42j8ydJBuEA9+rUHYHN7vI2vBIZmDw8dBogWfEhmDK6FcX8FNBXBF02FCfOD14NYagTqz4Esi3zo6gN2EBCtzQthLp5zT9gtfSRuxSdvdoQJY8B/XNoLFkLUi5pbHJ5UI1E4t60Fpl212kugT53L8go9RBTwGDgtQjI2T2SC000cOuyX44msZOjAk+2C3ZwCP5UNPFDn0JDtuoberAq72kcOA4Me4mHMDLAc+EmXL4a/bQ8Cgg5JkLgC8dNqXpPjACpVrM+EDKFd7jUUSXtqetErG1J1D0IikkMzJtd/bPn2hyPTiuRJJxR51u6t15GqO2mssgJm1ab+WiS+5H8j9ifcQ2kFZN4CeiPbY5r7gGv3BRegijlvwVlXA1Ri1jmpfKtZYaNwwPfQYI4UN47hE6j6eSsjJQRnDyWGn7AfBucWucyU682/hKqiFFUCNUWtbf0xJLCgTN8u5PNqpgwcPGTpQy0zUXceFeb7/0SKfOp9hBR/tCrjaRq7GdTWv/ecTEz2lxo5yFo8PGXogykzmAmDnxNaGWNhK890td590J5oFjW4FXE1RW9adOLDumCxkI4Z5F9PwtpMDhDhtoYn1QkuHubeixZ4iLwbZca6WqKmuMZKoUwYHRsSpWe7H4SBpnD49GRcDG2QdA93vKXBOUdul+YDiT3MVQO1cDfWegSCn1Z8xmIxyEtcLQkzqIYPBejDK8NlFwceXb4q5x4ic5Z704r5AW1BzFfA1c7UruRrqOqO+WodMYu2hQ85DjIt5X1+fv9qJZF7lzOK8AsocxPNFn9YrK9yvwV3zNZEXH8+zxiIGd6b9/gVScs/U0uADNKvHQNZ8KJ625dHrYnRDwJHVDPQTeOawAcDhAzEG3Nv++1KR+xZ6X/EnoQK4FOCuEQF4gg8Oog7A+YIdMmPyNgPzNPcyGgICgbY8zUfAGZPVFIirLHIN8T//Utxgg2JowlcmP/6X/uQRZoJPoKmzETTwjNE2jEFdy7RZjhhQxIaAYpuBepJPNwBjtM3vFZyDjUNf5Q33k8M3/JtEzz+EtE2eqrsdJNHJX5CSeygagOKLQKc1A2MtwNRRXMqWax/lqL+/3+HkdxKwJTaFjaEOzhfzqWve0BQ4rNx9nchNl226ewvsFT52lsgHT4v3CAANr1AT0Ic6UwbXuldqftpDXNu07BsCBoCnuZaTfNpO8JlHXXPuMWCjjBPO6s2XS/TIrX7+TeVP1X0rG/dA4sQRxAbQsgadMvyUm20G5mreIGMPASNAIoVk2tI4fPRjLLtXoM/afXM896BUb/mxRC89wWV0JK/iaXD4OoO77Y1NQIChg6iT02591PNwHZMqsyEQRMC0nGajj5x5/O+HThkx+kU7OPO9DXd033WtyGvPIr1zaHv3MFh8+632uQI2DIATbNsc8IHop0w7dMpJPBSjbSE5GhgYcHjk2ztgAMaSh2zwEXD6YUt6IYaNwRhxD06t3nu9RL99FO4JS1VcBT78VJH93CMNHch8EWjqSZwbzoaBjljNtUxflk37IYP8GtAQUAAEKSQ3ayOwzLM6G0bbQ7K89KRUH7hJosW/nDgnn+5ksXrgh4eerTHn/XETpIEeahDgoXOoa67lkTaDHyvUEHAQSC03a9MAYxxQUiOk+fy8eOj4o+75U4/dKTJe3666t4+Cb13PPVakd0odmAQW2xkCn2DShzjm0Gc5YzTXMuO1DTIoyecPGUMh9U0AW7MNoHOYS85GYAzs9FGmzljaG3LWvCXy9CKpPnO/+yxj8dg9cdc9Iba624HinwO592HuqewzgyCy+AQbOl/YNsqMg03HMkZzLes8yuQ6DjIo1Tc4OOjqHj5cIDnkC9l0LP3k9EFPs9HPGOrI102ix4PsDyu4TvLKEolec1+yXbnMm0f9z5bbSHX73UV23EsiXG9whwMQCswik2tAaWNckq7HYgxtIa5tWg7lwg9K83m/bQgYCQhkkNYpkzfjZw7B9YPXxoePfspWZzzztb9uHe6XbapLXxRZ/qrI229I5B5kVl29QsR9ETdyjzUWHH7wCzz8fio+IMKPnLrdvP8pQ/d8azzbsbrl1iLv2VZk1g4Szd5ZZLPpfgksquVsAgTBp/1puh/U/WG+zqMvzYaYLL+OsWNqPUJDwMDi0pmmax9lcjsW7eTaT2C1DXGMtTLj6IeeNAZ8IB07ZGnuLwuNLMrksBFE+unTPElGDohjMA42yuTapuUsP2JBOi5NjxsCQbZ4abr2USa3Y6XZ4Qv5tV3Lemxrh083CHSQHn/I0mizBUNcyEbwOA5iGEfOXOpZMYxjnh47Tc6TZ8fM0usaAsG2eGm69jUjh2Jh03auhTbNKTNGcytDJyFP59KuuQZP2yGHALA26ppT5njQaSPX44ds2p9XtnG59HK57Gr07kklkprRdexoyBhDj6PXQzs5fVqnDRxkfUPWRrsGgTHg2q5l+rSNsuUcD/aQjzaOqeOHK9ux8ug+Bg0BwRZuJLrOHYmctvvnuOTYBhB0axvy1P+1MRqU+sh3NQ0orcyznH5wfZhhHOyjJduxhqMjBxSxIaDYIo1E17latvNon5Z1HOzWp/1aDsXBT8rya5CYozn95PBpmbGw0U6ufVky/DovSbZxw9GRQ6prCBhtwayeFWPjtZ4k2zF1XMhn/YgBhezWZvWhzHf/6sLDavUkG+06Xsv0g4Py+vLGhcYM2ex4iCHB19AQcIaKZm3N6DpWy6G5mvWHDivcQM3tuNoXktMKp+P14QB2mzcSXedqeTjzhHJgI3H8YEMgKFRAa7O6zbN+rWvZ5kEHDScGeXmbBLHNkAUfuSykHsfarB7K0zFatrHWZ/0hPckGO0iPmdgQCLSA5LXZvDTd+kJz5IlBHigUO+QZnb+6eHrEkN3arI58bdOy9eXRQzFJNthBds7/B54M58XmchgLAAAAAElFTkSuQmCC"></image>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
 )
}

const Whatsapp = () => {
  return (
    <svg width="85px" height="83px" viewBox="0 0 85 83" version="1.1">
        <title>Input Copy</title>
        <desc>Created with Sketch.</desc>
        <g id="placement_final" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <g id="malaysia_homepage_new_launch" transform="translate(-923.000000, -3853.000000)">
                <g id="Group-97" transform="translate(-3.000000, 2554.000000)">
                    <g id="Group-95">
                        <g id="cta" transform="translate(100.000000, 1070.000000)">
                            <g id="Group-87" transform="translate(132.000000, 0.000000)">
                                <g id="Group-85" transform="translate(505.000000, 15.000000)">
                                    <g id="Group-80-Copy-2" transform="translate(0.000000, 79.800000)">
                                        <g id="Group-78">
                                            <g id="Group-86" transform="translate(0.000000, 133.800000)">
                                                <g id="Group-73" transform="translate(161.049675, 0.546172)">
                                                    <g id="Input-Copy" transform="translate(28.053814, 0.000000)">
                                                        <path d="M41.2704274,0 L42.8910155,0 C65.6840432,-4.18701126e-15 84.1614429,18.4773997 84.1614429,41.2704274 L84.1614429,41.2704274 C84.1614429,64.0634551 65.6840432,82.5408549 42.8910155,82.5408549 L41.2704274,82.5408549 C18.4773997,82.5408549 2.79134084e-15,64.0634551 0,41.2704274 L0,41.2704274 C-2.79134084e-15,18.4773997 18.4773997,4.18701126e-15 41.2704274,0 Z" id="Rectangle-4-Copy" fill="#009D00"></path>
                                                        <g id="Group-42" transform="translate(19.637670, 17.883852)" fill="#FFFFFF">
                                                            <g id="Group-57">
                                                                <g id="Group-61">
                                                                    <g id="Group-49">
                                                                        <g id="Group-52">
                                                                            <path d="M0,45.5014839 C0.26063946,44.5161825 0.485737175,43.6610152 0.71083489,42.8104955 C1.47379767,40.0753544 2.25571605,37.3518324 2.98076764,34.6073961 C3.08732155,34.1670712 3.03888784,33.7043451 2.84333956,33.2944354 C-4.23657594,20.0602565 3.06606782,4.04678393 17.922517,0.621467053 C31.4117938,-2.48316209 43.538637,6.44961816 45.7540724,18.3917507 C48.0571774,30.8520961 39.749887,42.6803613 27.1491539,44.8438417 C22.1401374,45.6943613 17.3514797,44.9739758 12.7808113,42.7872573 C12.3341578,42.5939766 11.8369883,42.542779 11.3591415,42.6408563 C5.28387268,44.1304276 1.22974436,45.2644539 0,45.5014839 Z M5.35969507,40.3216798 C7.65095286,39.7314284 9.76450193,39.2132156 11.856726,38.6369072 C12.3877516,38.4809417 12.9617788,38.5607601 13.4276711,38.8553467 C17.3870214,41.1094562 21.6307057,41.9344138 26.153985,41.2256474 C36.598519,39.5803798 43.7518875,29.9969288 42.1548784,19.7906929 C41.3911839,14.7404691 38.5685145,10.2111282 34.3341518,7.24131793 C30.0997891,4.27150765 24.8170092,3.11601508 19.6972348,4.03981246 C6.77188705,6.36363666 -0.0924085357,20.4971355 6.33590832,31.6933205 C7.04674321,32.9342426 7.24340753,33.9544014 6.7647787,35.2720098 C6.20795804,36.8615055 5.85491004,38.5160684 5.35969507,40.3216798 L5.35969507,40.3216798 Z" id="Shape"></path>
                                                                            <path d="M28.8874447,33.6210317 C24.3854904,33.4630117 17.2392303,29.9842469 12.336839,21.6324227 C10.597663,18.667223 10.8488246,15.2814111 13.3983524,12.9599107 C14.2608321,12.1698105 15.3413011,12.3650117 16.3530561,12.5300032 C16.5900011,12.5695083 16.8624878,12.9413201 16.9880686,13.2271505 C17.6396673,14.6795406 18.2841576,16.1458737 18.8528255,17.6424165 C19.1537456,18.4348406 18.6016638,19.169169 17.4311557,20.4821297 C17.0615216,20.900418 16.9856992,21.2884967 17.274772,21.7672045 C19.0670785,24.8426881 21.8866961,27.2173042 25.2574478,28.4900279 C25.6877042,28.6929583 26.2045977,28.5565884 26.4729755,28.1693401 C28.309299,25.9686786 28.4964855,25.329627 29.4632209,25.778125 C34.1120811,27.9648436 34.4722375,28.0508251 34.4935625,28.5667141 C34.6641629,32.3963764 31.0625994,33.776728 28.8874447,33.6210317 Z" id="Path"></path>
                                                                        </g>
                                                                    </g>
                                                                </g>
                                                            </g>
                                                        </g>
                                                    </g>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </g>
                            </g>
                        </g>
                    </g>
                </g>
            </g>
        </g>
    </svg>
  )
}

const Rating = {
icon: <Ratings />,
}

const Socials = [
  {
    type: 'link',
    label: 'Messenger',
    url: 'https://m.me/ohmyhomemy',
    icon: <Messenger />
  },
  {
    type: 'link',
    label: 'WhatsApp',
    url: 'https://wa.me/60192665593',
    icon: <Whatsapp />
  },
  {
    type: 'click',
    label: 'Live Chat',
    icon: <Intercom />
  }
]



export default { Socials, Rating }