import React, { useState } from 'react'
import styled from 'styled-components'
import Intercom from 'react-intercom'
import Icon from './icons'

const AgentTestimonialWrapper = styled.div`
  position: relative;
  padding: 20px;
  margin: 0 0 25px;

  @media (min-width : 1128px) {
    position: sticky;
    top: 200px;
    border: 1px solid #D9DDE7;
    border-radius: 3px;
  }
`

const AgentTestimonialHeader = styled.div`
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-between;
  align-content: center;
  align-items: center;
`

const AgentImage = styled.div`
 
`

const AgentFeatured = styled.div`
  width: 94px;
  height: 94px;
  border-radius: 50%;
  overflow: hidden;
`

const AgentInfo = styled.div`
  width: 80%;
  padding-left: 22px;
`

const AgentName = styled.h2`
  font-family: Roboto,sans-serif;
  font-size: 18px;
  line-height: 21px;
  margin: 0 0 5px;
  font-weight: 500;
`

const AgentPosition = styled.h3`
  font-family: Roboto,sans-serif;
  font-size: 14px;
  line-height: 16px;
  margin: 0 0 5px;
  font-weight: 300;
`

const AgentRatings = styled.img`

`

const AgentBadge = styled.h3`
  font-family: Roboto,sans-serif;
  font-size: 16px;
  line-height: 19px;
  font-weight: 400;
  color: #FFF;
  background-color: #F5A623;
  border-radius: 12px;
  text-align: center;
  padding: 3px 8px;
  width: 100%;
  margin: 10px 0 0;
  max-width: 150px;
`

const AgentTestimonialContent = styled.div`
  font-family: Roboto, sans-serif;
  padding: 15px 15px;
  font-size: 16px;
  line-height: 24px;
  color: #000;
  background: #E2F1FF;
  margin: 25px 0;
  position: relative;
  border-radius: 5px;

  &:before {
    content: "1";
    position: absolute;
    left: 35px;
    top: -13px;
    border-right: 10px solid transparent;
    border-left: 10px solid transparent;
    border-bottom: 14px solid #E2F1FF;
    overflow: hidden;
    text-indent: -99999px;
    height: 0;
    width: 0;
  }
  
`

const AgentTestimonialFooter = styled.div`
  display: flex;
  flex-wrap: nowrap;
  justify-content: space-evenly;
`

const AgentTestimonialSocial = styled.div`
  width: 30%;
  display: block;
  text-align: center;
  font-size: 14px;

  &:hover{
    & svg{
      transform: scale(1.1);
    }
  }
`

const Social = styled.img`
  width: 50px;
  transform: scale(1);
  transition: transform 300ms ease-in-out;
  margin-bottom: 17px;
`

const SocialImage = styled.a`
  text-align: center;
  border-radius: 100%;
  height: auto;
  margin: 0 auto;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  color: #000000;
  text-decoration: none;
  cursor: pointer;

  & svg{
    width: 50px;
    transform: scale(1);
    transition: transform 300;
  }
`

const View = styled.span`
  color: #E55710;
  cursor: pointer;
`


const defaultProfile = '/themes/omhmy/assets/images/default_profile.jpg'

// const socials = [
//   {
//     type: 'messenger',
//     label: 'Messenger',
//     url: 'https://m.me/ohmyhomemy',
//     icon: '/details/messenger.svg'
//   },
//   {
//     type: 'whatsapp',
//     label: 'WhatsApp',
//     url: 'https://wa.me/60192665593',
//     icon: '/details/whatsapp.svg'
//   },
//   { type: 'intercom',
//     label: 'Live Chat',
//     icon: '/details/intercomm.svg'
//   }
// ]

interface Props {
  agent?: any,
  media_path?: any,
  more?: Boolean
}


const AgentTestimonial = ({
  agent,
  media_path,
}:Props) => {

  const [ more, setViewMore ] = useState(false)
  
  const {
    name,
    comment,
    profileUrl,
    type
  } = agent

  const socials = Icon.Socials

  const handleIntercom = () => {
  }

  return (
    <AgentTestimonialWrapper>
      <AgentTestimonialHeader>
        <AgentImage>
          <AgentFeatured
            style={{
              background: ` center / cover url(${profileUrl || defaultProfile})  no-repeat `
            }}
          />
        </AgentImage>
        <AgentInfo>
          <AgentName>{name || ''}</AgentName>
          { Icon.Rating.icon}
          <AgentBadge>{type || 'Ohmyhome Agent'}</AgentBadge>
        </AgentInfo>
      </AgentTestimonialHeader>
      <AgentTestimonialContent>
        { comment && comment.length > 200 ? `${comment.slice(0, more ? comment.length : 199)}${!more ? '...' : ''}` : comment}
        { comment && comment.length > 200 ? <View onClick={ () => setViewMore(!more)}> {more ? 'View Less' : 'View More'}</View> : ''}
      </AgentTestimonialContent>
      <AgentTestimonialFooter>
        {
          socials && socials.map((item, key) => {
            const {
              type,
              label,
              url,
              icon
            } = item

            return(
              <AgentTestimonialSocial key={key}>
                {
                  type === 'click' ?
                    <SocialImage
                      onClick={() => handleIntercom()}
                      id={'custom_launcher_selector'}
                    >
                      {icon}
                      { label }
                    </SocialImage>
                    : <SocialImage href={url || '#'} target="_blank">
                      {icon}
                      { label }
                    </SocialImage>
                }
              </AgentTestimonialSocial>
            )
          })
        }
      </AgentTestimonialFooter>
    </AgentTestimonialWrapper>
  )
}

export default AgentTestimonial
