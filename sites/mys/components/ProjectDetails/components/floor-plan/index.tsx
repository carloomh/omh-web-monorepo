import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import Responsive from 'react-responsive'

import ModalFloorPlan from './modal-floorplan'

const Mobile = props => <Responsive {...props} maxWidth={1127} />
const Desktop = props => <Responsive {...props} minWidth={1128} />

const Container = styled.section`
  margin-bottom: 40px;
  
  &:before{
    content: '';
    margin-top: -200px;
    height: 200px;
    visibility: hidden; 
    pointer-events: none;
    display: block; 
  }
`

const Header = styled.header`
  font-size: 22px;
  font-weight: 500;
  margin: 0 0 22px;
  padding: 0 20px;

  @media (min-width : 1128px) {
    padding: 0;
  }
`

const Content = styled.div`
  border: 1px solid #D9DDE7;
  
  @media (min-width : 1128px) {
    border-radius: 3px;
  }
`

const Row = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 26% [col-start]) 16%;
  border-bottom: 1px #D9DDE7 solid;
  align-items: center;
  justify-items: center;
  padding: 12px 0;
  justify-content: space-around;
  cursor: pointer;

  &:last-child{
    border: 0;
  }

  @media (min-width : 1128px) {
    grid-template-columns: 22% repeat(3, 26% [col-start]);
  }
`

const Th = styled.div`
  font-size: 14px;
  font-weight: 500;
  line-height: 22px;
  text-align: center;

  @media (min-width : 1128px) {
    font-size: 18px;
  }
`

const Col = styled.div`
  text-align: center;
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;

  & p{
    margin: 0;
    font-size: 12px;
    line-height: 22px;
  }

  @media (min-width : 1128px) {
    & p{
      font-size: 14px;
    }
  }
`

const FloorImage = styled.div`
  height: 45px;
  width: 45px;
  background-color: #FFFFFF !important;
`

const View = styled.a`

`

interface FloorPlanProps {
  floorPlans: any,
  media_path: any,
}

const FloorPlan = ({
  floorPlans,
  media_path,
} : FloorPlanProps) => {

  const [ activeFloor, setActive ] = React.useState(null)

  const handleActiveFloorPlan = key => {
    setActive(key)
  }

  return (
    <Container id={'floor_plan'}>
      <Header>Floor Plan</Header>
      <Content>
        <Row
          style={{
            'backgroundColor': '#F9F9FA'
          }}
        >
          <Desktop>
            <Th> Image </Th>
          </Desktop>
          <Th> Unit Type </Th>
          <Th> Built-up Size </Th>
          <Th> Starting Price </Th>
          <Mobile>
            <Th> View </Th>
          </Mobile>
        </Row>
        {
          floorPlans && floorPlans.map( (item, key) => {
            return(
              item ?
                <Row
                  key={key}
                  onClick={() => handleActiveFloorPlan(key)}
                >
                  <Desktop>
                    <Col>
                      {
                        item.imageUrl ? 
                          <FloorImage style={{
                            background: `center / contain url(${item.imageUrl}?h=144&w=144&c=limit) no-repeat`,
                          }}
                          />
                          : 
                          <FloorImage style={{
                            background: `center / contain url('/themes/omhmy/assets/images/layout_not_available.svg') no-repeat`,
                          }}
                          />
                      }
                    </Col>
                  </Desktop>
                  <Col>
                    <p>{ item.unit }</p>
                    <p>{ item.description }</p>
                  </Col>
                  <Col>
                    <p>{ item.size }sqft </p>
                  </Col>
                  <Col>
                    <p>{ item.minAmount }</p>
                  </Col>
                  <Mobile>
                    <Col>
                      <View onClick={() => handleActiveFloorPlan(key)}>View</View>
                    </Col>
                  </Mobile>
                </Row>
                : ''
            )
          })
        }
        {
          activeFloor !== null ? 
            <ModalFloorPlan
              floorPlans={floorPlans}
              activeFloorPlan={activeFloor}
              handleActiveFloorPlan={handleActiveFloorPlan}
            />
            : ''
        }
      </Content>
    </Container>
  )
}

export default FloorPlan