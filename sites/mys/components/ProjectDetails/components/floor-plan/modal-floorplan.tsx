import React, { Component, Fragment } from 'react'
import styled from 'styled-components'
import Responsive from 'react-responsive'
import { SlickSlider } from "omhwebui"
import numeral from 'numeral'

import Dialog from '@material-ui/core/Dialog'

const Mobile = props => <Responsive {...props} maxWidth={1127} />
const Desktop = props => <Responsive {...props} minWidth={1128} />


const ModalContainer = styled.div`
  border-radius: 10px;
  background: #FFFFFF;
  padding: 20px;
  width: auto;
  margin: 25px;
`

const ModalContent = styled.div`
  width: auto;
  display: grid;
  grid-template-columns: 100%;
  justify-items: center;
  height: 100vh;
  overflow: scroll;
  margin-top: 50px;

  @media (min-width : 1124px) {
    grid-template-columns: 50% 50%;
    width: 770px;
    height: auto;
    margin-top: 0;
    overflow: hidden;
  }
`

const FloorColumn = styled.div`
  display: flex;
  justify-content: center;
  flex-direction: column;
  width: 100%;
  padding: 0 25px;
  box-sizing: border-box;
  margin-bottom: 60px;
  
  @media (min-width : 1124px) {
    margin-bottom: 0;
  }
`

const Slider = styled.div`
  width: 100%;
  padding: 0;
  box-sizing: border-box;
  outline: none;
  flex: 0 0 100%;

  @media (min-width : 1124px) {
    padding: 20px;
  }
`
const SliderItem = styled.div`
  min-height: 400px;
  margin-bottom: 20px;

  @media (min-width : 1124px) {
    min-height: 470px;
    margin-bottom: 0;
  }
`

const FloorType = styled.h5`
  font-size: 25px;
  font-weight: 600;
  color: #E55710;
  margin: 0 0 9px;
  line-height: 24px;
`
const FloorDesc = styled.p`
  font-size: 16px;
  color: #616161;
  margin: 0 0 10px;
  line-height: 20px;
`

const SizePrice = styled.div`
  display: grid;
  grid-template-columns: 50% 50%;
  margin: 30px 0px 45px;

  & section{
    &:first-child{
      border-right: 1px solid rgba(151, 151, 151, .3);
      margin-right: 30px;
    }
    & h4{
      margin: 0 0 8px;
      font-size: 16px;
      color: #616161;
      font-weight: 400;
    }
    & h3{
      margin: 0;
      font-size: 22px;
      font-weight: 600;
    }
  }
`
const OtherUnit = styled.div`
  & h5{
    margin: 0 0 20px;
    color: #212121;
    font-weight: 500;
  }
`

const Units = styled.div`
  display: flex;
  overflow: auto;
  padding-bottom: 10px;
`

const Unit = styled.div`
  margin: 0 15px 10px 0;
  cursor: pointer;

  &:last-child{
    margin-right: 0;
  }
`
const UnitImage = styled.div`
  width: 70px;
  height: 70px;
  border: 2px transparent solid;

  &.active{
    border: 2px #e5571099 solid;
    border-radius: 3px;
    box-shadow: 0 2px 3px 0 #6b6b6b1f;
  }
`

const UnitType = styled.p`
  font-size: 12px;
  margin: 8px 2px 0;
  text-align: center;
  line-height: 15px;

  &.active {
    font-weight: 500;
  }
`

const MobileScroll = styled.div`
  display: flex;
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: normal;
  -webkit-overflow-scrolling: touch;
`

const DefaultFloorPlan = () => (
  <svg width="209px" height="209px" viewBox="0 0 209 209" version="1.1">
      <g id="-" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd" opacity="0.300000012">
          <g id="Floor-Plan(no_availble_floorplan)" transform="translate(-430.000000, -356.000000)" fill="#000000">
              <g id="Group-49" transform="translate(426.900000, 356.875000)">
                  <polygon points="138.597895 0 138.349091 8.71856287 203.28689 8.96268263 203.038086 109.84891 178.406507 109.599808 178.157703 118.323353 203.28689 118.572455 203.038086 199.525557 117.205742 199.276455 117.454545 143.238515 152.038278 143.482635 152.282105 134.75909 73.9088995 134.509988 73.6650718 143.238515 108.746411 143.482635 108.492632 199.525557 12.7031579 199.276455 12.9569378 8.71856287 76.9244019 8.86304192 114.662967 37.5147305 120.08689 30.6843593 79.8055502 0.24411976 4.24382775 0 4 208 211.751196 208 212 0.24411976"></polygon>
              </g>
          </g>
      </g>
  </svg>
)


interface ModalFloorPlanProps {
  floorPlans?: any,
  activeFloorPlan?: any,
  handleActiveFloorPlan?: any,
}

const ModalFloorPlan = ({
  floorPlans,
  activeFloorPlan,
  handleActiveFloorPlan
}: ModalFloorPlanProps) => {

  // const [ showModal, setModal ] = React.useState(activeFloorPlan !== null)
  let [ ref, handleSlide ] = React.useState(null)

  return(
    <Dialog
      onClose={() => handleActiveFloorPlan(null)}
      open={activeFloorPlan !== null}
      maxWidth={'lg'}
    >
      <ModalContainer>
        <ModalContent>
          <FloorColumn>
            <Desktop>
              <SlickSlider
                slidesToScroll = {1}
                slidesToShow = {1}
                initialSlide = {activeFloorPlan}
                afterChange={handleActiveFloorPlan}
                ref = { ref => handleSlide(ref) }
              >
                {
                  floorPlans && floorPlans.map((item, key) => {
                    return(
                      <Slider key={key}>
                        {
                          item.imageUrl ? 
                            <SliderItem
                              style={{
                                background: `center / contain url(${item.imageUrl}?h=144&w=144&c=limit) no-repeat`,
                              }}
                            />
                            :
                            <SliderItem
                              style={{
                                background: `center / contain url('/themes/omhmy/assets/images/layout_not_available.svg') no-repeat`,
                              }}
                            />
                        }
                      </Slider>
                    )
                  })
                }
              </SlickSlider>
            </Desktop>
          </FloorColumn>
          <FloorColumn>
            {
              floorPlans && floorPlans[activeFloorPlan] ?
                <React.Fragment>
                  <FloorType>{floorPlans[activeFloorPlan] && floorPlans[activeFloorPlan].unit}</FloorType>
                  <FloorDesc>{floorPlans[activeFloorPlan] && floorPlans[activeFloorPlan].description}</FloorDesc>
                  <SizePrice>
                    <section>
                      <h4>Built Up Size</h4>
                      <h3>{numeral(floorPlans[activeFloorPlan].size).format('0.0')} sqft</h3>
                    </section>
                    <section>
                      <h4>Starting Price</h4>
                      <h3>RM {numeral(floorPlans[activeFloorPlan].minAmount).format('0,0')}</h3>
                    </section>
                  </SizePrice>
                  <OtherUnit>
                    <h5>Other Unit Types</h5>
                    <Units>
                      {
                        floorPlans && floorPlans.length && floorPlans.map( (item, key) => {
                          return(
                            <Unit
                              key={key}
                              onClick={() => handleActiveFloorPlan(key)}
                            >
                              {
                                item.imageUrl ?
                                  <UnitImage
                                    style={{
                                      background: `center / contain url(${ item.imageUrl }) no-repeat`
                                    }}
                                    className={activeFloorPlan === key ? 'active' : ''}
                                  />
                                  : 
                                  <UnitImage><DefaultFloorPlan /></UnitImage>
                              }
                              <UnitType className={activeFloorPlan === key ? 'active' : ''}>{item.unit}</UnitType>
                            </Unit>
                          )
                        })
                      }
                    </Units>
                  </OtherUnit>
                </React.Fragment>
                : ''
            }
          </FloorColumn>
        </ModalContent>
      </ModalContainer>
    </Dialog>
  )
}

export default ModalFloorPlan

