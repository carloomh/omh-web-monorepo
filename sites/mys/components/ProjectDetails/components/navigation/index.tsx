import React from 'react'
import styled from 'styled-components'

const Nav = styled.nav`
  height: 50px;
  background-color: #F9F9FA;
  width: 100%;
  z-index: 9;
  box-shadow: 0 2px 4px 0 rgba(0,0,0,0.17);

  @media (min-width : 1128px) {
    height: 44px;
  }
`
const Container = styled.div`
  margin: 0px auto;
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  overflow: scroll;
  width: 100%;

  @media (min-width : 1128px) {
    overflow: hidden;
    width: 972px;
  }
`

const Item = styled.a`
  text-decoration: none;
  height: 100%;
  padding: 10px 15px;
  font-size: 16px;
  display: flex;
  align-items: center;
  color: #9B9B9B;
  text-transform: capitalize;
  height: 100%;
  box-sizing: border-box;
  cursor: pointer;
  border-bottom: 3px solid #F9F9FA;
  white-space: nowrap;

  &.active{
    border-bottom: 3px solid #EC6423;
    color: #EC6423;
  }

  &:hover{
    color: #EC6423;
  }

  @media (min-width : 1128px) {
    font-size: 17px;
  }
`

const Navigation = ({ 
  links,
  active,
  scrollTo
}) => {
  return (
    <Nav>
      <Container>
        {
          links && links.map((link, index) => {
            return(
              <Item
                key={index}
                onClick={(e) => scrollTo(`${link}`, e)}
                className={ `navlinks ${link === active ? 'active' : ''}`}
                href={`#${link}`}
              >{link.replace(/_/g, ' ')}</Item>
            )
          }) 
        }
      </Container>
    </Nav>
  )
}
export default Navigation