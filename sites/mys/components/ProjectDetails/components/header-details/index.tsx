import React from 'react'
import styled from 'styled-components'
import moment from 'moment'
import numeral from 'numeral'
import Uptodate from '../uptodate'

const Header = styled.header`
  width: 100%;
  z-index: 99;
  background-color: #FFFFFF;
  transition: height 200ms ease-out;
  overflow: hidden;
`

const Content = styled.div`
  display: flex;
  width: 972px;
  margin: 0px auto;
  padding: 23px 0;
  height: 100%;
  box-sizing: border-box;
`

const Details = styled.div`
  width: 60%;
  margin-right: 40px;
  min-width: 560px;
`
const Contact = styled.div`
  width: 40%;
`

const ProjectTitle = styled.h1`
  font-size: 24px;
  font-weight: bold;
  line-height: 24px;
  margin: 0 0 6px;
`

const ProjectAddress = styled.p`
  color: #4A4A4A;
  opacity: 0.7;
  font-size: 16px;
  line-height: 20px;
  margin: 0;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  width: 95%;

  & img{
    height: 13px;
  }
`
const LaunchDate = styled.div`
  color: #4A4A4A;
  margin-top: 12px;
  font-size: 15px;

  & span{
    color: #000000;
    margin: 0 15px 0 0;
  }
`

const UpdateLink = styled.a`
  color: #EE620F;
  text-decoration: underline !important;
  cursor: pointer;
`

const ProjectPrice = styled.h2`
  font-size: 20px;
  letter-spacing: -0.5px;
  line-height: 22px;
  font-weight: 500;
  margin: 0 0 12px;
  display: flex;
  align-items: flex-end;
  height: 24px;
`

const ProjectPsf = styled.p`
  opacity: 0.7;
  color: #4A4A4A;
  font-size: 16px;
  line-height: 20px;
  margin: 0;
  margin-left: 10px;
`

const CallUs = styled.p`
  margin: 0;
  display: flex;
  align-items: center;
  justify-content: flex-start;

  & a {
    border: 1px solid #E55710;
    border-radius: 5px;
    padding: 8px;
    text-align: center;
    text-decoration: none;
    color: #E55710;
    min-width: 128px;
    display: flex;
    justify-content: space-between;
    margin-left: 10px;
    font-size: 14px;

    & svg{
      transform: rotate(6deg);
      height: 14px;
      margin-top: 3px;
    }

    & span{
      margin-left: 10px;
    }
  }
`


const Phone = () => (
  <svg width="17px" height="17px" viewBox="0 0 17 17" version="1.1">
      <defs></defs>
      <g id="Homepage-2" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
          <g id="Find-a-Home-New" transform="translate(-1297.000000, -31.000000)" fill="#E55710" fillRule="nonzero">
              <g id="Menu-1" transform="translate(33.000000, 15.000000)">
                  <g id="Group-6" transform="translate(1255.000000, 8.000000)">
                      <g id="Group-5">
                          <g id="Group-16">
                              <g id="Group-17" transform="translate(9.000000, 8.000000)">
                                  <g id="Group-22">
                                      <g id="Group-23">
                                          <g id="ic_phone_48px">
                                              <path d="M3.42361111,7.36194444 C4.78361111,10.0347222 6.97,12.2211111 9.6475,13.5811111 L11.7252778,11.4986111 C11.985,11.2388889 12.3580556,11.1633333 12.6838889,11.2672222 C13.7416667,11.6166667 14.8797222,11.8055556 16.0555556,11.8055556 C16.5797222,11.8055556 17,12.2258333 17,12.75 L17,16.0555556 C17,16.5797222 16.5797222,17 16.0555556,17 C7.18722222,17 0,9.81277778 0,0.944444444 C0,0.420277778 0.425,0 0.944444444,0 L4.25,0 C4.77416667,0 5.19444444,0.420277778 5.19444444,0.944444444 C5.19444444,2.12027778 5.38333333,3.25833333 5.73277778,4.31611111 C5.83666667,4.64194444 5.76111111,5.015 5.50138889,5.27472222 L3.42361111,7.36194444 Z" id="Shape"></path>
                                          </g>
                                      </g>
                                  </g>
                              </g>
                          </g>
                      </g>
                  </g>
              </g>
          </g>
      </g>
  </svg>
)

const Pin = () => (
  <svg viewBox="0 0 50 50" version="1.1" fill="#cccccc" width="15px" height="13px">
    <g id="surface1" fill="#cccccc">
    <path d="M 25 1 C 16.179688 1 9 8.179688 9 17 C 9 31.113281 23.628906 47.945313 24.25 48.65625 C 24.441406 48.875 24.710938 49 25 49 C 25.308594 48.980469 25.558594 48.875 25.75 48.65625 C 26.371094 47.933594 41 30.8125 41 17 C 41 8.179688 33.820313 1 25 1 Z M 25 12 C 28.3125 12 31 14.6875 31 18 C 31 21.3125 28.3125 24 25 24 C 21.6875 24 19 21.3125 19 18 C 19 14.6875 21.6875 12 25 12 Z " fill="#cccccc"/>
    </g>
  </svg>
)

interface HeaderProps {
  show: any,
  details: any,
  telephone: any,
}

const HeaderDetails = ({ show, details, telephone }: HeaderProps) => {

  const [modal, handleUpdates] = React.useState(false)
  const { address } = details

  return (
    <React.Fragment>
      <Header
        style={{
          height: show ? '118px' : 0
        }}
      >
        <Content>
          <Details>
            <ProjectTitle>{ details && details.name }</ProjectTitle>
            { address && address.addressStreetName &&  address.stateName && 
              <ProjectAddress><Pin />{address.addressStreetName}, {address.townName}, {address.stateName}</ProjectAddress> }
            <LaunchDate>
              TOP: <span>{ moment(details.started_date).format('YYYY')}</span>
              <UpdateLink
                onClick={ () => handleUpdates(true)}
              >Get up-to-date on new launches</UpdateLink>
            </LaunchDate>
          </Details>
          <Contact>
            {
              details.minAmount &&
              details.maxAmount &&
              <ProjectPrice>
                <span>RM {numeral(details.minAmount).format('0,0')} - RM {numeral(details.maxAmount).format('RM0,0') }</span>
                <ProjectPsf>RM {details.minAmount} psf</ProjectPsf>
              </ProjectPrice>
            }
            
            <CallUs>Interested? Call Us! <a href={`tel:${ telephone || '60 16-299 1366' }`}><Phone /><span>+{ telephone || '60 16-299 1366' }</span></a> </CallUs>
          </Contact>
        </Content>
      </Header>
      <Uptodate
        show={modal}
        handleShow={handleUpdates}
      />
    </React.Fragment>
  )
}

export default HeaderDetails