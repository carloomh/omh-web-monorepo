import * as React from "react"
import Link from 'next/link'
import Header from './Header'
import Footer from './Footer'

const Layout = ({ children, title = 'Ohmyhome' }) => (
  <React.Fragment>
    <Header />
      {children}
    <Footer />
    <style jsx global>{`
      body { }
      a {
        cursor: point;
      }
    `}</style>
  </React.Fragment>
)

export default Layout