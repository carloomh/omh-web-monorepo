import * as React from "react"
import Link from 'next/link'
import styled from 'styled-components'

const MainHader = styled.header`
  position: fixed;
  width: 100%;
  background: #FFFFFF;
  height: 40px;
  z-index: 99;
  & nav a {
    font-size: 16px;
    text-decoration: none;
    padding: 5px 10px;
  }
`

const Header = () => (
  <MainHader style={{
    position: 'fixed',
    width: '100%',
    background: '#FFFFFF',
    height: '40px',
    zIndex: 99
  }}>
    <nav>
      <Link href='/'>
        <a>Home</a>
      </Link>
      |
      <Link href='/profile'>
        <a>About</a>
      </Link>
      |
      <Link href='/contact'>
        <a>Contact</a>
      </Link>
      |
      <Link href='/profile'>
        <a>Profile</a>
      </Link>
    </nav>
  </MainHader>
)

export default Header