import * as React from 'react'
import { storiesOf } from '@storybook/react'
import ImageLightbox from '../components/ImageLightbox'

const imageSrc = "https://misdirection.ohmyhome.com/image/xcprbt4tmkmk3a6o7udc"

storiesOf('ImageLightbox', module)
  
  .add('Basic', () => (
    <div>
      <ImageLightbox
        mainSrc={imageSrc}
        onCloseRequest={() => {}}
      />
    </div>
  ))