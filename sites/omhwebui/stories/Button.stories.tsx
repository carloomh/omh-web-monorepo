import * as React from 'react'
import { muiTheme } from 'storybook-addon-material-ui'
import { storiesOf } from '@storybook/react'

import Theme from '../src/theme'
import Button from '../components/Button/default'
import BtnSocial from '../components/Button/social'

import defaultReadme from '../components/Button/docs/default.md'
import socialReadme from '../components/Button/docs/social.md'

storiesOf('Button', module)
  .addDecorator(muiTheme([Theme]))
  .addParameters({
    readme: {
      sidebar: defaultReadme,
    },
  })
  .add('Basic', () => (
    <React.Fragment>
      <Button
        className={null}
        action={() => console.log('button')}
        variant="contained"
        color="primary"
        text="Open login"
      />
      <div style={{ marginBottom: '20px' }} />
      <Button
        className={null}
        action={() => console.log('button')}
        variant="outlined"
        text="Outlined"
      />
    </React.Fragment>
  ))
  .addParameters({
    readme: {
      sidebar: socialReadme,
    },
  })
  .add('Facebook', () => (
    <BtnSocial
      icon="facebook"
      variant="contained"
      text="Log in with Facebook"
      action={() => console.log('true')}
    />
  ))
  .add('Google', () => (
    <BtnSocial
      icon="google"
      variant="contained"
      text="Log in with Google"
      action={() => console.log('google')}
    />
  ))