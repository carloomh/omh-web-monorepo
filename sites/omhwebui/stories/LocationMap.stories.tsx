import * as React from 'react'
import { muiTheme } from 'storybook-addon-material-ui'
import { storiesOf } from '@storybook/react'

import Theme from '../src/theme'
import LocationMap from '../components/LocationMap'
import defaultReadme from '../components/LocationMap/readme.md'

const markers = [
  {
    "_id": 33368,
    "type": 2,
    "name": "Ampang Park",
    "description": "Kelana Jaya",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.1599344",
      "longitude": "101.7191329",
      "_id": 0
    }
  },
  {
    "_id": 33369,
    "type": 2,
    "name": "Klcc",
    "description": "Kelana Jaya",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.159408",
      "longitude": "101.7135734",
      "_id": 0
    }
  },
  {
    "_id": 33449,
    "type": 2,
    "name": "Raja Chulan",
    "description": "KL Monorail",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.1508798",
      "longitude": "101.7104046",
      "_id": 0
    }
  },
  {
    "_id": 33428,
    "type": 2,
    "name": "Tun Razak Exchange",
    "description": "Sungai Buloh - Kajang MRT",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.1422509",
      "longitude": "101.7201648",
      "_id": 0
    }
  },
  {
    "_id": 33448,
    "type": 2,
    "name": "Airasia\u2013Bukit Bintang",
    "description": "KL Monorail",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.146015",
      "longitude": "101.7114894",
      "_id": 0
    }
  },
  {
    "_id": 33367,
    "type": 2,
    "name": "Damai",
    "description": "Kelana Jaya",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.1645034",
      "longitude": "101.7243652",
      "_id": 0
    }
  },
  {
    "_id": 33427,
    "type": 1,
    "name": "Pavilion Kuala Lumpur\u2013Bukit Bintang",
    "description": "Sungai Buloh - Kajang MRT",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.1462879",
      "longitude": "101.7108541",
      "_id": 0
    }
  },
  {
    "_id": 33366,
    "type": 1,
    "name": "Dato' Keramat",
    "description": "Kelana Jaya",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.166547",
      "longitude": "101.71961",
      "_id": 0
    }
  },
  {
    "_id": 33447,
    "type": 2,
    "name": "Imbi",
    "description": "KL Monorail",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.14283",
      "longitude": "101.70945",
      "_id": 0
    }
  },
  {
    "_id": 33370,
    "type": 2,
    "name": "Kampung Baru",
    "description": "Kelana Jaya",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.1613893",
      "longitude": "101.7066655",
      "_id": 0
    }
  },
  {
    "_id": 33450,
    "type": 2,
    "name": "Bukit Nanas",
    "description": "KL Monorail",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.1562023",
      "longitude": "101.704792",
      "_id": 0
    }
  },
  {
    "_id": 33371,
    "type": 2,
    "name": "Dang Wangi",
    "description": "Kelana Jaya",
    "address": {
      "countryCode": "MYS",
      "latitude": "3.1562218",
      "longitude": "101.7029561",
      "_id": 0
    }
  }
]

storiesOf('LocationMap', module)
  .addDecorator(muiTheme([Theme]))
  .addParameters({
    readme: {
      sidebar: defaultReadme,
    },
  })
  .add('LocationMap', () => (
    <div>
      <LocationMap
        showList={true}
        markers={markers}
        filters={[2,1]}
        centerLat={3.1562218}
        centerLong={101.7029561}
        selectedFilter={'mrt'}
      />
    </div>
  ))