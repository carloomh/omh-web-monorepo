import * as React from 'react'
import { storiesOf } from '@storybook/react'
import { muiTheme } from 'storybook-addon-material-ui'
import { Formik, Form } from 'formik';
import * as Yup from 'yup'
// import figmaDecorator from 'storybook-addon-figma'
import MobileInput from '../components/MobileInput'

import Theme from '../src/theme'
import readme from '../components/MobileInput/readme.md'

// const figmaUrl = 'https://www.figma.com/file/M4qkM5L9tlnE5uqy6djecvMe/Design-System?node-id=22%3A19'

const countryList = [
  {
    code: "SGP",
    img: "https://s3-ap-southeast-1.amazonaws.com/omh0pub/static/SGP.png",
    name: "Singapore",
    phone: 65
  },
  {
    code: "SGP",
    img: "https://s3-ap-southeast-1.amazonaws.com/omh0pub/static/SGP.png",
    name: "Singapore",
    phone: 65
  }
]

storiesOf('Mobile Input', module)
  .addDecorator(muiTheme([Theme]))
  // .addDecorator(figmaDecorator({
  //   url: figmaUrl
  // }))
  .addParameters({
    readme: {
      sidebar: readme,
    },
  })
  .add('Basic', () => (
    <div
      style={{
        width: "360px",
        padding: "40px"
      }}
    >
      <Formik
        initialValues={{
          inputBase: 'Naked input',
        }}
        onSubmit={() => console.log('submit')}
        validationSchema={Yup.object().shape({
          mobileNumber: Yup
            .string()
            .required('Invalid mobile number. Please try again ')
        })}
        render={() => (
          <Form>
            <MobileInput
              name="mobileNumber"
              label="Mobile Number"
              type="tel"
              country={"0"}
              setCountry={() => console.log('set country')}
              countryList={countryList}
              saveValue={() => console.log('save value')}
              value={""}
            />
          </Form>
        )}
      />
    </div>
  ))