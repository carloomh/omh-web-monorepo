import * as React from 'react'
import { muiTheme } from 'storybook-addon-material-ui'
import { storiesOf } from '@storybook/react'
import Theme from '../src/theme'
import { COLOR } from '../src/colors'
import colorReadme from '../src/colors/readme.md'

const ColorItem = ({ color, name }) => {
  return (
    <div
      style={{
        padding: '10px'
      }}
    >
      <span
        style={{
          fontSize: '12px',
        }}
      >
        {name}
      </span>
      <div
        style={{
          backgroundColor: color,
          width: '100px',
          height: '100px',
          marginTop: '5px',
          marginBottom: '5px'
        }}
      />
      <span
        style={{ fontSize: '12px' }}
      >
        {color}
      </span>
    </div>
  )
}

const ColorList = () => {
  return (
    <div
      style={{
        display: 'flex',
        flexWrap: 'wrap',
        padding: '7px'
      }}
    >
      {Object.keys(COLOR).map((item) => {
        console.log(item)
        return <ColorItem color={COLOR[item]} name={item} />
      })}
    </div>
  )
}

storiesOf('Color Profile', module)
  .addDecorator(muiTheme([Theme]))
  .addParameters({
    readme: {
      sidebar: colorReadme,
    },
  })
  .add('Primary', () => (
    <ColorList />
  ))