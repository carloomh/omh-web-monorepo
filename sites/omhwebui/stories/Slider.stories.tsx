import * as React from 'react'
import { muiTheme } from 'storybook-addon-material-ui'
import { storiesOf } from '@storybook/react'

import Theme from '../src/theme'
import SlickSlider from '../components/SlickSlider'
import defaultReadme from '../components/SlickSlider/readme.md'

const items = [
  'https://misdirection.ohmyhome.com/cms/media/qa_test_images/m-vertica-kl-city/Photos/m-vertica-reshoot-facade-aerial-view-resized.jpg?h=1024&w=1024&c=limit',
  'https://misdirection.ohmyhome.com/cms/media/qa_test_images/m-vertica-kl-city/Photos/1704mvertica-overall-facility01-resized.jpg?h=1024&w=1024&c=limit'
]

storiesOf('Slider', module)
  .addDecorator(muiTheme([Theme]))
  .addParameters({
    readme: {
      sidebar: defaultReadme,
    },
  })
  .add('Slider', () => (
    <div
      style={{
        maxWidth: '568px',
      }}
    >
      <SlickSlider
        slidesToShow={1}
        slidesToScroll={1}
      >
        {
          items.map( (item, key) => {
            return (
              <div
                key={key}
              >
                <div
                  style={{
                    background: `center / cover url(${item}) no-repeat`,
                    height: '300px'
                  }}  
                />
              </div>
            )
          })
        }
      </SlickSlider>
    </div>
  ))