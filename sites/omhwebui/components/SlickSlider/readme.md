# Mobile Number Input

```js
  import SlickSlider from 'omhwebui/components/Slider'
```


# Usage
```js
  <SlickSlider
    slidesToScroll={1}
    slidesToShow={1}
    slidesToShow={0}
  />
```

### Props ###

Name | Type | Options | Required
------------ | ------------- | ------------- | -------------
**slidesToScroll** | Number | Number of slides to scroll | yes
**slidesToShow** | Number | Number of slides to show | yes
**initialSlide** | Number | Index of initial slide | no