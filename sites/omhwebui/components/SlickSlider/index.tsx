import * as React from "react"
import Slider from 'react-slick'
import styled from 'styled-components'

import { ArrowRight, ArrowLeft } from './arrows'

import 'slick-carousel/slick/slick.css'

const Arrow = styled.div`
  color: #FFFFFF;
  z-index: 1;
  position: absolute;
  top: 50%;
  bottom: 50%;
  margin-top: -12px;
  height: 38px;
  width: 38px;
  opacity: 0.5;
  cursor: pointer;
  transition:  0.2s opacity ease-in-out;
  background: rgba(0, 0, 0, 0.5);
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  padding: 7px;
  box-sizing: border-box;

  ${ props => props.dir == 'right' ? 'right: 4px;' : ''}
  ${ props => props.dir == 'left' ? 'left: 4px;' : ''}

  &:hover{
    opacity: 1;
  }

  & img{
    width: 25px;
  }

  &.slick-prev img {
    margin-right: 2px;
  }

  &.slick-next img {
    margin-right: -2px;
  }
`


interface TextInputProps {
  children: object,
  nextArrow?: Function,
  prevArrow?: Function,
  slidesToScroll?: number,
  slidesToShow?: number
  afterChange?: Function
  initialSlide?: number,
  ref?: any
}

interface ArrowProps {
  // className?: string,
  onClick?: () => {},
  dir?: string,
  // slideCount?: number,
  // currentSlide?: number,
  slidesToShow: number,
  // hideRight?: Boolean,
  // hideLeft?: Boolean,
}

const SlickArrow = (props: ArrowProps) => {
  const {
    // className,
    onClick,
    dir,
    // slideCount,
    // currentSlide,
    // slidesToShow,
  } = props

  return (
    <Arrow
      // className={className}
      onClick={onClick}
      dir={dir}
      // hideRight={currentSlide + slidesToShow === slideCount }
      // hideLeft={currentSlide === 0 }
    >
      { 
        dir === 'right' 
          ? <ArrowRight /> 
          : <ArrowLeft />
      }
    </Arrow>
  )
}


const settings = {
  dots: false,
  infinite: false,
  speed: 300,
  lazyLoad: true
}

const SlickSlider = React.forwardRef((props: TextInputProps, ref) => {
  const {
    children,
    // nextArrow,
    // prevArrow,
    slidesToScroll,
    slidesToShow,
    // afterChange,
    initialSlide,
    afterChange,
  } = props

  return(
    <Slider
      {...settings}
      ref={ref}
      slidesToScroll={slidesToScroll}
      slidesToShow={slidesToShow}
      afterChange={afterChange || ''}
      initialSlide={initialSlide || 0}
      nextArrow={<SlickArrow dir='right' slidesToShow={slidesToShow} />}
      prevArrow={<SlickArrow dir='left' slidesToShow={slidesToShow} />}
    >
      {children}
    </Slider>
  )
})

export default SlickSlider
