import * as React from 'react'
import styled from 'styled-components'
import { Field } from 'formik'
import { fieldToTextField } from 'formik-material-ui'
import MuiTextField from '@material-ui/core/TextField';
// components
import OutlinedInput from '@material-ui/core/OutlinedInput'
import FormControl from '@material-ui/core/FormControl'
import Select from '@material-ui/core/Select'
import MenuItem from '@material-ui/core/MenuItem'
import CircularProgress from '@material-ui/core/CircularProgress'

//styled
const Container = styled.div`
  width: 100%;
  position: relative;
  display: flex;
  form {
    display: flex;
    width: 100%;

    & > * {
      flex: 1;
    }
  }
`
const DropDown = styled(FormControl)`
  min-width: 80px;
  max-width: 80px;
  position: absolute !important;
  left: 0;
  z-index: 1;
  background-color: transparent;
  overflow: hidden;

  .MuiOutlinedInput-root.Mui-focused
  .MuiOutlinedInput-notchedOutline {
    border-color: transparent;
    border-width: 0;
  }

  .MuiCircularProgress-root {
    svg {
      display: block;
    }
  }

  .MuiSelect-select {
    padding: 0;
    height: 48px;
    display: flex;
    justify-content: center;
    align-items: center;
    border-radius: 0;
    border-right: 1px solid #DADCDF;
  }

  fieldset {
    border-bottom-right-radius: 0;
    border-top-right-radius: 0;
    border-right-width: 0;
    border-width: 0;
    border-right-width: 1px;
  }

  svg {
    display: none;
  }
`
const MobileField = styled(MuiTextField)`
  label {
    left: 85px;
  }

  fieldset {
    legend {
      margin-left: 85px;
    }
  }

  input {
    padding-left: 100px;
    transform: translateZ(0)
  }

  input:-internal-autofill-selected {
    background-color: transparent !important;
  }
`

const Loading = styled.div`
  height: 48px;
  display: flex;
  justify-content: center;
  align-items: center;

  & > * {
    width: 25px !important;
    height: 25px !important;
  }
`

interface TextInputProps {
  name: string,
  label: string,
  type: string,
  country: string,
  countryList: {
    [key: number]: {
      phone: number,
      img: string,
    },
    length: number
  },
  setCountry: Function,
  saveValue: Function,
  value: string,
}

function MobileInput(props: TextInputProps) {
  const {
    name,
    label,
    type,
    country,
    setCountry,
    countryList,
    saveValue,
    value
  } = props

  return (
    <Container>
      <DropDown variant="outlined">
        {countryList.length === 0
          ? <Loading><CircularProgress color="primary" /></Loading>
          :
          <Select
            value={country}
            onChange={(e) => setCountry(e.target.value)}
            input={<OutlinedInput labelWidth={100} name="country-code" id="country-code" />}
          >
            {Object.keys(countryList).map((item, index) => (
              <MenuItem
                value={index}
                key={`${index}-${item}`}
              >
                <img
                  src={countryList[index].img}
                  style={{
                    width: '25px',
                    height: '20px',
                    paddingRight: '5px'
                  }} />
                +{countryList[index].phone}
              </MenuItem>
            ))}
          </Select>
        }
      </DropDown>
      <Field
        margin="none"
        name={name}
        fullWidth
        value={value}
        pattern="\d{1,5}"
        render={(props: any) => (
          <MobileField
            {...fieldToTextField(props)}
            label={label}
            variant="outlined"
            type={type}
            value={value}
            onChange={(e) => {
              const pattern = /^[0-9\b]+$/
              if(e.target.value === '' || pattern.test(e.target.value)) {
                [
                  props.form.setFieldValue(props.field.name, e.target.value),
                  saveValue(name, e.target.value),
                ]
              }
            }}
          />
        )}
      />
    </Container>
  )
}

export default MobileInput