# Mobile Number Input

```js
  import MobileInput from 'omhwebui/components/MobileInput'
```


# Usage
```js
  <MobileInput
    name="mobileNumber"
    label="Mobile Number"
    type="tel"
    country={"0"}
    setCountry={() => console.log('set country')}
    countryList={countryList}
    saveValue={() => console.log('save value')}
    value={""}
  />
```

# Validation
This component is used inside Formik. The validation is passed from Formik down to this component.


# Note
This component needs the OMH country list api to render the countries as Dropdown.