import * as React from "react"
// component
import { Field } from 'formik'
import MuiTextField from '@material-ui/core/TextField';
import { fieldToTextField, TextFieldProps } from 'formik-material-ui'

interface TextInputProps {
  name?: string,
  label: string,
  type: string,
  value: string,
  saveValue?: Function | null
}

function TextInput({
  name,
  label,
  type,
  value,
  saveValue,
}: TextInputProps) {

  return (
    <Field
      margin="none"
      name={name}
      fullWidth
      value={value}
      render={(props: TextFieldProps) => {
        return (
          <MuiTextField
            {...fieldToTextField(props)}
            label={label}
            variant="outlined"
            value={value}
            type={type}
            onChange={(e) => [
              props.form.setFieldValue(props.field.name, e.target.value),
              saveValue(name, e.target.value),
            ]}
          />
        )
      }}
    />
  )
}

export default TextInput