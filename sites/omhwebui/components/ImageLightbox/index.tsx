import * as React from "react"
import Lightbox from 'react-image-lightbox'
import 'react-image-lightbox/style.css'

interface ImageLightboxProps {
  mainSrc?: string
  nextSrc?: string
  prevSrc?: string
  onCloseRequest?: any,
  reactModalStyle? : any,
  onMoveNextRequest? : any,
  onMovePrevRequest? : any,
  imagePadding? : any,
}

function ImageLightbox({
  onCloseRequest,
  mainSrc,
  nextSrc,
  prevSrc,
  reactModalStyle,
  onMovePrevRequest,
  onMoveNextRequest,
  imagePadding
}: ImageLightboxProps) {

  return (
    <Lightbox
      mainSrc={mainSrc}
      nextSrc={nextSrc}
      prevSrc={prevSrc}
      imagePadding={imagePadding}
      onCloseRequest={onCloseRequest}
      reactModalStyle={reactModalStyle}
      onMoveNextRequest={onMoveNextRequest}
      onMovePrevRequest={onMovePrevRequest}
    />
  )
}

export default ImageLightbox