import * as React from "react"
import { Field } from 'formik'
import { TextField } from 'formik-material-ui'
// components
import IconButton from '@material-ui/core/IconButton';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

interface TextInputProps {
  label: string,
  showPassword: Boolean,
  togglePassword: Function,
  name: string,
  type: string,
  value: string,
  // action: Function,
}

// function InputPassword({
//   label,
//   showPassword,
//   togglePassword,
//   name,
// }: TextInputProps) {
//   return (
//     <Field
//       component={TextField}
//       name={name}
//       label={label}
//       type={showPassword ? 'text' : 'password'}
//       margin="none"
//       variant="outlined"
//       InputProps={{
//         endAdornment: (
//           <InputAdornment position="end">
//             <IconButton
//               edge="end"
//               aria-label="Toggle password visibility"
//               onClick={() => togglePassword()}
//             >
//               {showPassword ? <VisibilityOff /> : <Visibility />}
//             </IconButton>
//           </InputAdornment>
//         ),
//       }}
//     />
//   )
// }

export const InputPassword: React.FunctionComponent<TextInputProps> = ({
  label,
  showPassword,
  togglePassword,
  name,
  value
}) => {
  return (
    <Field
      component={TextField}
      name={name}
      label={label}
      type={showPassword ? 'text' : 'password'}
      margin="none"
      variant="outlined"
      value={value}
      InputProps={{
        endAdornment: (
          <InputAdornment position="end">
            <IconButton
              edge="end"
              aria-label="Toggle password visibility"
              onClick={() => togglePassword()}
            >
              {showPassword ? <VisibilityOff /> : <Visibility />}
            </IconButton>
          </InputAdornment>
        ),
      }}
    />
  )
}


export default InputPassword