import * as React from "react"
import styled from 'styled-components'

const Time = styled.div`
  color: #EE620F;
`

function Counter(props) {

  const {
    count,
    setCount,
  } = props

  function format(time) {
    let seconds = time % 60;
    let minutes = Math.floor(time / 60);
    minutes = minutes.toString().length === 1 ? "0" + minutes : minutes;
    seconds = seconds.toString().length === 1 ? "0" + seconds : seconds;
    return minutes + ':' + seconds;
  }

  function useInterval(callback, delay) {
    const savedCallback = React.useRef()

    // Remember the latest function.
    React.useEffect(() => {
      savedCallback.current = callback
    }, [callback])

    // Set up the interval.
    React.useEffect(() => {

      function tick() {
        savedCallback.current()
      }

      if(delay !== null) {
        let id = setInterval(tick, delay)
        return () => clearInterval(id)
      }
    }, [delay])
  }

  function runTime() {
    if(count > 0) {
      useInterval(() => {
        setCount(count - 1)
      }, 1000)
    } else {
      setCount(0)
      return null
    }
  }

  runTime()

  return (
    <Time>
      {format(count)}
    </Time>
  )
}

export default Counter