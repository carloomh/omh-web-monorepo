import * as React from "react"
import Drawer from '@material-ui/core/Drawer'
import styled from 'styled-components'
// media query
import { device } from '../../src/media-queries'

const DrawerStyled = styled(Drawer)`
  position: relative;
  .content {
    width: 100%;

    @media ${device.mobileL} {
      width: 460px;
    }
  }
`

interface IProps {
  anchor: "left" | "top" | "right" | "bottom",
  toggle: Function,
  isToggleHidden: boolean,
  status: boolean,
}

// function omhDrawer(props: IProps) {

//   const {
//     anchor,
//     status,
//     toggle,
//     isToggleHidden,
//     children
//   } = props

//   function renderToggle() {
//     if(isToggleHidden) {
//       return (
//         <div
//           className="toggle"
//           onClick={(e) => toggle(e)}
//         >
//           <svg width="16" height="16" viewBox="0 0 16 16" fill="none">
//             <path opacity="0.5" fillRule="evenodd" clipRule="evenodd" d="M1.12538 0L0 1.12538L6.87462 8L0 14.8746L1.12538 16L8 9.12538L14.8746 16L16 14.8746L9.12538 8L16 1.12538L14.8746 0L8 6.87462L1.12538 0Z" fill="black" />
//           </svg>
//           <style jsx>{`
//             .toggle {
//               padding: 20px;
//               position: absolute;
//             }
//           `}</style>
//         </div>
//       )
//     }
//   }

//   return (
//     <DrawerStyled
//       anchor={anchor}
//       open={status}
//       onClose={(e) => toggle(e)}
//       classes={{
//         paperAnchorRight: "content",
//       }}
//     >
//       {renderToggle()}
//       {children}
//     </DrawerStyled>
//   )
// }

export const omhDrawer: React.FunctionComponent<IProps> = ({
  anchor,
  toggle,
  isToggleHidden,
  status,
  children
}) => {

  function renderToggle() {
    if(isToggleHidden) {
      return (
        <div
          className="toggle"
          onClick={(e) => toggle(e)}
        >
          <svg width="16" height="16" viewBox="0 0 16 16" fill="none">
            <path opacity="0.5" fillRule="evenodd" clipRule="evenodd" d="M1.12538 0L0 1.12538L6.87462 8L0 14.8746L1.12538 16L8 9.12538L14.8746 16L16 14.8746L9.12538 8L16 1.12538L14.8746 0L8 6.87462L1.12538 0Z" fill="black" />
          </svg>
          <style jsx>{`
            .toggle {
              padding: 20px;
              position: absolute;
            }
          `}</style>
        </div>
      )
    }
  }

  return (
    <DrawerStyled
      anchor={anchor}
      open={status}
      onClose={(e) => toggle(e)}
      classes={{
        paperAnchorRight: "content",
      }}
    >
      {renderToggle()}
      {children}
    </DrawerStyled>
  )
}

export default omhDrawer
