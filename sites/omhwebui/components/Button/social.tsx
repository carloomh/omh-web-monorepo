import * as React from "react"
import Button from '@material-ui/core/Button'

const iconFacebook = (
  <svg width="10" height="17" viewBox="0 0 10 17" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M6.57288 2.8645C6.2424 2.8645 5.91192 3.26808 5.91192 3.81888V5.6916H9.18V8.40922H5.91192V16.5243H2.79072V8.40922H0V5.6916H2.79072V4.11264C2.79072 1.836 4.4064 0 6.57288 0H9.18V2.8645H6.57288Z" fill="white" />
  </svg>

)

const iconGoogle = (
  <svg width="22" height="14" viewBox="0 0 22 14" fill="none" xmlns="http://www.w3.org/2000/svg">
    <path fillRule="evenodd" clipRule="evenodd" d="M7 6V8.4H10.97C10.81 9.43 9.77 11.42 7 11.42C4.61 11.42 2.66 9.44 2.66 7C2.66 4.56 4.61 2.58 7 2.58C8.36 2.58 9.27 3.16 9.79 3.66L11.69 1.83C10.47 0.69 8.89 0 7 0C3.13 0 0 3.13 0 7C0 10.87 3.13 14 7 14C11.04 14 13.72 11.16 13.72 7.16C13.72 6.7 13.67 6.35 13.61 6H7Z" fill="white" />
    <path fillRule="evenodd" clipRule="evenodd" d="M22 6H20V4H18V6H16V8H18V10H20V8H22" fill="white" />
  </svg>
)

interface Test {
  className?: Object,
  variant: 'text' | 'outlined' | 'contained',
  text: string,
  action?: Function | null
  icon?: 'facebook' | 'google'
}

const btnDefault = {
  width: '100%',
  margin: '0 0 20px 0',
  minHeight: '45px',
  fontSize: '15px',
  borderRadius: '4px !important',
  fontFamily: 'inherit !important',
  fontWeight: 500,
  padding: '0 !important'
}

const styles = {
  'facebook': {
    color: '#FFFFFF',
    backgroundColor: '#507CC0',
    ...btnDefault
  },
  'google': {
    color: '#FFFFFF',
    backgroundColor: '#DC4E41',
    ...btnDefault
  },
}

function SocialButton(props: Test) {
  const {
    className,
    variant,
    text,
    action,
    icon,
  } = props

  const iconSrc = icon === "facebook" ? iconFacebook : iconGoogle

  return (
    <div
      className="btn-container"
    >
      <Button
        variant={variant}
        classes={className}
        style={styles[icon]}
        onClick={() => action()}
      >
        {iconSrc} <span>{text}</span>
      </Button>
      <style jsx>{`
        span {
          padding-left: 7px;
        }
      `}</style>
    </div>
  )
}

export default SocialButton