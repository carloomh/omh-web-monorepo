# Default Button

```js
import BtnDefault from "omhwebui/components/Button/default";
```

<!-- Brief summary of what the component is, and what it's for. -->

<!-- STORY -->

<!-- STORY HIDE START -->

<!-- STORY HIDE END -->

### Usage ###

```js
  <Button
    className={null}
    action={() => console.log('action')}
    variant="contained"
    color="primary"
    text="Open login"
  />
```

```js
  <Button
    className={null}
    action={() => console.log('action')}
    variant="outlined"
    color="primary"
    text="Open login"
  />
```

### Props ###

Name | Type | Options | Required
------------ | ------------- | ------------- | -------------
**className** | Object | Content cell 2 | no
**action** | Function | event trigger function | yes
**variant** | String | "contained" or "outlined" | yes
**color** | String | "primary" or "secondary" | no
**text** | String | label of the button | yes
