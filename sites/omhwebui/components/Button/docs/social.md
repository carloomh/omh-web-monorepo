# Social Button

```js
import BtnDSocial from "omhwebui/components/Button/social";
```

<!-- Brief summary of what the component is, and what it's for. -->

<!-- STORY -->

<!-- STORY HIDE START -->

<!-- STORY HIDE END -->

### Usage ###

```js
  <BtnSocial
    icon="facebook"
    variant="contained"
    text="Log in with Facebook"
    action={() => console.log('true')}
  />
```

```js
  <BtnSocial
      icon="google"
      variant="contained"
      text="Log in with Google"
      action={() => console.log('google')}
    />
```

### Props ###

Name | Type | Options | Required
------------ | ------------- | ------------- | -------------
**icon** | String | 'facebook' or 'google' | yes
**variant** | String | "contained" or "outlined" | yes
**text** | String | label of the button | yes
**action** | Function | event trigger function | yes



