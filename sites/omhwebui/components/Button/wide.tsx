import * as React from "react"
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import styled from 'styled-components'

const ButtonWide = styled(Button)`
  width: 100%;
  margin: 0 0 20px 0;
  min-height: 45px;
  font-size: 15px;
  border-radius: 4px !important;
  font-weight: 500 !important;
  padding: 0 !important;
`

interface Props {
  className?: Object,
  variant: 'text' | 'outlined' | 'contained',
  text: string,
  action?: Function | null
  type?: 'button' | 'submit' | 'reset',
  color?: 'inherit' | 'primary' | 'secondary' | 'default'
  children?: any | null,
  disabled?: boolean,
  isSubmitting?: boolean
  isWide?: boolean
}

function Btn(props: Props) {

  const {
    className,
    variant,
    text,
    action,
    type,
    color,
    children,
    disabled,
    isSubmitting,
  } = props

  return (
    <div
      className="btn-container"
    >
      <ButtonWide
        disabled={disabled}
        variant={variant}
        classes={className}
        color={color}
        type={type}
        onClick={() => action()}
      >
        {isSubmitting
          ?
          <CircularProgress
            color="inherit"
            style={{
              width: "25px",
              height: "25px"
            }}
          />
          : children || text}
      </ButtonWide>
    </div>
  )
}

export default Btn