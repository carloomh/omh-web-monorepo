import * as React from "react"
import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'
import styled from 'styled-components'

const ButtonDefault = styled(Button)`
  margin: 0 0 20px 0;
  min-width: 100% !important;
  min-height: 45px;
  font-size: 15px;
  border-radius: 4px !important;
  font-weight: 500 !important;
`

interface Props {
  className?: Object,
  variant: 'text' | 'outlined' | 'contained',
  text: string,
  action?: Function | null
  type?: 'button' | 'submit' | 'reset',
  color?: 'inherit' | 'primary' | 'secondary' | 'default'
  children?: any | null,
  disabled?: boolean,
  isSubmitting?: boolean,
  fullWidth?: boolean
}

function Btn(props: Props) {

  const {
    className,
    variant,
    text,
    action,
    type,
    color,
    children,
    disabled,
    isSubmitting,
    fullWidth
  } = props

  return (
    <div
      className="btn-container"
    >
      <ButtonDefault
        disabled={disabled}
        variant={variant}
        classes={className}
        color={color}
        type={type}
        fullWidth={fullWidth}
        onClick={() => action()}
      >
        {isSubmitting
          ?
          <CircularProgress
            color="inherit"
            style={{
              width: "25px",
              height: "25px"
            }}
          />
          : children || text}
      </ButtonDefault>
    </div>
  )
}

export default Btn