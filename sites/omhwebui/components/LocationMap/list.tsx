import * as React from "react"
import styled from 'styled-components'
import Icons from './icons'
import getTimeDisplay from './helper'

interface styleProps {
  grow?: Boolean,
  color?: any
}

const Container = styled.div`
  font-family: Roboto, sans-serif;
`

const Filter = styled.div`
  display: flex;
  justify-content: ${(props:styleProps) => props.grow ? 'space-between': ''};
  overflow: auto;
`

const FilterItem = styled.div`
  padding: 10px;
  flex-grow: 1;
  text-align: center;
  cursor: pointer;
  border-bottom: 3px solid ${(props:styleProps) => props.color || '#f4f4f4f4'};
  display: flex;
  flex-wrap: wrap;
  align-items: center;
  font-weight: 500;
  justify-content: center;
`

const FilterPin = styled.div`
  & svg{
    width: 60px;
    height: 60px;
    margin-right: 5px;
  }
`

const FilterLabel = styled.span`
  color: ${props => props.color};
  font-size: 14px;
  line-height: 18px;
`

const InfraList = styled.ul`
  min-height: 70px;
  max-height: 270px;
  overflow: auto;
  list-style: none;
  padding: 0;
  margin: 0;
  scroll-behavior: smooth; 
`

const InfraListItem = styled.li`
  padding: 25px 30px;
  border-bottom: 1px solid #F4F4F4;
  display: flex;
  justify-content: space-between;
  cursor: pointer;

  svg{
    height: 35px;
    flex: 0 0 30px;
  }

  &:hover, &.active{
    background: #F9F9FA;
  }
`

const Address = styled.div`
  width: 80%;
`

const ListHeader = styled.div`
  display: flex;
  align-items: flex-start;
`

const Name = styled.h3`
  font-size: 18px;
  font-weight: 500;
  color: #484848;
  margin: 5px 0;
`
const Desc = styled.div`
  font-size: 14px;
  font-weight: 300;
`

const Walk = styled.div`
  width: auto;
  height: 35px;
  display: flex;
  align-items: flex-start;
  justify-content: space-between;
  width: 25%;
  @media (min-width : 1128px) {
    width: 20%;
  }
`

const Walking = styled.div`
  flex: 0 0 70%;
  h5{
    font-size: 18px;
    font-weight: 500;
    margin: 0 0 5px;
  }
  p{
    font-size: 16px;
    color: #BDBDBD;
    margin: 0;
  }
`

const listColor = {
  bus: '#2f5dab', 
  mrt: '#13aee5', 
  school: '#318540',
  food: '#94c83e', 
  market: '#fdb615', 
  health: '#f174ac'
}



interface LocationListProps {
  markers?: any,
  filterList?: any,
  center: any,
  filter: any,
  setSelectFilter: Function
}

declare const google: any;

const HOUR_IN_MILLISECOND = 60 * 60 * 1000
const AVG_WALKING_SPEED_KM_PER_HR = 5

const computeWalkingTime = (distance) => {
  const walkingTime = distance / (AVG_WALKING_SPEED_KM_PER_HR * 1000)
  return getTimeDisplay(walkingTime * HOUR_IN_MILLISECOND)
}

const computeDistanceBetween = (distance) => {
  const converted = distance/1000
  return converted < 1 ? `${Math.round(distance)}m` : `${converted.toFixed(2)}km`
}

const LocationList = (props: LocationListProps) => {
  const {
    markers,
    filterList,
    center,
    filter,
    setSelectFilter
  } = props

  
  return(
    <Container>
      {
        filterList && 
        <Filter grow={filterList.length > 3}>
          {filterList && filterList.map((fil, key) => {
            return(
              <FilterItem
                key={key}
                onClick={() => setSelectFilter(fil.code)}
                color={ fil.code === filter ? listColor[fil.code] : ''}
              >
                <FilterPin>
                  <Icons type={fil.code} active={fil.code === filter} />
                </FilterPin>
                <FilterLabel color={listColor[fil.code]}>{fil.label}</FilterLabel>
              </FilterItem>
            )
          })}
        </Filter>
      }
      <InfraList id={'infraList'}>
        {
          markers && markers.length && markers.map((marker, key) => {
            const {
              type,
              name,
              address,
              description
            } = marker

            const { latitude, longitude } = address
            const point = new google.maps.LatLng(parseFloat(latitude), parseFloat(longitude))
            const main = new google.maps.LatLng(parseFloat(center.lat), parseFloat(center.lng))

            const distanceBetween = google.maps.geometry.spherical.computeDistanceBetween(main, point)
            const walkingTime = computeWalkingTime(distanceBetween)

            return(
              <InfraListItem
                key={key}
                // onMouseEnter={() => this.handleLocationHover(key)}
                // onMouseLeave={() => this.handleLocationHover(null)}
                // onClick={() => this.handleLocationSelected(key)}
                // className={ key === itemSelected ? 'active' : ''}
                id={`filterItem-${key}`}
              >
                <Address>
                  <ListHeader>
                  <Icons type={'pin'} active={true} fill={filterList ? listColor[filterList[type - 1].code] : '#EEEEEE'} />
                    <div>
                      <Name>{name}</Name>
                      <Desc>{description}</Desc>
                    </div>
                  </ListHeader>
                  {/* {
                    details && details.length ? 
                      <Block>
                        { details.map( (detail, key) => {
                          return <Detail key={key}>{detail}</Detail>
                        }) }
                      </Block>
                      : ''
                  } */}
                </Address>
                <Walk>
                  {/* <IconWalk src='/themes/omhmy/assets/images/ic_directions_walk.svg' /> */}
                  <Walking>
                    <h5>{ walkingTime }</h5>
                    <p>{ computeDistanceBetween(distanceBetween) }</p>
                  </Walking>
                </Walk>
              </InfraListItem>
            )
          })
        }         
      </InfraList>
    </Container>
  )
}

export default LocationList
