const HOUR_IN_MILLISECOND = 60 * 60 * 1000
const MINUTE_IN_MILLISECOND = 60 * 1000
const SECONDS_IN_MILLISECOND = 1000

const getTimeDisplay = (timeInMilli) => {
  if (timeInMilli >= HOUR_IN_MILLISECOND)
    return `${Math.round(timeInMilli / HOUR_IN_MILLISECOND)} hr`
  else if(timeInMilli >= MINUTE_IN_MILLISECOND)
    return `${Math.round(timeInMilli / MINUTE_IN_MILLISECOND)} min`
  else if(timeInMilli >= SECONDS_IN_MILLISECOND)
    return `${Math.round(timeInMilli / SECONDS_IN_MILLISECOND)} s`
  else
    return 'NA'
}

export default getTimeDisplay