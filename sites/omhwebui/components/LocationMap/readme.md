# Location Map

```js
  import LocationMap from 'omhwebui/components/LocationMap'
```


# Usage
```js
  <LocationMap
    showList={false}
  />
```

### Props ###

Name | Type | Options | Required
------------ | ------------- | ------------- | -------------
**showList** | Boolean | Show location list | yes