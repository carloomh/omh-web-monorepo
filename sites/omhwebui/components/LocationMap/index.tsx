import * as React from "react"
import { compose, withProps, lifecycle } from "recompose"
import { GoogleMap, withGoogleMap, Marker, withScriptjs } from "react-google-maps"
import styled from 'styled-components'
import LocationList from './list'

const Container = styled.div`
`

const defaultFilters = [
  { code: 'bus',
    label: "Bus Stop"
  },
  {
    code: "mrt",
    label: "MRT Station"
  },
  {
    code: "school",
    label: "School"
  },
  {
    code: "food",
    label: "Restaurant"
  },
  {
    code: "market",
    label: "Market"
  },
  {
    code: "health",
    label: "Health Care"
  }
]

interface MapProps {
  children?: object,
  locations?: any
  showList?: Boolean,
  mapStyle?: any,
  zoomToMarkers?: any,
  markers?: any,
  zoom?: number,
  center?: any,
  filters?: any,
  centerLat: number,
  centerLong: number,
  selectedFilter: any
}

declare const google: any;

const LocationMap = compose(
  withProps({
    googleMapURL: "https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=AIzaSyDltPjN_iX87BuSivz3d0HGS85BNns6WSs",
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `auto` }} />,
    mapElement: <div style={{ height: `350px` }} />,
  }),
  lifecycle({
    componentDidMount() {
      this.setState({
        zoomToMarkers: map => {
          const bounds = new google.maps.LatLngBounds()
          map.props.children.forEach((child) => {
            if (child.type === Marker) {
              bounds.extend(new google.maps.LatLng(child.props.position.lat, child.props.position.lng))
            }
          })
          map.fitBounds(bounds)
        }
      })
    },
  }),
  withScriptjs,
  withGoogleMap
)((props: MapProps) => {

  const {
    markers,
    showList = false,
    mapStyle,
    zoom,
    center,
    children,
    filters,
    centerLat,
    centerLong,
  } = props

  const centerMap = {
    lat: centerLat,
    lng: centerLong
  }

  let filterList = filters ? defaultFilters.filter((f, i) => f && filters.indexOf(i+1) > -1 ) : defaultFilters
  const [filter, setSelectFilter] = React.useState(filterList && filterList[0].code || 'mrt')
  const filteredMarker = markers && markers.filter((marker) => filterList[marker.type-1] && filterList[marker.type-1].code === filter )

  return(
    <Container>
      <GoogleMap
        ref={props.zoomToMarkers}
        center= { centerMap }
        defaultOptions={{
          disableDefaultUI: false, // disable default map UI
          keyboardShortcuts: false, // disable keyboard shortcuts
          scaleControl: false, // allow scale controle
          rotateControl: false,
          mapTypeControl: false,
          minZoom: true,
          styles: mapStyle || {}
        }}
        defaultCenter={ center || { lat: 25.0391667, lng: 121.525 } }
        zoom={zoom || 7}
        options={{
          minZoom: zoom || 3
        }}
      >
        {
          children || filteredMarker && filteredMarker.map((marker, key) => {
            const { address } = marker
            return(
              <Marker
                key={key}
                position={{ 
                  lat: parseFloat(address.latitude), 
                  lng: parseFloat(address.longitude)
                }}
              />
            )
          }
          )
        }
      </GoogleMap>
      {
        showList && filteredMarker
          ?
            <LocationList
              markers={filteredMarker}
              filterList={filterList && filterList.sort()}
              center={centerMap}
              setSelectFilter={setSelectFilter}
              filter={filter}
            />
          : ''
      }
    </Container>
  )
})

export default LocationMap
