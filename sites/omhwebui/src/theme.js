import { createMuiTheme } from '@material-ui/core/styles'
import { COLOR } from './colors'

const theme = {
  palette: {
    primary: {
      main: `${COLOR.ORANGE_PRIMARY}`,
      // dark: '#FFCC00'
    },
  },
  overrides: {
    MuiFormHelperText: {
      root: {
        lineHeight: '1.7'
      }
    },
    MuiButton: {
      root: {
        boxShadow: 'none'
      },
      label: {
        textTransform: 'initial'
      },
      contained: {
        boxShadow: 'none'
      },
      outlined: {
        boxSizing: 'border-box'
      }
    },
    MuiButtonBase: {
      root: {
        height: '45px',
        boxShadow: 'none'
      }
    },
    MuiInputBase: {
      root: {
        height: '48px'
      },
    },
    MuiOutlinedInput: {
      root: {
        "&$focused > fieldset": {
          borderColor: `${COLOR.BLUE_TEAL} !important`
        }
      }
    },
    MuiInput: {
      underline: {
        "&::after": {
          borderBottom: `2px solid ${COLOR.ORANGE_PRIMARY}`
        }
      }
    },
    MuiInputLabel: {
      root: {
        color: "#9B9B9B",
        "&$focused": {
          color: COLOR.BLUE_TEAL
        }
      },
      outlined: {
        transform: 'translate(14px, 17px) scale(1)'
      }
    },
    MuiFormControl: {
      root: {
        width: '100%'
      }
    }
  }
}

export default theme