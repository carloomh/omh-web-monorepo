# Color Pallete


```js
  import { COLOR } from 'src/colors'
```

### Usage


```css
  h1 {
    color: `${COLOR.ORANGE_PRIMARY}`
  }
```