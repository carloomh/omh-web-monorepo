import { addParameters, configure, addDecorator } from "@storybook/react"
import { themes, create } from '@storybook/theming'
import { setOptions } from "@storybook/addon-options"
import { addReadme } from 'storybook-readme'

const req = require.context("../stories", true, /.stories.tsx$/)

const basicTheme = create({
  base: 'light',
  brandTitle: 'README addon',
  brandUrl: 'https://github.com/tuchk4/storybook-readme',
  brandImage: null,
})

setOptions({
  downPanelInRight: true,
})

addParameters({
  options: {
    showPanel: true,
    panelPosition: 'right',
    theme: basicTheme,
  },
  readme: {
    // You can set the global code theme here.
    codeTheme: 'duotone-sea'
  },
});

addDecorator(addReadme)

function loadStories() {
  req.keys().forEach(req)
}

configure(loadStories, module)